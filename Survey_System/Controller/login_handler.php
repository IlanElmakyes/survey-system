<?php
use Model\Classes\DB_Connections as dbConnect;
session_start();
require_once $_SESSION['AUTOLOAD_PATH'];
header('Content-Type: application/json');
function handleManager()
{
    $managerDB = new dbConnect\ManagerDB();
    $GLOBALS['result']['manager'] = true;
        if(!empty($_POST['password']))
        {
            if($managerDB->verifyManagerLogin($_POST['code'],$_POST['password']))
            {
                $_SESSION['MANAGER'] = TRUE;
                $GLOBALS['result']['redirect']="../View/managerIndex.php";
            }
        }
}
function handleSurvey()
{
    
    $activeSurveyDB = new dbConnect\ActiveSurveyDB();
    if($activeSurveyDB->checkIfExist('active_survey','code',$_POST['code']))
    {
        if($activeSurveyDB->checkTimer($_POST['code']))
        {
            $_SESSION['SURVEY_CODE'] = $_POST['code'];
            $GLOBALS['result']['redirect']="../View/survey.php";
            $GLOBALS['result']['expire']=$activeSurveyDB->getExpireDate($_POST['code']);
        }
    }
}
$GLOBALS['result'] = array('manager'=>false,'redirect'=>"");
$managerDB = new dbConnect\ManagerDB();
if($managerDB->verifyManagerLogin($_POST['code']))
{
    handleManager();
}
else
{
    handleSurvey();
}
echo json_encode($GLOBALS['result']);
?>