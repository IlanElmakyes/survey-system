<?php
use Model\Classes\DB_Connections as dbConnect;
use Model\Classes\Data_Objects as objects;
session_start();
require_once $_SESSION['AUTOLOAD_PATH'];
header('Content-Type: application/json');
define("EXTRA_CATEGORYS","קטגוריות נוספות");
\set_time_limit(300);
switch($_POST['reportAction'])
{
    case "teacher":
        echo singleTeacherReportData($_POST['id']);
        break;
    case "class":
        echo singleClassReportData($_POST['id']);
        break;
    case "faculty":
        echo facultyClassesData($_POST['id']);
        break;
    case "DownloadCSV":
        echo getFileToDownload(json_decode($_POST['fileText']),$_POST['fileName']);
        break;
    case "deleteReports":
        deleteReportFiles();
        break;
    case "fullReportRedirect":
        echo getFullReportsURL();
        break;
    case "ReportSessionData":
        echo getReportSessionData();
        break;
    case "fullTeacher":
        echo fullTeacherReport($_POST['id']);
        break;
    case "fullFaculty":
        echo fullFacultyReport($_POST['id']);
        break;
}
/**
 * prepares and return an array that represents a full report for a single faculty
 * the array is build like so:
 * array('className1'=>array('teacher1'=>(array('numeric'=>array(0=>*question1 info*,1=>*question2 info*,....)...)...)...)...)
 */
function fullFacultyReport($facultyId)
{
    $ret_arr = array();
    $facultiesDB = new dbConnect\FacultiesDB();
    $answerNumericDB = new dbConnect\AnswerNumericDB();
    $questionsDB = new dbConnect\QuestionsDB();
    $classesDB = new dbConnect\ClassesDB();
    $activeSurveyDB = new dbConnect\ActiveSurveyDB();
    $classes = $facultiesDB->getClassesByFacultyId($facultyId);
    if(!empty($classes))
    {
        $ret_arr[EXTRA_CATEGORYS] = array();
        $categoryNamesArray = array();
        foreach($classes as $class)
        {
            $teachers = $classesDB->getAllTeachers($class->getId(),"object");
            if(empty($teachers))
                continue;
            foreach($teachers as $teacher)
            {
                $ret_arr[$class->getName()][$teacher->getName()]=array("numeric"=>array(array("שאלה","מס משיבים","1","2","3","4","5","סטיית תקן","ממוצע")));
                $answersNumeric = $answerNumericDB->getAnswersByClassAndTeacher($class->getId(),$teacher->getId());
                if(empty($answersNumeric))
                    continue;
                foreach($answersNumeric as $answerNumeric)
                {
                    $questiontext = $questionsDB->getTextById($answerNumeric->getQuestionId());
                    $totalAnswered = $answerNumeric->totalNumberAnswered();
                    $ret_arr[$class->getName()][$teacher->getName()]['numeric'][]=array($questiontext,$totalAnswered,getPrcentText($totalAnswered,$answerNumeric->getAnswer_1()),getPrcentText($totalAnswered,$answerNumeric->getAnswer_2()),
                    getPrcentText($totalAnswered,$answerNumeric->getAnswer_3()),getPrcentText($totalAnswered,$answerNumeric->getAnswer_4()),getPrcentText($totalAnswered,$answerNumeric->getAnswer_5()),
                    $answerNumeric->standardDeviation(),$answerNumeric->getAverage());
                }
            }
            foreach($activeSurveyDB->getAllCategorysByClassId($class->getId()) as $categoryName){
                if(!in_array($categoryName,$categoryNamesArray))
                    $categoryNamesArray[]=$categoryName;
            }
        }
        if(!empty($categoryNamesArray))
            fillCategoryAnswers($ret_arr,$facultyId,$categoryNamesArray);
    }
    return json_encode($ret_arr);
}
/**
 * fills ret_arr with answers for a category besides teacher
 */
function fillCategoryAnswers(&$ret_arr,$facultyId,$categoryNamesArray)
{
    $answerTextDB = new dbConnect\AnswerTextDB();
    $answerNumericDB = new dbConnect\AnswerNumericDB();
    $questionsDB = new dbConnect\QuestionsDB();
    foreach($categoryNamesArray as $categoryName)
    {
        if($categoryName == dbConnect\CategoryDB::teacher)
            continue;
        $answersNumeric = $answerNumericDB->getAnswersByFacultyAndCategory($facultyId,$categoryName);
        $ret_arr[EXTRA_CATEGORYS][$categoryName]=array("numeric"=>array(array("שאלה","מס משיבים","1","2","3","4","5","סטיית תקן","ממוצע")),"text"=>array());
        if(empty($answersNumeric))
            return;
        foreach($answersNumeric as $answerNumeric)
        {
            $questiontext = $questionsDB->getTextById($answerNumeric->getQuestionId());
            $totalAnswered = $answerNumeric->totalNumberAnswered();
            $ret_arr[EXTRA_CATEGORYS][$categoryName]['numeric'][]=array($questiontext,$totalAnswered,getPrcentText($totalAnswered,$answerNumeric->getAnswer_1()),getPrcentText($totalAnswered,$answerNumeric->getAnswer_2()),
            getPrcentText($totalAnswered,$answerNumeric->getAnswer_3()),getPrcentText($totalAnswered,$answerNumeric->getAnswer_4()),getPrcentText($totalAnswered,$answerNumeric->getAnswer_5()),
            $answerNumeric->standardDeviation(),$answerNumeric->getAverage());
        }
        $answersText = $answerTextDB->getAnswersByFacultyAndCategory($facultyId,$categoryName);
        if(!empty($answersText))
            foreach($answersText as $answerText)
                $ret_arr[EXTRA_CATEGORYS][$categoryName]['text'][] = $answerText->getText();
    }
}
/**
 * prepares and return an array that represents a full report for a single teacher
 * the array is build like so:
 * array('facultyName1'=>array('class1'=>(array('numeric'=>array(0=>*question1 info*,1=>*question2 info*,....)...)...)...)...)
 */
function fullTeacherReport($teacherId)
{
    $ret_arr = array('text'=>array());
    $teachersDB = new dbConnect\TeachersDB();
    $answerNumericDB = new dbConnect\AnswerNumericDB();
    $answerTextDB = new dbConnect\AnswerTextDB();
    $questionsDB = new dbConnect\QuestionsDB();
    $classFacultyInfo = $teachersDB->getClassesAndFacultyInformation($teacherId);
    if(!empty($classFacultyInfo))
    {
        foreach($classFacultyInfo as $row)
        {
            $ret_arr[$row['faculty_name']][$row['class_name']]=array("numeric"=>array(array("שאלה","מס משיבים","1","2","3","4","5","סטיית תקן","ממוצע")));
            $answersNumeric = $answerNumericDB->getAnswersByClassAndTeacher($row['class_id'],$teacherId);
            if(!empty($answersNumeric))
            {
                foreach($answersNumeric as $answerNumeric)
                {
                    $questiontext = $questionsDB->getTextById($answerNumeric->getQuestionId());
                    $totalAnswered = $answerNumeric->totalNumberAnswered();
                    $ret_arr[$row['faculty_name']][$row['class_name']]['numeric'][] = array($questiontext,$totalAnswered,getPrcentText($totalAnswered,$answerNumeric->getAnswer_1()),getPrcentText($totalAnswered,$answerNumeric->getAnswer_2()),
                    getPrcentText($totalAnswered,$answerNumeric->getAnswer_3()),getPrcentText($totalAnswered,$answerNumeric->getAnswer_4()),getPrcentText($totalAnswered,$answerNumeric->getAnswer_5()),
                    $answerNumeric->standardDeviation(),$answerNumeric->getAverage());
                }
            }
        }
        $answersText = $answerTextDB->getAnswersByTeacher($teacherId);
            if(!empty($answersText))
                foreach($answersText as $answerText)
                    $ret_arr['text'][] = $answerText->getText();
    }
    return json_encode($ret_arr);
}
//deletes all files from Report_files folder
function deleteReportFiles()
{
    $files = glob("../View/Report_Files/*");
    foreach($files as $file)
        if(is_file($file))
            unlink($file);
}
//creates a file in Reports_files folder, and send back it's path
function getFileToDownload($textArray,$fileName="")
{
    $BOM = "\xEF\xBB\xBF";
    //default file name
    if($fileName === "")
        $fileName = "../View/Report_Files/".date('ymd_Hms',time()).".csv";
    else $fileName = "../View/Report_Files/".$fileName.".csv";
    $file = fopen($fileName,"w");
    if(!$file)
        return 0;
    //This is a BOM (Byte Order Mark) for utf-8, it's used to tell the reading software (like exel) how to read the encoding
    fwrite($file,$BOM);
    foreach($textArray as $text)
        fwrite($file,$text);
    fclose($file);
    return json_encode($fileName);

}
/**
 * returns the report data that is saved in the current session
 * then unsets the session report variables
 */
function getReportSessionData()
{
    $reportData = json_encode($_SESSION['reportData']);
    unset($_SESSION['REPORT']);
    return $reportData;
}
/**
 * sets information in the session and sends the url to the reports page
 */
function getFullReportsURL()
{
    $dataArray = json_decode($_POST['dataArray']);
    $_SESSION['reportData'] = array("type"=>$_POST['type'],"id"=>$dataArray->id,"name"=>$dataArray->name);
    $_SESSION['REPORT'] = true;
    $_SESSION['PREV_PAGE'] = $_SERVER['HTTP_REFERER'];
    return json_encode("../View/managerReports.php");
}
/**
 * returns the ID and name of all the classes in a single faculty by faculty ID
 * the classes data is returned as a json object
 */
function facultyClassesData($facultyId)
{
    $ret_arr = array();
    $facultiesDB = new dbConnect\FacultiesDB();
    $allClasses=array();
    $allClasses=$facultiesDB->getFacultyClasses($facultiesDB->getById($facultyId));
    foreach($allClasses as $class)
        $ret_arr[] = array("id"=>$class->getId(),"name"=>$class->getName());
    return json_encode($ret_arr);
}
/**
 * returns the report data of a single teacher by ID
 * the data is returned in an array ordered for use with the JS function fillTable()
 */
function singleTeacherReportData($teacherId)
{
    $numericAnswerDB = new dbConnect\AnswerNumericDB();
    $textAnswerDB = new dbConnect\AnswerTextDB();
    $questionsDB = new dbConnect\QuestionsDB();
    $ret_arr = array('numeric'=>array(),'text'=>array());
    $ret_arr['numeric'][] = array("שאלה","מס משיבים","1","2","3","4","5","סטיית תקן","ממוצע");
    $allNumericAnswers=array();
    $allNumericAnswers=$numericAnswerDB->unifySameAnswerResults($numericAnswerDB->getAnswersByTeacher($teacherId));
    if(!empty($allNumericAnswers)) {
        foreach($allNumericAnswers as $answerNumeric)
        {
            $questiontext = $questionsDB->getTextById($answerNumeric->getQuestionId());
            $totalAnswered = $answerNumeric->totalNumberAnswered();
            $ret_arr['numeric'][] = array($questiontext,$totalAnswered,getPrcentText($totalAnswered,$answerNumeric->getAnswer_1()),getPrcentText($totalAnswered,$answerNumeric->getAnswer_2()),
            getPrcentText($totalAnswered,$answerNumeric->getAnswer_3()),getPrcentText($totalAnswered,$answerNumeric->getAnswer_4()),getPrcentText($totalAnswered,$answerNumeric->getAnswer_5()),
            $answerNumeric->standardDeviation(),$answerNumeric->getAverage());
        }
    }
    $allTextAnswers=array();
    $allTextAnswers=$textAnswerDB->getAnswersByTeacher($teacherId);
    if(!empty($allTextAnswers)) {
        foreach($allTextAnswers as $answerText)
        $ret_arr['text'][] = $answerText->getText(); 
    }
    return json_encode($ret_arr);
}

/**
 * returns the report data of a single class by ID
 * the data is returned in an array ordered for use with the JS function fillTable()
 */
function singleClassReportData($classId)
{
    $numericAnswerDB = new dbConnect\AnswerNumericDB();
    $questionsDB = new dbConnect\QuestionsDB();
    $ret_arr['numeric'] = array();
    $ret_arr['numeric'][] = array("שאלה","מס תשובות","1","2","3","4","5","סטיית תקן","ממוצע");
    $allNumericAnswers=array();
    $allNumericAnswers=$numericAnswerDB->unifySameAnswerResults($numericAnswerDB->getAnswersByClass($classId));
    if(!empty($allNumericAnswers)) {
        foreach($allNumericAnswers as $answerNumeric)
        {
            $questiontext = $questionsDB->getTextById($answerNumeric->getQuestionId());
            $totalAnswered = $answerNumeric->totalNumberAnswered();
            $ret_arr['numeric'][] = array($questiontext,$totalAnswered,getPrcentText($totalAnswered,$answerNumeric->getAnswer_1()),getPrcentText($totalAnswered,$answerNumeric->getAnswer_2()),
            getPrcentText($totalAnswered,$answerNumeric->getAnswer_3()),getPrcentText($totalAnswered,$answerNumeric->getAnswer_4()),getPrcentText($totalAnswered,$answerNumeric->getAnswer_5()),
            $answerNumeric->standardDeviation(),$answerNumeric->getAverage());
        }
    }

    return json_encode($ret_arr);

}
/**
 * returns the percent of a divisor and a dividend
 */
function getPrcentText($divisor,$dividend)
{
    return number_format((($dividend/$divisor)*100),2)."%";
}

?>