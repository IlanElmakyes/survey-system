<?php
use Model\Classes\DB_Connections as dbConnect;
use Model\Classes\Data_Objects as objects;

session_start();
require_once $_SESSION['AUTOLOAD_PATH'];
header('Content-Type: application/json');
if(isset($_POST['SettingsAction']))
{
    switch($_POST['SettingsAction'])
    {
        case "getCurrentTimer":
            echo \json_encode(timerToString());
            break;
        case "updateTimer":
            if(isset($_POST['TimerStep']))
                echo json_encode(updateTimer($_POST['TimerStep']));
            break;
        case "changeCredentials":
            echo json_encode(updateUsernameAndPassword($_POST['inputs']));
        break;  
    }
    
}

function updateUsernameAndPassword($inputs){
    if(empty($inputs))
        return false;
    $inputs = json_decode($inputs);
    $managerDb = new dbConnect\ManagerDB();
    $usernameToUpdate = "";
    $passwordToUpdate = "";
    if(!$managerDb->verifyManagerLogin($inputs->oldUsername,$inputs->oldPassword))
        return false;

    $usernameToUpdate = $inputs->newUsername ? $inputs->newUsername : $inputs->oldUsername;
    if(!empty($inputs->newPassword) && $inputs->newPassword === $inputs->confirmPassword)
        $passwordToUpdate = $inputs->newPassword;

    if(!preg_match("/[A-Za-z0-9]{3,16}/",$usernameToUpdate) || !preg_match("/[A-Za-z0-9]{3,16}/",$passwordToUpdate))
        return false;
    
    return $managerDb->updateManagerLoginInfo($inputs->oldUsername,$usernameToUpdate,$passwordToUpdate);
    
    
}
function timerToString()
{
    $actSurveyDb=new dbConnect\ActiveSurveyDB();
    $timer=$actSurveyDb->getTimerStep();
    return $timer->format('ימים: %d שעות: %h דקות: %i');
}

function updateTimer($newTimer)
{
    $actSurveyDb=new dbConnect\ActiveSurveyDB();
    if($actSurveyDb->updateTimerSetting($newTimer)===false)
        return 0;
    else return timerToString();
}

?>