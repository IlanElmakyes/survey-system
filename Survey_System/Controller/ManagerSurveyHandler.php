<?php
use Model\Classes\DB_Connections as dbConnect;
use Model\Classes\Data_Objects as objects;
session_start();
require_once $_SESSION['AUTOLOAD_PATH'];
header('Content-Type: application/json');

if(isset($_POST["managerSurveyAction"]))
{
    switch($_POST["managerSurveyAction"])
    {
        case "getSurveyQuestionnaires":
            echo json_encode(getSurveyQuestionnaires($_POST["SurveyId"]));
            break;
        case "getAllQuestionnaires":
            echo json_encode(getAllQuestionnaires());
            break;
    }
}
function getAllQuestionnaires()
{
    $questionnaireDB = new dbConnect\QuestionnaireDB();
    $questionnaires = $questionnaireDB->getAll();

    $ret_arr['כל השאלונים'] = array();
    $ret_arr['כל השאלונים'][] = array("מס'","שם","קטגוריה");
    foreach($questionnaires as $questionnaire)
    {
        $ret_arr['כל השאלונים'][] = array($questionnaire->getId(),$questionnaire->getName(),$questionnaire->getCategory());
    }
    return $ret_arr;
}
function getSurveyQuestionnaires($surveyId)
{
    $surveyDB = new dbConnect\SurveyDB();
    $ret_arr[] = array("מס'","שם","קטגוריה");
    $questionnaires = $surveyDB->getAllQuestionnaires($surveyId);
    foreach($questionnaires as $questionnaire)
    {
        $ret_arr[] = array($questionnaire['id'],$questionnaire['name'],$questionnaire['category_name']);
    }
    return $ret_arr;
}

?>