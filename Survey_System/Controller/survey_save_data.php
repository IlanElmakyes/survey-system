<?php
use Model\Classes\DB_Connections as dbConnect;
use Model\Classes\Data_Objects as objects;
\session_start();
require_once $_SESSION['AUTOLOAD_PATH'];

// const ENTITY_LESS_THEN = "&#60;";
// const ENTITY_GREATER_THEN = "&#62;";
// const ENTITY_SEMICOLON = "$#59;";
const MAX_STRING_LENGTH = 240;
if(isset($_POST['results']))
{
    $jsonData=json_decode($_POST['results']);
    $surveyDb= new dbConnect\ActiveSurveyDB();
    $survey = $surveyDb->getByCode($_SESSION['SURVEY_CODE']);

    for($i=0;$i<sizeof($jsonData);$i++)
    {
        if(!$jsonData[$i]->isSkiped)
        {
            $categoryName=$jsonData[$i]->name;
            $teacherId=$jsonData[$i]->id;
            $isTeacher=$jsonData[$i]->isTeacher;
            $text=$jsonData[$i]->textAnswer;
            for($j=0;$j<sizeof($jsonData[$i]->questions);$j++)
            {
                $questId=$jsonData[$i]->questions[$j]->id;
                $result=$jsonData[$i]->questions[$j]->numericAnswer;
                $survey->saveNumericAnswer($result,$questId,$teacherId,$categoryName,$isTeacher);
            }
            if(!empty($text))
            {
                //replacing script signs from plain text to entity numbers, so no scripts can be injected to the HTML page
                // $text = str_replace("<",ENTITY_LESS_THEN,$text);
                // $text = str_replace(">",ENTITY_GREATER_THEN,$text);
                // $text = str_replace(";",ENTITY_SEMICOLON,$text);
                //making sure that the string will not be too long for the DB, some data loss may occer
                $text = substr($text,0,MAX_STRING_LENGTH-1);
                $survey->saveTextAnswer($text,$teacherId,$categoryName,$isTeacher);
            }
        }
    }
    $surveyDb->updateAnswered($survey->getCode());
    $res['redirect']="../View/survey_finish.php";
    $res['expire']=$surveyDb->getExpireDate($survey->getCode());
    $res['code']=$survey->getCode();
    //$redirect="../View/survey_finish.php";
    //$jsonData=\json_encode($redirect);
    $jsonData=\json_encode($res);
    echo($jsonData);
}
?>