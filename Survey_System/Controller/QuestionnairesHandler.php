<?php
use Model\Classes\DB_Connections as dbConnect;
use Model\Classes\Data_Objects as objects;
session_start();
require_once $_SESSION['AUTOLOAD_PATH'];
header('Content-Type: application/json');

if(isset($_POST["QuestionnaireAction"]))
{
    switch($_POST["QuestionnaireAction"])
    {
        case "getQuestionnaires":
            echo json_encode(getQuestionnaires());
            break;
        case "getQuestions":
            if(isset($_POST["QuestionnaireID"]))
                echo json_encode(getQuestions($_POST["QuestionnaireID"]));
            break;
        case "deleteQuestionnaire":
            if(isset($_POST["QuestionnaireID"]))
                echo json_encode(deleteQuestionnaire($_POST["QuestionnaireID"]));
            break;
        case "getCategories":
            echo json_encode(getCategories());
            break;
        case "getQuestionnaire":
            if(isset($_POST["QuestionnaireID"]))
                echo json_encode(getQuestionnaire($_POST["QuestionnaireID"]));
            break;
        case "getAllQuestions":
            echo json_encode(getAllQuestions());
            break;
        case "isEditable":
            if(isset($_POST["QuestionnaireID"]))
                echo json_encode(isEditable($_POST["QuestionnaireID"]));
            break;
        case "saveQuestionnaire":
            if(isset($_POST["dataToSave"]))
                echo json_encode(saveQuestionnaire($_POST["dataToSave"]));
            break;
    }
}


//return assoc array of all active questionnaires
function getQuestionnaires()
{
    $headerRow = array("מס'","שם","קטגוריה");
    $questionnaireDB = new dbConnect\QuestionnaireDB();
    $questionnaires = $questionnaireDB->getAll();
    $ret_arr = array();
    $ret_arr[] = $headerRow;
    foreach($questionnaires as $quest)
    {
        $ret_arr[] = array($quest->getId(),$quest->getName(),$quest->getCategory());
    }
    return $ret_arr;
}

function getQuestions($id)
{
    $headerRow=array("מס'","שאלה");
    $questionnaireDB = new dbConnect\QuestionnaireDB();
    $res_arr=array();
    if($id!=-1)
    {
        $quest_arr=$questionnaireDB->getAllQuestions($id);  
        for($i=0;$i<sizeof($quest_arr);$i++)
        {
            $res_arr[]=array_values($quest_arr[$i]);
        }
    }
    array_unshift($res_arr,$headerRow);//insert $headerRow at the begining of array
    return $res_arr;
}

//function deleteד questionnaire if its not part of any survey
//return true if questionnaire was removed, otherwise return string with all parent surveys
function deleteQuestionnaire($id)
{
    $questionnaireDb=new dbConnect\QuestionnaireDB();
    $parentSurveys=$questionnaireDb->getParentSurveys($id);
    if(sizeof($parentSurveys)==0)
    {
        $questionnaireDb->remove($id);
        return true;
    }
    else
    {
        $msg="לא ניתן למחוק שאלון כי הוא חלק מסקרים: \n";
        for($i=0;$i<sizeof($parentSurveys);$i++)
        {
            $msg.="שם: ".$parentSurveys[$i]['name']." מספר: ".$parentSurveys[$i]['id']."\n";
        }
        return $msg;
    }
}

//get all categories
function getCategories() 
{
    $catDb=new dbConnect\CategoryDB();
    $res=$catDb->getAll("assoc");
    return $res;
}

//get questionnaire by id
function getQuestionnaire($id)
{
    $questionnaireDb=new dbConnect\QuestionnaireDB();
    if($id!=-1)
        $res= $questionnaireDb->getById($id);
    else
    {
        $res=new objects\Questionnaire(" "," ",1,-1);
    }
    return $res;
}

function getAllQuestions()
{
    $headerRow=array("מס'","שאלה");
    $questionDb=new dbConnect\QuestionsDB();
    $res=$questionDb->getAll("assoc");
    $res_arr=array();
    for($i=0;$i<sizeof($res);$i++)
    {
        $res_arr[]=array_values($res[$i]);
    }
    array_unshift($res_arr,$headerRow);//insert $headerRow at the begining of array
    return $res_arr;
}

//function returns true if questionnaire is not part of active survey and editable,
//otherwise return false
function isEditable($id)
{
    $questionnaireDb=new dbConnect\QuestionnaireDB();
    $partOfActive=$questionnaireDb->isPartOfActiveSurvey($id);
    return (!$partOfActive);
}

function saveQuestionnaire($data)
{
    $questionnaireData=json_decode($data);
    $categoryDb=new dbConnect\CategoryDB();
    if(!$categoryDb->checkIfExist('category','name',$questionnaireData->category_name))
    {
        $newCat=new objects\Category($questionnaireData->category_name,1);
        $categoryDb->insert($newCat);
    }
    $questDb=new dbConnect\QuestionnaireDB();
    $questionnaire=new objects\Questionnaire($questionnaireData->name,$questionnaireData->category_name,1,$questionnaireData->id);
    if($questionnaireData->id==-1)
    {
        $id=$questDb->insert($questionnaire);
        $questionnaire->setId($id);
    }
    else
    {
        $questDb->update($questionnaire);
    }
    saveQuestionChanges($questionnaire,$questionnaireData->changes);
    return true;
}

function saveQuestionChanges($questionnaire,$changes)
{
    $questDb=new dbConnect\QuestionnaireDB();
    $questDb->deleteAllQuestions($questionnaire->getID());
    for($i=0;$i<sizeof($changes);$i++)
    {
            $questDb->addQuestion($questionnaire->getID(),$changes[$i]);
    }
}
?>