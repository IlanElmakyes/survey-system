<?php
use Model\Classes\DB_Connections as dbConnect;
use Model\Classes\Data_Objects as objects;
session_start();
require_once $_SESSION['AUTOLOAD_PATH'];
header('Content-Type: application/json');


switch($_POST['title'])
{
    case "Manager-Home":
        echo json_encode(managerHome());
        break;
    case "Manager-Question":
        echo json_encode(managerQuestion());
        break;
    case "Manager-Survey":
        echo json_encode(managerSurvey());
        break;
}
/**
 * creating a nested assoc array with all survey data
 * array will look like -> arr['סקרים']=>array(array("ID","Survey Name"),array(survey1 info),array(survey2 info)...)
 */
function managerSurvey()
{
    $headerRow = array("מס'","שם הסקר");
    $surveyDB = new dbConnect\SurveyDB();
    $surveys = $surveyDB->getAll();
    $ret_arr['סקרים'] = array();
    $ret_arr['סקרים'][] = $headerRow;
    foreach($surveys as $survey)
    {
        $ret_arr['סקרים'][] = array($survey->getId(),$survey->getName());
    }
    return $ret_arr;
}
/**
 * creating a nested assoc array with all question data
 * array will look like -> arr['שאלות']=>array(array(ID,Question Text),array(question1 info),array(question2 info)....)
 */
function managerQuestion()
{
    $headerRow = array("מס'","שאלה");
    $questionDB = new dbConnect\QuestionsDB();
    $questions = $questionDB->getAll();
    $ret_arr["שאלות"] = array();
    $ret_arr["שאלות"][] = $headerRow;
    foreach($questions as $question)
    {
        $ret_arr["שאלות"][] = array($question->getId(),$question->getText());
    }
    return $ret_arr;
}
/**
 * /building the data on all faculties in an assoc array that looks like this:
 * ['faculty_name'=>array('faculty_name','number_active','number_done','number_none','number_classes'),
 *                  array(array=>('class_name',survey_code,survey_status,number_answered),array=>('class_name2',...)),
 *                  
 *  'faculty_name2'=>array('faculty_name','number_active','number_done','number_none','number_classes')
 *                   array(array=>('class_name',...),array=>('class_name2',...)),...]
 * */
function managerHome()
{
    $headerRow = array("כיתה","קוד כניסה","מצב הסקר","מספר המשיבים");
    $activeSurveyDB = new dbConnect\ActiveSurveyDB();
    $facultiesDB = new dbConnect\FacultiesDB();
    $faculties = $facultiesDB->getAll();
    $ret_arr = array();
    foreach($faculties as $faculty)
    {
        $facultyName=$faculty->getName();
        $ret_arr[$facultyName] = array();
        $ret_arr[$facultyName][0]=array();
        $ret_arr[$facultyName][0][]=$facultyName;
        $countDone=0;
        $countActive=0;
        $countNone=0;
        $ret_arr[$facultyName][1][] = $headerRow;
        $classes = $facultiesDB->getFacultyClasses($faculty);
        foreach($classes as $class)
        {
            $activeSurvey = $activeSurveyDB->getActiveSurveyByClassId($class->getId());
            if(!empty($activeSurvey))
            {
                $code = $activeSurvey->getCode();
                $activeSurveyDB->getActiveSurveyStatus($code)>=0?$countActive++:$countDone++;
                $ret_arr[$faculty->getName()][1][] = array($class->getName(),$code,$activeSurveyDB->getActiveSurveyStatus($code)>=0?"פעיל":"הסתיים",$activeSurvey->getNumberAnswered());
            }
            else  {
                $countNone++;
                $ret_arr[$faculty->getName()][1][] = array($class->getName(),"N/A","N/A","N/A");
            }
        }
        $ret_arr[$facultyName][0][]=$countActive;
        $ret_arr[$facultyName][0][]=$countDone;
        $ret_arr[$facultyName][0][]=$countNone;
        $ret_arr[$facultyName][0][]=sizeof($classes);
    }
    return $ret_arr;
}
?>