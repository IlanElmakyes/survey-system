<?php
namespace Controller;
use Model\Classes\DB_Connections\ExcelDB;
use Model\Classes\Data_Objects\Excel;
require_once $_SESSION['AUTOLOAD_PATH'];
\set_time_limit(300);
class ExcelHandler
{
    public static function handleUpload($filePath)
    {
        if(\file_exists($filePath))
        {
            if(Excel::checkFileType($filePath))//file type filter
            {
                $dest_file=$_SESSION['HOME_PATH'].'/uploads/'.basename($filePath);
                if(move_uploaded_file($filePath, $dest_file))//move file from tmp directory
                {
                    $excelReader=new Excel($dest_file);
                    $preparedData=$excelReader->prepareData();
                    if($preparedData!=null)
                    {
                        $extcelToDB=new ExcelDB();
                        $extcelToDB->insertData($preparedData);
                        $_SESSION['RESULT']="עדכון בוצע בהצלחה";
                    }
                    else  $_SESSION['RESULT']="קובץ ריק או פורמט הקובץ לא נכון";
                }
                else $_SESSION['RESULT']="משהו השתבש בהעלת הקובץ...";
            }
            else $_SESSION['RESULT']="סוג קובץ לא נכון";
        }
        else $_SESSION['RESULT']="משהו השתבש בהעלת הקובץ...";
    }
}
?>