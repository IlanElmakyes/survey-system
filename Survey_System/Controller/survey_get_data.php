<?php
namespace Controller;
use Model\Classes\DB_Connections as dbConnect;
use Model\Classes\Data_Objects as objects;
\session_start();
require_once $_SESSION['AUTOLOAD_PATH'];

if(isset($_SESSION['SURVEY_CODE']))
{
    $surveyDb= new dbConnect\ActiveSurveyDB();
    if($surveyDb->checkIfExist('active_survey','code',$_SESSION['SURVEY_CODE']))
    {
        $survey = $surveyDb->getByCode($_SESSION['SURVEY_CODE']);
        $surveyData=$survey->getSurveyData();
        $jsonData=\json_encode($surveyData);
        echo($jsonData);
    }

}
?>