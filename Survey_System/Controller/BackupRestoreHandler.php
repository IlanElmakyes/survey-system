<?php
namespace Controller;
use Model\Classes\DB_Connections as dbConnect;
use Model\Classes\Data_Objects as objects;
if(!isset($_SESSION)) session_start();
require_once $_SESSION['AUTOLOAD_PATH'];
\set_time_limit(300);
 class BackupRestoreHandler
 {
    public const FRESH_DB = "../Model/Backups/DB_FUNDAMENTALS/DB_SEED_INFORMATION.sql";
    public static function backupDatabase()
    {
        $db = new dbConnect\dbConnection();
        $ret = $db->backupDatabase();
        return $ret? $ret:0;
    }
    public static function restoreDatabase($filePath = "")
    {
        if($filePath !== "" && !file_exists($filePath))
            return false;
        if($filePath === "")
            $filePath = dbConnect\dbConnection::getLocalBackupFile();
        $db = new dbConnect\dbConnection();
        return $filePath? $db->restoreDatabase($filePath):false;
    }
 }
?>