<?php
use Model\Classes\DB_Connections as dbConnect;
use Model\Classes\Data_Objects as objects;
session_start();
require_once $_SESSION['AUTOLOAD_PATH'];
header('Content-Type: application/json');

switch($_POST["action"])
{
    case "insert":
        insertData();
        break;
    case "delete":
        deleteData();
        break;
    case "edit":
        editData();
        break;

}
function insertData()
{
    switch($_POST["dataType"])
    {
        case "question":
            echo json_encode(insertNewQuestion($_POST["questionText"]));
            break;
        case "survey":
            echo json_encode(insertNewSurvey(json_decode($_POST["surveyData"])));
            break;
    }
}
function deleteData()
{
    switch($_POST["dataType"])
    {
        case "question":
            echo json_encode(deleteQuestion($_POST["questionId"]));
            break;
        case "survey":
            echo json_encode(deleteSurvey($_POST["surveyId"]));
            break;
    }
}
function editData()
{
    switch($_POST["dataType"])
    {
        case "question":
            echo json_encode(editQuestion($_POST["questionData"]));
            break;
        case "survey":
            echo json_encode(editSurvey(json_decode($_POST["surveyData"])));
    }
}
#region insert functions

function insertNewQuestion($questionText)
{
    $ret_flag = 0;
    try
    {
        if($questionText !== null && $questionText !== "")
        {
            $questionDB = new dbConnect\QuestionsDB();
            $questionDB->insert(new objects\Questions($questionText));
            $ret_flag = 1;
        }
    }
    catch(Exception $e){$ret_flag = 0;}
    return $ret_flag;
}

function insertNewSurvey($surveyData)
{
    $ret_flag = 0;
    $surveyDB = new dbConnect\SurveyDB();
    $survey = new objects\Survey(-1,$surveyData->name,1);
    try
    {
        $survey->setId($surveyDB->insert($survey));
        $surveyDB->addQuestionnairesToSurvey($survey->getId(),$surveyData->questionnaires);
        $ret_flag = 1;
    }
    catch(Exception $e){$ret_flag = 0;}
    return $ret_flag;
}

#endregion

#region delete functions

function deleteQuestion($questionId)
{
    $ret_flag = 0;
    try
    {
        $questionDB = new dbConnect\QuestionsDB();
        $questionDB->remove($questionId);
        $ret_flag = 1;
    }
    catch(Exception $e){$ret_flag = 0;}
    return $ret_flag;
}
/**
 * return -1 if the survey is a part of an active survey that isn't done
 * return 0 if the delete failed for any other reason
 * return 1 if the delete was successful
 */
function deleteSurvey($surveyId)
{
    $ret_flag = 0;
    try
    {
        $surveyDB = new dbConnect\SurveyDB();
        $activeSurveyDB = new dbConnect\ActiveSurveyDB();
        $activeSurvey = $activeSurveyDB->getBySurveyId($surveyId);
        foreach($activeSurvey as $survey)
        {
            if(isset($survey) && $activeSurveyDB->getActiveSurveyStatus($survey->getCode()))
            {
                if($activeSurveyDB->getActiveSurveyStatus($survey->getCode()) == dbConnect\ActiveSurveyDB::ACTIVE)
                    throw new Exception("active");
            }
            else throw new Exception;
        }
        
        $surveyDB->remove($surveyId);
        $ret_flag = 1;
    }
    catch(Exception $e){$e->getMessage() == "active"? $ret_flag = -1:$ret_flag=0;}
    return $ret_flag;
}
#endregion

#region edit Functions

function editQuestion($questionData)
{
    $ret_flag = false;
    if(intval($questionData[0]) > 0)
    {
        try
        {
            $questionDB = new dbConnect\QuestionsDB();
            $questionDB->update($questionData[0],$questionData[1]);
            $ret_flag = true;
        }
        catch(Exception $e){$ret_flag = false;}
        return $ret_flag? 1:0;
    }
}

function editSurvey($surveyData)
{
    $ret_flag = 0;
    $surveyDB = new dbConnect\SurveyDB();
    $survey = $surveyDB->getSurveyById($surveyData->id);
    $survey->setName($surveyData->name);
    try
    {
        $surveyDB->updateSurvey($survey,$surveyData->questionnaires);
        $ret_flag = 1;
    }
    catch(Exception $e){$ret_flag = 0;}
    return $ret_flag;
}

#endregion

?>