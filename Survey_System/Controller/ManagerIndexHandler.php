<?php
use Model\Classes\DB_Connections as dbConnect;
use Model\Classes\Data_Objects as objects;
session_start();
require_once $_SESSION['AUTOLOAD_PATH'];
header('Content-Type: application/json');

switch($_POST['managerIndexAction'])
{
    case "getAllSurveys":
        echo json_encode(getAllSurveys());
        break;
    case "activateSurvey":
        echo json_encode(activateSurvey(json_decode($_POST['surveyData'])));
        break;
}

function getAllSurveys()
{
    $surveyDB = new dbConnect\SurveyDB();
    $ret_arr['רשימת סקרים'] = array();
    $ret_arr['רשימת סקרים'][] = array("מס'","שם");
    $surveys = $surveyDB->getAll();
    foreach($surveys as $survey)
    {
        $ret_arr['רשימת סקרים'][] = array($survey->getId(),$survey->getName());
    }
    return $ret_arr;
}

//function activates chosen survey for given classes
function activateSurvey($surveyData)
{
    $ret_flag = 0;
    try
    {
        $activeSurveyDB = new dbConnect\ActiveSurveyDB();
        $classesDB = new dbConnect\ClassesDB();
        foreach($surveyData->classesNames as $className)
        {
            $activeSurvey = $activeSurveyDB->getByClassName($className);
            if(isset($activeSurvey))
            {
                //if class has an active survey, and it's still inactive, delete it and create a new one
                if($activeSurveyDB->getActiveSurveyStatus($activeSurvey->getCode()) == dbConnect\ActiveSurveyDB::INACTIVE)
                {
                    $primKey['dateTime'] = $activeSurvey->getDate();
                    $primKey['classId'] = $activeSurvey->getClassId();
                    $activeSurveyDB->remove($primKey);
                }
                //if a class has an active survey that is now live, do nothing 
                else if($activeSurveyDB->getActiveSurveyStatus($activeSurvey->getCode()) == dbConnect\ActiveSurveyDB::ACTIVE)
                    continue;
                //and if a class has an active survey that's already done, create a new one
            }
            //if class does not have an active survey, create it and insert it to the DB
            $activeSurvey = new objects\ActiveSurvey($classesDB->getByName($className)->getId(),$surveyData->surveyId);
            $activeSurveyDB->insert($activeSurvey);
        }
        $ret_flag = 1;
    }
    catch(Exception $e){var_dump($e); $ret_flag=0;}
    return $ret_flag;
}
?>