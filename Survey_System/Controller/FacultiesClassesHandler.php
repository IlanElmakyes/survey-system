<?php
use Model\Classes\DB_Connections as dbConnect;
use Model\Classes\Data_Objects as objects;
session_start();
require_once $_SESSION['AUTOLOAD_PATH'];
header('Content-Type: application/json');

if(isset($_POST["FacultiesAction"]))
{
    switch($_POST["FacultiesAction"])
    {
        case "getFaculties":
            echo json_encode(getFaculties());
            break;
        case "getClasses":
            if(isset($_POST["FacultyId"]))
                echo json_encode(getClasses($_POST["FacultyId"]));
            break;
        case "getTeachers":
            if(isset($_POST["ClassId"]))
                echo json_encode(getTeachers($_POST["ClassId"]));
            break;
        case "addNewFaculty":
            if(isset($_POST["FacultyName"]))
                echo json_encode(addFaculty($_POST["FacultyName"]));
            break;
        case "deleteFaculty":
            if(isset($_POST["FacultyId"]))
                echo json_encode(deleteFaculty($_POST["FacultyId"]));
            break;
        case "editFaculty":
            if(isset($_POST["FacultyId"]))
                echo json_encode(editFaculty($_POST["FacultyId"]));
            break;
        case "deleteClass":
            if(isset($_POST["ClassId"]))    
                echo json_encode(deleteClass($_POST["ClassId"]));
            break;
        case "updateClass":
            if(isset($_POST["dataToSave"]))
                echo json_encode(updateClass($_POST["dataToSave"]));
            break;
        case "addNewClass":
            if(isset($_POST["dataToSave"]))
                echo json_encode(addNewClass($_POST["dataToSave"]));
            break;
        case "editFacultyName":
            if(isset($_POST["dataToSave"]))
                echo json_encode(editFacultyName($_POST["dataToSave"]));
            break;
    }
}

//function returns all faculties in format needed to build table
function getFaculties()
{
    $facultyDb=new dbConnect\FacultiesDB();
    $allFaculties=$facultyDb->getAll();
    $headerRow=array("מס","שם");
    $ret_arr[]=$headerRow;
    for($i=0;$i<sizeof($allFaculties);$i++)
    {
        $ret_arr[]=array($allFaculties[$i]->getId(),$allFaculties[$i]->getName());
    }
    return $ret_arr;
}

//function get all classes of given faculty
function getClasses($id) 
{
    $facultyDb=new dbConnect\FacultiesDB();
    $faculty=$facultyDb->getById($id);
    $classes=$facultyDb->getFacultyClasses($faculty);
    $headerRow=array("מס'","שם");
    $ret_arr[]=$headerRow;
    for($i=0;$i<sizeof($classes);$i++)
    {
        $ret_arr[]=array($classes[$i]->getId(),$classes[$i]->getName());
    }
    return $ret_arr;
}

//function returns all teachers for given class
function getTeachers($id)
{
    $classDb=new dbConnect\ClassesDB();
    $teachers=$classDb->getAllTeachers($id);
    $headerRow=array("מס'","שם");
    $ret_arr[]=$headerRow;
    for($i=0;$i<sizeof($teachers);$i++)
    {
        $ret_arr[]=array($teachers[$i]['id'],$teachers[$i]['name']);
    }
    return $ret_arr;
}

//function adds new faculty if there is no faculty with this name,
//or retrive faculty if it has same name instead of createing new
//and return apropriate messages
function addFaculty($newFaculty)
{
    $facultyDb = new dbConnect\FacultiesDB();
    if(!$facultyDb->checkIfExist("faculties","name",$newFaculty))
        {
            $faculty=new objects\Faculties(-1,$newFaculty,1);
            $facultyDb->insert($faculty);
            return 0;
        }
    else
    {
        $facultyCheck=$facultyDb->getByName($newFaculty,true);
        if($facultyCheck->getStatus()==0)
        {
            $facultyCheck->setStatus(1);
            $facultyDb->update($facultyCheck);
            return "בוצע שיחזור מגמה";
        }
        else
            return "לא ניתן להוסיף מגמה כי קיימת מגמה בשם זהה";
    }
}


//function updates given faculty name if new name is different from current
//and if there is now other facuty with same name
//return 0 if name was updated or if new name same as current
//return error mesagges in other cases
function editFacultyName($data)
{
    $facultyData=json_decode($data);
    $facultyDb=new dbConnect\FacultiesDB();
    $facultyUpdate=$facultyDb->getById($facultyData->id);
    if($facultyUpdate->getName()==$facultyData->name)
    {
        return 0;
    }
    else if(!$facultyDb->checkIfExist("faculties","name",$facultyData->name))
    {
        $facultyUpdate->setName($facultyData->name);
        $facultyDb->update($facultyUpdate);
        return 0;
    }
    else
    {
        $facultyCheck=$facultyDb->getByName($facultyData->name,true);
        if($facultyCheck->getStatus()==1)
        {
            return "לא ניתן לעדכן שם כי קיימת מגמה בשם זהה";
        }
        else if($facultyCheck->getStatus()==0)
        {
            return "מגמה בשם הזה נמחקה, ניתן לשחזר אותה בהוספת מגמה חדשה";
        }
    }
}

//function set status 0 to faculty and all its classes if none of them participating in active survey
//otherwise return message with list of classes from this faculty that are part of active survey
function deleteFaculty($id)
{
    $facultyDb = new dbConnect\FacultiesDB();
    $classDb=new dbConnect\ClassesDB();
    $faculty=$facultyDb->getById($id);
    $classes=$facultyDb->getFacultyClasses($faculty);
    $count=0;
    $msg="";
    for($i=0;$i<sizeof($classes);$i++)
    {
        if($classDb->isPartOfActiveSurvey($classes[$i]->getId()))
            {
                $count++;
                $msg.=$classes[$i]->getName()."\n";
            }
    }
    if($count>0)
    {
        $msgStart="לא ניתן למחוק מגמה כי ".$count." מהכיתות משתתפות בסקר פעיל:\n";
        return $msgStart.$msg;
    }
    else
    {
        for($i=0;$i<sizeof($classes);$i++)
        {
            $classDb->remove($classes[$i]->getId());
        }
        $facultyDb->remove($id);
        return 0;
    }
}

//function returns array with result status 0 at index 0 and faculty info at index 1 if its not part of active survey
//otherwise return status 1 at index 0 and error message at index 1
function editFaculty($id)
{
    $facultyDb = new dbConnect\FacultiesDB();
    $classDb=new dbConnect\ClassesDB();
    $faculty=$facultyDb->getById($id);
    $classes=$facultyDb->getFacultyClasses($faculty);
    $count=0;
    $res_arr=array();
    $msg="";
    for($i=0;$i<sizeof($classes);$i++)
    {
        if($classDb->isPartOfActiveSurvey($classes[$i]->getId()))
            {
                $count++;
                $msg.=$classes[$i]->getName()."\n";
            }
    }
    if($count>0) 
    {
        $res_arr[]=1;
        $msgStart="לא ניתן לערוך מגמה כי ".$count." מהכיתות משתתפות בסקר פעיל:\n";
        $res_arr[]=$msgStart.$msg;
    }      
    else
    {
        $res_arr[]=0;
        $res_arr[]=$faculty;
    }
    return $res_arr;
}

//remove class by id
function deleteClass($classId)
{
    $classDb=new dbConnect\ClassesDB();
    $classDb->remove($classId);
    return 0;
}

function updateClass($data)
{
    $classData=json_decode($data);
    $classDb=new dbConnect\ClassesDB();
    $classUpdate=$classDb->getById($classData->id);
    if($classUpdate->getName()==$classData->name)
    {
        return 0;
    }
    else if(!$classDb->checkIfExist("classes","name",$classData->name))
    {
        $classUpdate->setName($classData->name);
        $classDb->update($classUpdate);
        return 0;
    }
    else
    {
        $classCheck=$classDb->getByName($classData->name,true);
        if($classCheck->getStatus()==1)
        {
            return "לא ניתן לעדכן שם כי קיימת כיתה בשם זהה";
        }
        else if($classCheck->getStatus()==0)
        {
            return "כיתה בשם הזה נמחקה, ניתן לשחזר אותה בהוספת כיתה חדשה";
        }
    }
}

function addNewClass($data)
{
    $classData=json_decode($data);
    $classDb=new dbConnect\ClassesDB();
    $classToAdd=new objects\Classes(-1,$classData->name,$classData->facultyId,1);

    if(!$classDb->checkIfExist("classes","name",$classData->name))
    {
        $classDb->insert($classToAdd);
        return 0;
    }
    else
    {
        $classCheck=$classDb->getByName($classData->name,true);
        if($classCheck->getStatus()==1)
        {
            return "לא ניתן להוסיף כיתה כי קיימת כיתה בשם זהה";
        }
        else if($classCheck->getStatus()==0&&$classCheck->getFacultyId()==$classToAdd->getFacultyId())
        {
            $classCheck->setStatus(1);
            $classDb->update($classCheck);
            return "בוצע שיחזור כיתה";
        }
        else return "לא ניתן להוסיף כיתה";
    }
}