<?php
use Model\Classes\DB_Connections as dbConnect;
use Model\Classes\Data_Objects as objects;
session_start();
require_once $_SESSION['AUTOLOAD_PATH'];
header('Content-Type: application/json');

if(isset($_POST["TeachersAction"]))
{
    switch($_POST["TeachersAction"])
    {
        case "getTeachers":
            echo json_encode(getTeachers());
            break;
        case "getClasses":
            if(isset($_POST["TeacherId"]))
                echo json_encode(getClasses($_POST["TeacherId"]));
            break;
        case "deleteTeacher":
            if(isset($_POST["TeacherId"]))
                echo json_encode(deleteTeacher($_POST["TeacherId"]));
            break;
        case "editTeacher":
            if(isset($_POST["TeacherId"]))
                echo json_encode(editTeacher($_POST["TeacherId"]));
            break;
        case "addNewTeacher":
            if(isset($_POST["TeacherName"]))
                echo json_encode(addTeacher($_POST["TeacherName"]));
            break;
        case "updateTeacher":
            if(isset($_POST["dataToSave"]))
                echo json_encode(updateTeacher($_POST["dataToSave"]));
            break;
    }
}

//function returns all teachers
function getTeachers()
{
    $teacherDb=new dbConnect\TeachersDB();
    $allTeachers=$teacherDb->getAll();
    $headerRow=array("מס'","שם");
    $ret_arr[]=$headerRow;
    for($i=0;$i<sizeof($allTeachers);$i++)
    {
        $ret_arr[]=array($allTeachers[$i]->getId(),$allTeachers[$i]->getName());
    }
    return $ret_arr;
}


//function returns all classes of given teacher
function getClasses($id) 
{
    $teacherDb=new dbConnect\TeachersDB();
    $allClasses=$teacherDb->getClasses($id);
    $headerRow=array("מס'","מגמה","כיתה");
    $ret_arr[]=$headerRow;
    for($i=0;$i<sizeof($allClasses);$i++)
    {
        $ret_arr[]=array($allClasses[$i]['class_id'],$allClasses[$i]['faculty_name'],$allClasses[$i]['class_name']);
    }
    return $ret_arr;
}

//function removes teacher if there is no classes connected to that teacher
function deleteTeacher($id)
{
    $teacherDb=new dbConnect\TeachersDB();
    $classes=$teacherDb->getClasses($id);
    if(sizeof($classes)>0)
        return "לא ניתן למחוק מרצה כי קיימות כיתות מקושרות";
    else
    {
        $teacherDb->remove($id);
        return 0;
    }
}

function editTeacher($id)
{
    $teacherDb=new dbConnect\TeachersDB();
    $classes=$teacherDb->getClasses($id);
    $classDb=new dbConnect\ClassesDB();
    if(sizeof($classes)>0)
    {
        $msg="";
        $count=0;
        for($i=0;$i<sizeof($classes);$i++)
        {
            if($classDb->isPartOfActiveSurvey($classes[$i]['class_id']))
            {
                $count++;
                $msg.="כיתה ".$classes[$i]['class_name']." במגמת ".$classes[$i]['faculty_name']."\n";
            }
        }
        if($count>0)
        {
            $res[]="לא ניתן לעדכן פרטי מרצה כי ".$count." כיתות השייכות למרצה משתתפות בסקר פעיל:\n".$msg;
            return $res;
        }
    }
    $res[]=0;
    $res[]=$teacherDb->getById($id);
    $res[]=getClasses($id);
    $res[]=getAllClasses();
    return $res;
}

function addTeacher($teacherName)
{
    $teacherDb=new dbConnect\TeachersDB();
    if(!$teacherDb->checkIfExist("teachers","name",$teacherName))
        {
            $teacher=new objects\Teachers(-1,$teacherName,1);
            $teacherDb->insert($teacher);
            return 0;
        }
    else
    {
        $teacherCheck=$teacherDb->getByName($teacherName,true);
        if($teacherCheck->getStatus()==0)
        {
            $teacherCheck->setStatus(1);
            $teacherDb->update($teacherCheck);
            return "בוצע שחזור מרצה";
        }
        else
            return "לא ניתן להוסיף מרצה כי קיימ/ת מרצה בשם זהה";
    }
}

function updateTeacher($data)
{
    $teacherData=json_decode($data);
    $teacherDb=new dbConnect\TeachersDB();
    $teacher=new objects\Teachers($teacherData->id,$teacherData->name,1);
    if($teacherDb->checkIfExist("teachers","name",$teacher->getName()))
    {
        $teacherCheck=$teacherDb->getByName($teacher->getName(),true);
        if($teacherCheck->getId()!=$teacher->getId())
            return "לא ניתן לשמור שינויים כי קיים\ת או היה מרצה בשם זהה.\nניתן לשחזר מרצה שנמחק בעזרת הוספת מרצה";
    }
    $teacherDb->update($teacher);
    $teacherDb->removeFromAllClasses($teacher->getId());
    $classDb=new dbConnect\ClassesDB();
    for($i=0;$i<sizeof($teacherData->classes);$i++)
    {
        $classDb->addTeacherToClass($teacherData->classes[$i],$teacher->getId());
    }
    return 0;
}

function getAllClasses()
{
    $classDb=new dbConnect\ClassesDB();
    $allClasses=$classDb->getAllForTable();
    $headerRow=array("מס'","מגמה","כיתה");
    $ret_arr[]=$headerRow;
    for($i=0;$i<sizeof($allClasses);$i++)
    {
        $ret_arr[]=array($allClasses[$i]['class_id'],$allClasses[$i]['faculty_name'],$allClasses[$i]['class_name']);
    }
    return $ret_arr;
}