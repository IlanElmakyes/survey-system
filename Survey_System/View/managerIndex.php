<?php
namespace View;
session_start();
if(!isset($_SESSION['MANAGER']))
    header("Location: index.php");
require $_SESSION['AUTOLOAD_PATH'];
?>
<html dir='rtl'>
    <meta charset='UTF-8'>
    <head>
        <script src="JS_Scripts\tableHandling_script.js"></script>
        <script src="JS_Scripts\managerIndex_script.js"></script>
        <script src="JS_Scripts\formHandling_script.js"></script>
        <script src="JS_Scripts\managerSurveys_script.js"></script>
        <script src="JS_Scripts\spinnerActions.js"></script>
        <link rel="stylesheet" href="CSS\spinner.css">
        <link rel="stylesheet" href="CSS\manager_pages.css">													   
        <link rel="stylesheet" href="CSS\clickable_elements.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <title>Manager-Home</title>
    </head>
    <body onload="initializeIndexPage()">
        <header>
            <div class="spinner"></div>
            <h2 class="header_inline">סטטוס והפעלת סקרים</h2>
            <hr class="header_block">
        </header>
        <?php echo (Navigation::generateNav());?>
        <main>
            <div id="table_div" class="main_block">
            <table id="main_table">
            <tr>
                <td class="main_table_th">מגמה</td>
                <td class="main_table_th">כיתות שעונים לסקר</td>
                <td class="main_table_th">כיתות שסיימו לענות</td>
                <td class="main_table_th">כיתות ללא סקר</td>
                <td class="main_table_th">סה"כ כיתות</td>
            </table>
            </div>
            <div id="survey_activation" class="main_block">
                <button type="button" class="open_form" id="survey_activation_open_button" onclick="activateSurveyPopupOpen('activationForm','survey_activation_open_button')">הפעל סקר</button>
                <form action="#" class="popup_form" id="activationForm" style="display:none;">
                </form>
            </div>
        </main>
        <footer></footer>    
    </body>
</html>