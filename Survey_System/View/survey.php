<?php
namespace View;
use Model\Classes\DB_Connections as dbConnect;
use Model\Classes\Data_Objects as objects;

\session_start();

if(isset($_SESSION['SURVEY_CODE']))
{
    require_once $_SESSION['AUTOLOAD_PATH'];
    $surveyDb= new dbConnect\ActiveSurveyDB();
    if($surveyDb->checkIfExist('active_survey','code',$_SESSION['SURVEY_CODE']))
    {
        echo("
    <html dir='rtl'>
        <head>
            <script src='JS_Scripts".DIRECTORY_SEPARATOR."survey_script.js'></script>
            <script src='JS_Scripts".DIRECTORY_SEPARATOR."spinnerActions.js'></script>
            <link rel='stylesheet' href='CSS".DIRECTORY_SEPARATOR."survey_style.css'>
            <link rel='stylesheet' href='CSS".DIRECTORY_SEPARATOR."spinner.css'>
            <link rel='stylesheet' href='https://fonts.googleapis.com/icon?family=Material+Icons'> 
            <meta name='viewport' content='width=device-width, initial-scale=1'>
        </head>
        <meta charset='utf-8'>
        <body>
            <main>
            <div class='main_block'>
            <p class='category'></p><hr id='line'>
            <div id='errmsg_div' style='display : none;'>
            </div>
                <div id='toggle_div'>
                <input type='checkbox' id='skip_toggle'>
                <label for='skip_toggle'>דלג על השאלון</label>
                </div>
            <table id='questionnaire'>
                <tr>
                <th></th>
                <th>1</th>
                <th>2</th>
                <th>3</th>
                <th>4</th>
                <th>5</th>
                </tr>
            </table>
            <div id='text_input'>
                <p>הערות:</p>
                <div id='char_count'>0/240</div>
                <textarea id='comment' maxLength='240' onkeyup='counter(this);'></textarea>  
            </div>
            <div id='nav_buttons'>             
                <button id='btn_prev'>קודם</button> 
                <button id='btn_next'>הבא</button>                          
            </div>
            <div id='finish'  style='display: none;'>
                <p id='finish_choice'>האם ברצונך לסיים את הסקר ולשלוח תוצאות או לחזור חזרה לסקר?</p>
                <div id='choice_buttons'>
                    <button id='btn_return'>חזרה לסקר</button> 
                    <button id='btn_send'>סיים ושלח</button> 
                </div>
            </div>
            </div>
            </main>
        </body>
    </html>");
    }
}
else header('Location: index.php');
?>