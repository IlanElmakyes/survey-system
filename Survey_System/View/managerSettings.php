<?php
namespace View;
session_start();
if(!isset($_SESSION['MANAGER']))
    header("Location: index.php");
require $_SESSION['AUTOLOAD_PATH'];


?>
<html dir='rtl'>
    <meta charset='UTF-8'>
    <head>
        <title>Settings</title>
        <link rel="stylesheet" href="CSS\clickable_elements.css">
        <link rel="stylesheet" href="CSS\manager_pages.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="CSS\spinner.css">
        <script src="JS_Scripts\settings_script.js"></script>
        <script src="JS_Scripts\spinnerActions.js"></script>
    </head>
    <body onload="pageInitialize()">
        <header>
            <div class="spinner"></div>
            <h2 class="header_inline">הגדרות</h2>
            <hr class="header_block">
        </header>
        <?php echo (Navigation::generateNav());?>
        <main>
            <div id="timer_set_div" class="main_block">
            <h3>טיימר עבור סקר פעיל</h3>
            <p>טיימר זה מגדיר במשך כמה זמן יהיה אפשר להתחבר לסקר אחרי כניסה ראשונה. הגדרה נוכחית:</p>
            <p id="current_timer_setting"></p>
            <p>הגדרה חדשה:</p> 
            <div id="new_timer_setting">
            <label>ימים:</label> 
            <input type="number" id="days" value=0 min="0" max="99" size="4" required oninvalid='timerValidation(event)'>
            <label>שעות:</label> 
            <input type="number" id="hours" value=0 min="0" max="23" size="4" required oninvalid='timerValidation(event)'>
            <label>דקות:</label> 
            <input type="number" id="minutes" value=0 min="0" max="59" size="4" required oninvalid='timerValidation(event)'>
            <button type="button" id="set_timer_btn">עדכן</button>
            </div>

            <h3>החלפת שם משתמש וסיסמא</h3>
            <button type="button" id="btn_openNewCredentialsModal">החלף שם משתמש וסיסמה</button>
            <div id="changeCredentialsModal" class="modal">
                <div class ="modalWrapper">
                    <div class="modalContent">
                        <form action="#" id="changeCredentialsForm" class="modalForm" method="post">
                            <label for="oldUsernameInput">הכנס שם משתמש נוכחי:</label>
                            <input type="text" id="oldUsernameInput" class="inputField" placeholder="Current Username">
                            <br>
                            <label for="oldPasswordInput">הכנס סיסמה נוכחית:</label>
                            <input type="password" id="oldPasswordInput" class="inputField" placeholder="Current Password" required>
                            <br><br>
                            <label for="newUsernameInput">הכנס שם משתמש חדש(אופציונלי):</label>
                            <input type="text" id="newUsernameInput" class="inputField" placeholder="New Username">
                            <br>
                            <label for="newPasswordInput">הכנס סיסמה חדשה:</label>
                            <input type="password" id="newPasswordInput" class="inputField" placeholder="New Password">
                            <br>
                            <label for="confirmPassword">אמת סיסמה חדשה:</label>
                            <input type="password" id="confirmPasswordInput" class="inputField" placeholder="Confirm New Password">
                            <br>
                            <button type="button" id="btn_closeNewCredentialsModal" class="modalClose">סגור</button>
                            <button id="btn_newCredentials">שמור שינויים</button>
                            </form>
                        </div>
                    </div>
            </div>
        </main>
        <footer></footer>
    </body>
</html>