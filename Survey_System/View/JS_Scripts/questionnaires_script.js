const MAX_NAME_LEN = 50;
/**
 * 
 * @param {the table id} tableId 
 * function called on page body load
 * initialize all page elements:
 * - fill questionnaires table with data and delte/edit symbols
 *      and add events for delete/edit and view 
 * -set all divs not visible except for questionnaires table div
 * -set buttons events
 */
function pageInitialize() {
    xhttpPostRequest("QuestionnaireAction=getQuestionnaires", buildQuestionnairesTable);
    document.getElementById("new_questionnaire_btn").addEventListener('click', function () {
        createNewQuestionnaire();
    });
    document.getElementById("questions_div").style.display = "none";
    document.getElementById("allquestions_div").style.display = "none";
}

/**
 * 
 * @param {the table ID} tableId 
 * function fills questionnaires table and adds eidt/delete buttons with events
 * and click event on each row to view questinnaire details
 */
function buildQuestionnairesTable(data) {

    let table = document.getElementById("questionnaires_table");
    fillTable(table, data);
    //setting click eventHandler on th tags
    table.addEventListener('click', function (e) {
        if (e.target.nodeName.toUpperCase() !== "TH") return;
        header_click(e.target);
    });
    addDeleteEdit(table,
        function (tableCell) {//delete onclick function
            deleteQuestionnaire(tableCell.parentElement.cells[0].innerHTML);
        },
        function (tableCell) {//edit onclick function
            editQuestionnaire(tableCell.parentElement.cells[0].innerHTML);
        });
    addViewMore(table, onViewMoreClick);
    correctHeader(table);
    addSearchBar(table);
}

/**
 * 
 * @param {object} cell- DOM cell (td) 
 *function called on button "view more" click
 * reset questions_div - set header, create and fill questions table, create and set buttons
 */
function onViewMoreClick(cell) {
    if (cell === null)
        return;
    var row = getParentTable(cell).rows[cell.parentElement.rowIndex];//var row accessable in anonym function passed to xhttp request
    let params = "QuestionnaireAction=getQuestions&QuestionnaireID=" + row.cells[0].innerHTML;
    xhttpPostRequest(params, function (data) {//this will be executed after we get data from server
        let questDiv = document.getElementById("questions_div");
        cleanElement(questDiv);
        let divHeader = document.createElement("h3");
        divHeader.innerHTML = "שאלון " + row.cells[1].innerHTML + " בקטגוריה " + row.cells[2].innerHTML;
        let questTable = document.createElement("table");
        questTable.id = "questions_table";
        fillTable(questTable, data);
        correctHeader(questTable);
        questDiv.appendChild(divHeader);
        questDiv.appendChild(questTable);
        addQuestionDivBtns(questDiv);
        btns = questDiv.getElementsByTagName("button");
        for (let i = 0; i < btns.length; i++) {
            btns[i].value = row.cells[0].innerHTML;
        }
        questDiv.style.display = "";
        document.getElementById("allquestions_div").style.display = "none";
    });
}

//create and initialize buttons for questions_div
function addQuestionDivBtns(div) {
    let btns = [
        {
            id: "delete_btn",
            text: "מחק",
            func: function () {
                deleteQuestionnaire(this.value);
            }
        },
        {
            id: "edit_btn",
            text: "ערוך",
            func: function () {
                editQuestionnaire(this.value);
            }
        },
        {
            id: "close_btn",
            text: "סגור",
            func: function () { div.style.display = "none"; }
        }
    ];
    for (let i = 0; i < btns.length; i++) {
        let btn = document.createElement("button");
        btn.id = btns[i].id;
        btn.innerHTML = btns[i].text;
        btn.addEventListener('click', btns[i].func);
        div.appendChild(btn);
    }
}

/**
 * Function removes all childs of element
 * @param {object} element - DOM element to be cleaned
 * 
 */
function cleanElement(element) {
    while (element.firstChild) {
        element.removeChild(element.firstChild);
    }
}
/**
 *
 * @param {string} params - string with parametrs to be send in request
 * @param {function} successFunc - function will be caled when request successfully finished
 * request data from  "../Controller/QuestionnairesHandler.php"
 */
function xhttpPostRequest(params, successFunc) {
    spinnerStart();
    let xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            let data = JSON.parse(this.response);
            successFunc(data);
            spinnerStop();
        }
    };
    xhttp.open("POST", "../Controller/QuestionnairesHandler.php", true);
    xhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhttp.send(params);
}

/**
 * function makes request to server to delete questionnaire by id
 * if questionnaire was deleted - update page elements
 * otherwise alert message that questionnaire can not be removed since its part of list of surveys
 * @param {string} id - questionnaire id to be deleted
 */
function deleteQuestionnaire(id) {
    if (confirm("למחוק את השאלון?")) {
        let params = "QuestionnaireAction=deleteQuestionnaire&QuestionnaireID=" + id;
        xhttpPostRequest(params, function (res) {
            if (res == true) {
                let table = document.getElementById("questionnaires_table");
                deleteRow(table, id);
                document.getElementById("questions_div").style.display = "none";
                document.getElementById("allquestions_div").style.display = "none";
            }
            else {
                alert(res);
            }
        });
    }
}

//build all elements needed to edit questionnaire if questionnaire not part of active survey
function editQuestionnaire(id) {
    let params = "QuestionnaireAction=isEditable&QuestionnaireID=" + id;
    xhttpPostRequest(params, function (isEditable) {
        if (isEditable) {
            div = document.getElementById("questions_div");
            cleanElement(div);
            buildEditDiv(div, id);
            div.style.display = "";
            divQuest = document.getElementById("allquestions_div");
            cleanElement(divQuest);
            buildAllQuestionsDiv();
            divQuest.style.display = "";
            addEditButnsEvents(id);
        }
        else alert("שאלון זה משתתף בסקר פעיל ולא ניתן לשינוי כעת");
    });

}

function createNewQuestionnaire() {
    let id = -1;
    div = document.getElementById("questions_div");
    cleanElement(div);
    buildEditDiv(div, id);
    div.style.display = "";
    divQuest = document.getElementById("allquestions_div");
    cleanElement(divQuest);
    buildAllQuestionsDiv();
    divQuest.style.display = "";
    addEditButnsEvents(id);
}

//function builds edit qiustionnaire div
function buildEditDiv(div, id) {
    div.appendChild(buildEditHeader(id));
    table = buildEditQuestionnaireTable(id);
    div.appendChild(table);
    removeBtn = document.createElement("button");
    removeBtn.innerHTML = "הסר";
    removeBtn.id = "remove_btn";
    div.appendChild(removeBtn);
    div.appendChild(createEditBtnsDiv());
}



function createEditBtnsDiv() {
    btnsDiv = document.createElement("div");
    btnsDiv.id = "btns_holder_div";
    saveBtn = document.createElement("button");
    saveBtn.id = "save_btn";
    saveBtn.innerHTML = "שמור";
    closeBtn = document.createElement("button");
    closeBtn.id = "cancel_btn";
    closeBtn.innerHTML = "בטל וסגור";
    btnsDiv.appendChild(saveBtn);
    btnsDiv.appendChild(closeBtn);
    return btnsDiv;
}
/**
 * function builds table with questions of specific questionnaire and checkboxes to allow editing of this questionnaire 
 * @param {object} row - selected questionnaire in questionnaires table
 */
function buildEditQuestionnaireTable(id) {
    var questTable = document.createElement("table");
    questTable.id = "questions_table";
    let params = "QuestionnaireAction=getQuestions&QuestionnaireID=" + id;//get questions for this questionnaire
    xhttpPostRequest(params, function (data) {//this will be executed after we get data from server
        fillTable(questTable, data);
        addCheckBox(questTable);
        addSearchBar(questTable);
    });
    return questTable;
}
//function builds header div for edit questionnaire div
//header div contains select for categories with preselected category and text input for name
function buildEditHeader(id) {
    headerDiv = document.createElement("div");
    headerDiv.id = "edit_header";
    let params = "QuestionnaireAction=getQuestionnaire&QuestionnaireID=" + id;
    xhttpPostRequest(params, function (quest) {
        nameLbl = document.createElement("label");
        nameLbl.innerHTML = "שם שאלון:";
        nameInput = document.createElement("input");
        nameInput.id = "quest_name_input";
        nameInput.type = "text";
        nameInput.required=true;
        nameInput.setAttribute("maxLength",MAX_NAME_LEN);
        nameInput.pattern='^.*\\S+.*$';
        nameInput.addEventListener('invalid',nameValidation);
        nameInput.value = quest.name;
        let selectCat = createCategoryDiv(quest.category_name);
        headerDiv.appendChild(nameLbl);
        headerDiv.appendChild(nameInput);
        headerDiv.appendChild(selectCat);
    });
    return headerDiv;
}

function createCategoryDiv(category_name) {
    let div = document.createElement("div");
    div.id = "category_div";
    div.innerHTML = "<div><input type='radio' id='radio_exist' name='category' value='exist' checked='true'><label for='radio_exist'>קטגוריה קיימת</label>";
    div.innerHTML += "<select id='category_select'></div>";
    div.innerHTML += "<div><input type='radio' id='radio_new' name='category' value='new'><label for='radio_new'>קטגוריה חדשה</label>";
    div.innerHTML+="<input type='text' id='new_category_input' required pattern='^.*\\S+.*$' maxLength=50 oninvalid='nameValidation(event)'></div>";
    buildSelectCategory(category_name);
    return div;
}

/**
 * function creates select element with all categries and preselected given category
 * @param {string} category 
 */
function buildSelectCategory(category) {
    xhttpPostRequest("QuestionnaireAction=getCategories", function (data) {
        let selectCat = document.getElementById("category_select");
        for (let i = 0; i < data.length; i++) {
            let opt = document.createElement("option");
            opt.text = data[i].name;
            opt.value = data[i].name;
            if (data[i].name == category)
                opt.selected = true;
            selectCat.appendChild(opt);
        }
    });
}
/**
 * Delete row from table 
 * @param {object} table 
 * @param {string} id - id is value stored in first column (cells[0])
 */
function deleteRow(table, id) {
    for (let i = 0; i < table.rows.length; i++) {
        if (table.rows[i].cells[0].innerHTML == id)
            table.deleteRow(i);
    }
}

//function builds div for all questions
function buildAllQuestionsDiv() {
    let allQuestDiv = document.getElementById("allquestions_div");
    let table = document.createElement("table");
    table.id = "allquestions_table";
    allQuestDiv.appendChild(table);
    xhttpPostRequest("QuestionnaireAction=getAllQuestions", buildAllQuestionsTable);
    let addQuestionBtn = document.createElement("button");
    addQuestionBtn.id = "add_question_btn";
    addQuestionBtn.innerHTML = "הוסף";
    allQuestDiv.appendChild(addQuestionBtn);
}

//function builds table with all questions pool
function buildAllQuestionsTable(data) {
    let table = document.getElementById("allquestions_table");
    fillTable(table, data);
    table.addEventListener('click', function (e) {
        if (e.target.nodeName.toUpperCase() !== "TH") return;
        header_click(e.target);
    });
    addCheckBox(table);
    addSearchBar(table);
}

//function adds events to all buttons neede to edit questionnaire
function addEditButnsEvents(questionnaireId) {
    var questionnaireChanges = [];//list of all changes applied
    let addQuestionBtn = document.getElementById("add_question_btn");
    let removeQuestionBtn = document.getElementById("remove_btn");
    let saveBtn = document.getElementById("save_btn");
    let cancelBtn = document.getElementById("cancel_btn");
    cancelBtn.addEventListener('click', function () {
        document.getElementById("questions_div").style.display = "none";
        document.getElementById("allquestions_div").style.display = "none";
    });
    addQuestionBtn.addEventListener('click', function () {
        addQuestionToQuestionnaire();
    });
    removeQuestionBtn.addEventListener('click', function () {
        removeQuestion();
    });
    saveBtn.addEventListener('click', function () {
        saveChanges(questionnaireId, questionnaireChanges);
    });
}

//fucntion adds selected questions to questions table and to questionnaire actions array without duplications
function addQuestionToQuestionnaire() {
    let sourceTable = document.getElementById("allquestions_table");
    let destTable = document.getElementById("questions_table");
    let isDuplicates = false;
    let checkbox = sourceTable.querySelectorAll('input[type=checkbox]');//get all checkboxes from table
    for (let i = 1; i < checkbox.length; i++) { // for each checked     
        if (checkbox[i].checked) {
            let sourceRow = sourceTable.rows[i];
            let exist = false;
            for (let j = 0; j < destTable.rows.length; j++) {//check for duplicates
                if (destTable.rows[j].cells[1].innerHTML == checkbox[i].value) {
                    exist = true;
                    isDuplicates = true;
                }
            }
            if (!exist) {
                let destRow = destTable.insertRow(-1);//add row to table
                deepCopyTableRow(destRow, sourceRow);
                //questionnaireChanges.push(action);//add action to list 
            }
        }
    }
    if (isDuplicates) {//show error message if there duplicates
        showErrMsg("חלק משאלות כבר קיימות בשאלון, רק שאלות חדשות נוספו");
    }
}


//function removes all selected questions from questionnaire and saves this action to changes list
function removeQuestion() {
    let table = document.getElementById("questions_table");
    let checkbox = table.querySelectorAll('input[type=checkbox]');//get all checkboxes from table
    for (let i = checkbox.length - 1; i >= 1; i--) { // for each checked - loop backwards     
        if (checkbox[i].checked) {
            table.deleteRow(i);
        }
    }
}

function saveChanges(id, questionnaireChanges) {
    if (validateQuestionnaire()) {
        let category;
        if (document.getElementById("radio_exist").checked)
            category = document.getElementById("category_select").value;
        else
            category = document.getElementById("new_category_input").value;
        getChanges(questionnaireChanges);
        allChanges = {
            id: id,
            name: document.getElementById("quest_name_input").value,
            category_name: category,
            changes: questionnaireChanges
        };

        xhttpSendJsonPost(allChanges, "saveQuestionnaire", function (res) {
            if (!res) {
                showErrMsg("משהו השתבש בשמירת שאלון");
            }
            else
                document.location.reload(true);
        });
    }
}

function getChanges(questionnaireChanges) {
    let table = document.getElementById("questions_table");
    for (let i = 1; i < table.rows.length; i++) {
        questionnaireChanges.push(table.rows[i].cells[1].innerHTML);
    }
}

function validateQuestionnaire() {
    if(document.getElementById("quest_name_input").reportValidity()) {
        if(document.getElementById("radio_exist").checked||
            document.getElementById("radio_new").checked&&document.getElementById("new_category_input").reportValidity()) {
                if(document.getElementById("questions_table").rows.length>1){
                    return true;
                }
                else {
                    showErrMsg("לא ניתן לשמור שאלון ריק");
                    return false;
                }
        }
    }
}

function xhttpSendJsonPost(data, action, successFunc) {
    let jsonData = JSON.stringify(data);
    var toSend = new FormData();
    toSend.append("dataToSave", jsonData);
    toSend.append("QuestionnaireAction", action);
    //AJAX request
    spinnerStart();
    var xhttp = new XMLHttpRequest();
    xhttp.onload = function () {
        var res = JSON.parse(this.response);
        successFunc(res);
        spinnerStop();
    };
    xhttp.open('POST', "../Controller/QuestionnairesHandler.php", true);
    xhttp.send(toSend);
}