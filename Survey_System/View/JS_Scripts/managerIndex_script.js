async function initializeIndexPage() {
    await tableBuilder("main_table");
    let table = document.getElementById("main_table");
    let th = document.createElement("td");
    th.appendChild(createCheckAll("main_table"));
    th.className = "main_table_th";
    table.rows[0].insertBefore(th, table.rows[0].firstChild);
    for (let i = 1; i < table.rows.length; i++) {
        if (table.rows[i].className == "statistic") {
            let td = document.createElement("td");
            td.appendChild(createCheckAll(table.rows[i].cells[0].innerHTML + TABLE_ID));
            table.rows[i].insertBefore(td, table.rows[i].firstChild);
        }
    }
}
function activateSurveyPopupOpen(containerId, openButtonId) {
    openForm(openButtonId, containerId);
    let container = document.getElementById(containerId);
    cleanElement(container);
    let table = document.createElement("table");
    table.id = containerId + TABLE_ID;
    table.className = "noChecked";
    container.append(table);
    fillTableWithAllSurveyData(table);
    container.append(getActivateSurveyButton(table, document.getElementById("table_div").getElementsByTagName("table")));
    container.append(getCloseActivateSurveyFormButton(openButtonId, containerId));
    document.getElementById(openButtonId).style.display = "none";
    container.style.display = "";

}
function fillTableWithAllSurveyData(table) {
    spinnerStart();
    let xhttp = new XMLHttpRequest();
    let sendData = new FormData();
    sendData.append("managerIndexAction", "getAllSurveys");
    xhttp.onload = function () {
        let data = JSON.parse(this.response);
        let title = document.createElement("h3");
        title.textContent = Object.keys(data)[0];
        fillTable(table, data[Object.keys(data)[0]]);
        for (let i = 1; i < table.rows.length; i++)
            table.rows[i].classList.add("clickable");
        addRowHighlight(table);
        addSearchBar(table);
        addViewMore(table, viewMoreSurvey);
        correctHeader(table);
        spinnerStop();
    };
    xhttp.open("POST", "../Controller/ManagerIndexHandler.php", true);
    xhttp.send(sendData);
}
function getActivateSurveyButton(selectedSurveyTable, classesTableCollection) {
    let button = document.createElement("button");
    button.setAttribute("type", "submit");
    button.textContent = "הפעל סקר מסומן לכיתות נבחרות";
    button.onclick = function () {
        activateSurvey(selectedSurveyTable, classesTableCollection);
    };
    return button;
}
function getCloseActivateSurveyFormButton(openButtonId, containerId) {
    let button = document.createElement("button");
    button.setAttribute("type", "button");
    button.textContent = "סגור הפעלת סקר";
    button.onclick = function () {
        closeForm(openButtonId, containerId);
    };
    return button;
}
function activateSurvey(selectedSurveyTable, classesTableCollection) {
    //getting the selected survey ID
    let selectedSurveyId = -1;
    let i, j, rowCount, tableCount, table;
    for (i = 1, rowCount = selectedSurveyTable.rows.length; i < rowCount; i++)
        if (selectedSurveyTable.rows[i].className.includes("highlight") && selectedSurveyId == -1)
            selectedSurveyId = selectedSurveyTable.rows[i].cells[0].innerHTML;

    //making an array with all selected class's names
    selectedClassesNames = [];
    for (i = 1, tableCount = classesTableCollection.length; i < tableCount; i++) {
        table = classesTableCollection[i];
        for (j = 1, rowCount = table.rows.length; j < rowCount; j++) {
            if (table.rows[j].cells[0].children[0].checked)
                selectedClassesNames.push(table.rows[j].cells[1].innerHTML);
        }
    }
    spinnerStart();
    let xhttp = new XMLHttpRequest();
    let sendData = new FormData();
    sendData.append("managerIndexAction", "activateSurvey");
    sendData.append("surveyData", JSON.stringify({ 'surveyId': selectedSurveyId, 'classesNames': selectedClassesNames }));
    xhttp.onload = function () {
        if (!JSON.parse(this.response))
            alert("משהו השתבש בהפעלת סקר...");
        spinnerStop();
        location.reload();
    };
    xhttp.open("POST", "../Controller/ManagerIndexHandler.php");
    xhttp.send(sendData);

}

