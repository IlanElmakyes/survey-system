
function spinnerStart() {
    let spinner = document.getElementsByClassName("spinner")[0];
    if (!spinner)
        return;
    spinner.style.display = "inline-block";
}

function spinnerStop() {
    let spinner = document.getElementsByClassName("spinner")[0];
    if (!spinner)
        return;
    spinner.style.display = "none";
}
