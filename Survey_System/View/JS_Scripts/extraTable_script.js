/*
    Unicode characters:
    ***to access and use unicode chracters the hex value needs to be wrapped in '\u{}'
        example: '\u{2BC5}' - is the up-triangle symble
    lower right pencil - 270E
    bold small x - 1D605
*/
const RIGHT_PENCIL = '\u{270E}';
const BOLD_X = '\u{1D605}';
const SELECT_MENU_ID = "_select";
const SEARCH_INPUT_ID = "_search";
const TABLE_ID = "_table";
const MORE = "more";
//#region Add delete and edit
/**
 * 
 * @param {a table element} table 
 * @param {delete function to tie to the delete click} deleteFunction 
 * @param {edit function to tie to the edit click} editFunction
 * adds clickable edit and delete symbols to a table
 */
function addDeleteEdit(table, deleteFunction = clickDelete, editFunction = clickEdit) {
    if (!table)
        return;
    //getting all the row tags in the tBody of the table
    //rows[0] is a header row
    let cell;
    for (i = 1; i < table.rows.length; i++) {
        cell = table.rows[i].insertCell();
        cell.appendChild(getDeleteElement());
        cell = table.rows[i].insertCell();
        cell.appendChild(getEditElement());
    }
    table.addEventListener('click', function (e) {
        switch (e.target.className.toUpperCase()) {
            case "DELETE":
                deleteFunction(getParentCell(e.target));
                break;
            case "EDIT":
                editFunction(getParentCell(e.target));
                break;
        }
    })
}

/**
 * 
 * @param {a table cell that was clicked} tableCell
 * the default delete symbol function 
 */
function clickDelete(tableCell) {
    if (tableCell === null)
        return;
    alert("clicked delete of item in row " + tableCell.parentElement.rowIndex + "\n\
    the item id is: "+ getParentTable(tableCell).rows[tableCell.parentElement.rowIndex].cells[0].innerHTML);
}
/**
 * 
 * @param {a table cell that was clicked} tableCell 
 * the default edit symbol function 
 */
function clickEdit(tableCell) {
    if (tableCell === null)
        return;
    alert("clicked edit of item in row " + tableCell.parentElement.rowIndex + "\n\
    the item id is: "+ getParentTable(tableCell).rows[tableCell.parentElement.rowIndex].cells[0].innerHTML);
}
/**
 * creates an <a> tag with an edit symbol
 */
function getEditElement() {
    let aEditTag = document.createElement("a");
    let editSpan = document.createElement("span");
    editSpan.innerHTML = RIGHT_PENCIL;
    editSpan.className += "edit";
    editSpan.title = "edit";
    aEditTag.appendChild(editSpan);
    return aEditTag;
}
/**
 * creates an <a> tag with a delete symbol
 */
function getDeleteElement() {
    let aDeleteTag = document.createElement("a");
    let deleteSpan = document.createElement("span");
    deleteSpan.innerHTML = BOLD_X;
    deleteSpan.className += "delete";
    deleteSpan.style.color = "red";
    deleteSpan.title = "delete";
    aDeleteTag.appendChild(deleteSpan);
    return aDeleteTag;
}


//#endregion

//#region ViewMore
function addViewMore(table, viewMoreFunc) {
    if (!table)
        return;
    //getting all the row tags in the tBody of the table
    //rows[0] is a header row
    let cell;
    for (i = 1; i < table.rows.length; i++) {
        cell = table.rows[i].insertCell();
        cell.appendChild(getViewMoreElement(viewMoreFunc));
    }
}

/**
 * creates an <a> tag with a more icon using google icons 
 * and event listener viewMoreFunc
 * NOTE: to use google icons add 
 * <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
 * to html <head> and set class name of span that contains icon to "material-icons"
 */
function getViewMoreElement(viewMoreFunc) {
    let aMoreTag = document.createElement("a");
    let moreSpan = document.createElement("span");
    moreSpan.innerHTML = MORE;
    moreSpan.className += "material-icons";
    moreSpan.title = "view more";
    aMoreTag.className += "more";
    aMoreTag.appendChild(moreSpan);
    var func = viewMoreFunc || function () { };//empty function will be called if viewMoreFunction wasn't passed
    aMoreTag.addEventListener('click', function () {
        func(getParentCell(this));
    });
    return aMoreTag;
}

//#endregion

//#region Utility functions
/**
 * 
 * @param {any element inside a table cell} element
 * returns the parent cell of a given element.
 * if the element is not in an HTMLTableCellElement, a null will be returned
 * if the element sent is an HTMLTbleCellElement, it will be returned
 */
function getParentCell(element) {
    while (element !== null && !(element instanceof HTMLTableCellElement))
        element = element.parentElement;
    return element;
}

/**
 * 
 * @param {a table cell element} tableCell 
 * finds the parent table of a table cell element, and returned the table element
 * if the table is not found a null will be returnred
 */
function getParentTable(tableCell) {
    //this loop is used to get to the TableElement of a table cell
    //the hierarchy of parentElements can change between tables according to it's structure
    //this loop keeps asking for a parentElemet until an HTMLTable is returned
    //**a string comparison is used because instenceof will return true for sub-classes of HTMLTableElement**
    let table = tableCell.parentElement;
    while (table !== null && !(table.toString().includes("HTMLTableElement")))
        table = table.parentElement;
    return table;
}
/**
 * 
 * @param {an element id} idString 
 * removes the added const string from an element id key
 * (added strings after the key always start with '_' )
 */
function getKeyFromId(idString) {
    if (idString.indexOf("_") > 0)
        return idString.substring(0, idString.indexOf('_'));
    else
        return idString;
}

//#endregion


//#region Search bar
/**
 * 
 * @param {the div that containes the actual table element} tableDivId
 * adds a select menu and an input field that allows to search a string in a chosen column of the table
 */
function addSearchBar(tableDivId) {
    //if there is no table, do nothing
    let table = document.getElementById(getKeyFromId(tableDivId) + TABLE_ID);
    if (!table)
        return;
    //creating the select and input elements
    let selectMenu = getSelectMenuElement(table);
    let tableDiv = document.getElementById(tableDivId);
    let searchInput = getSearchInputElement(table, "חפש לפי " + selectMenu.options[selectMenu.selectedIndex].text);
    //inserting the elements to the div without deleting the current eventHandlers of existing elements
    if (tableDiv.childNodes.length > 0) {
        tableDiv.insertBefore(selectMenu, tableDiv.childNodes[0]);
        tableDiv.insertBefore(searchInput, selectMenu.nextSibling);
    }
}
/**
 * 
 * @param {table to create select menu options from} table
 * returns a select menu with option that represents the header columns of the table
 * each option in the menu has a text that matches a header text, and it's value is the header index in the table
 */
function getSelectMenuElement(table) {
    //creating a select menu, with options that are the headers of the table
    //each option will set in what column to search for the user input
    let selectMenu = document.createElement("select");
    selectMenu.id = getKeyFromId(table.id) + SELECT_MENU_ID;
    //creating and appending the options according to the columns in the table
    for (let i = 0; i < table.rows[0].cells.length; i++) {
        let option = document.createElement("option");
        option.value = i;
        option.text = table.rows[0].cells[i].innerHTML;
        selectMenu.appendChild(option);
    }
    //sets the selected option and event handler to the select menu
    selectMenu.selectedIndex = 0;
    selectMenu.addEventListener("change", function (e) {
        setSearchInputPlaceholder(e.target);
    });
    return selectMenu;
}
/**
 * 
 * @param {table to tie the search input to} table 
 * @param {the default text to first appear in the input field} firstPlaceholderText
 * creates and returnes a text input element that, on key up, activates searchTable function 
 */
function getSearchInputElement(table, firstPlaceholderText = null) {
    let searchInput = document.createElement("input");
    searchInput.type = "text";
    searchInput.id = getKeyFromId(table.id) + SEARCH_INPUT_ID;
    searchInput.placeholder = firstPlaceholderText !== null ? firstPlaceholderText : "Search here...";
    searchInput.addEventListener("keyup", function (e) {
        searchTable(table, e.target.value, document.getElementById(getKeyFromId(table.id) + SELECT_MENU_ID).value);
    });
    return searchInput;
}
/**
 * 
 * @param {table to search} table 
 * @param {string to search in the table} searchQuery 
 * @param {column index to search the string in} columnIndex 
 * searches a string in a specific column of the table and hides 
 * all the rows that does not contain that string
 */
function searchTable(table, searchQuery, columnIndex) {
    if (table.rows.length > 0 && table.rows[0].cells.length >= columnIndex) {
        for (i = 1; i < table.rows.length; i++) {
            if (searchQuery == "")
                table.rows[i].style.display = "";
            else {
                if (table.rows[i].cells[columnIndex].innerHTML.indexOf(searchQuery) > -1)//searching for a substring in the table cell
                    table.rows[i].style.display = "";
                else table.rows[i].style.display = "none";
            }
        }
    }
}
/**
 * 
 * @param {the select menu that changed} selectMenu 
 * sets the place holder to match the select menu selected option
 */
function setSearchInputPlaceholder(selectMenu) {
    let input = document.getElementById(getKeyFromId(selectMenu.id) + SEARCH_INPUT_ID);
    input.placeholder = "חפש לפי " + selectMenu.options[selectMenu.selectedIndex].text;
}
//#endregion

//#region question edit and delete functions

function questionDelete(tableCell) {
    if (tableCell === null)
        return;
    spinnerStart();
    let xhttp = new XMLHttpRequest();
    let sendData = new FormData();
    sendData.append("action", "delete");
    sendData.append("dataType", "question");
    sendData.append("questionId", getParentTable(tableCell).rows[tableCell.parentElement.rowIndex].cells[0].innerHTML);
    xhttp.onload = function () {
        if (JSON.parse(this.response))
            alert("Question was deleted from the DB successfully!");
        else alert("There was a problem deleteing the question from the DB...");
        spinnerStop();
        document.location.reload(true);
    };
    xhttp.open("POST", "../Controller/UserIOHandler.php");
    xhttp.send(sendData);
}
//#endregion