const MAX_TXT=240;
var dataArrIndex;
var jsonData;
var answersData;
function insertDataToForm(i) {
    var category = document.getElementsByClassName('category');
    category[0].setAttribute('id', jsonData[i].id);
    category[0].innerHTML = jsonData[i].name;
    questionnaire = document.getElementById('questionnaire');

    //crete elements of questionnaire 
    for (row = 0; row < jsonData[i].questions.length; row++) {
        var questionRow = document.createElement("tr");
        var questionCell = document.createElement("td");
        //first ell in row contains question text
        questionCell.appendChild(document.createTextNode(jsonData[i].questions[row].text));
        questionRow.appendChild(questionCell);
        //create cells with radio buttons
        //name - is question id
        //value and label from 1-5
        for (cell = 1; cell <= 5; cell++) {
            var radioTd = makeTableCell(jsonData[i].questions[row].id, cell);
            questionRow.appendChild(radioTd);
        }
        if (row % 2 != 0) {
            questionRow.className = "odd_row";
        }
        questionnaire.appendChild(questionRow);
    }
}

//function show error message for 4 seconds
function showErrMsg(msg) {
    let errMsg=document.getElementById("errmsg_div");
    let errIcon = document.createElement("span");
    errIcon.innerHTML = "error_outline";
    errIcon.classList.add("material-icons","error");
    errIcon.style.fontSize="20px";
    let msg_txt=document.createTextNode(' '+msg);
    errMsg.appendChild(errIcon);
    errMsg.appendChild(msg_txt);
    errMsg.style.display="";
    window.scrollTo(0,0);
    setTimeout(function(){
        errMsg.innerHTML="";
        errMsg.style.display="none";},4000);
}

function changePageContent() {
    setBtnsVisibility();
    //delete all child elements in questionnaire 
    if (dataArrIndex < jsonData.length) {
        if (jsonData[dataArrIndex].isSkiped) {
            document.getElementById("skip_toggle").checked = true;
            document.getElementById('errmsg_div').style.display = "none";
        }
        else {

            document.getElementById("skip_toggle").checked = false;
        }
        cleanPage();
        insertDataToForm(dataArrIndex);
        retrieveAnswers();
        counter(document.getElementById('comment'));
    }
     else {
        var category=document.getElementsByClassName('category');
         category[0].style.display="none";
         document.getElementById('skip_toggle').style.display="none";    
         document.getElementById('text_input').style.display="none";
         document.getElementById('questionnaire').style.display="none";
         document.getElementById('line').style.display="none";
         document.getElementById('finish').style.display="";
     }
}

function returnToSurvey() {
    dataArrIndex--;
    changePageContent();
    var category = document.getElementsByClassName('category');
    category[0].style.display = "";
    document.getElementById('skip_toggle').style.display = "";
    document.getElementById('text_input').style.display = "";
    document.getElementById('questionnaire').style.display = "";
}

function getAnswers() {
    if (!jsonData[dataArrIndex].isSkiped) {
        for (var i = 0; i < jsonData[dataArrIndex].questions.length; i++) {
            var answers = document.getElementsByName(jsonData[dataArrIndex].questions[i].id);
            var ansValue=0;
            for (var j = 0; j < answers.length; j++) {
                if (answers[j].checked) {
                    ansValue = answers[j].value;
                }
            }
            jsonData[dataArrIndex].questions[i].numericAnswer=ansValue;
        }
        var comment = document.getElementById('comment').value;
        if(comment.replace(/\s/, '').length>0){
            jsonData[dataArrIndex].textAnswer=comment; 
        }                
    }
}

function validateAnswers() {
    if(!jsonData[dataArrIndex].isSkiped)
    {
        for(var i=0;i<jsonData[dataArrIndex].questions.length;i++) {
            var answers=document.getElementsByName(jsonData[dataArrIndex].questions[i].id);
            var ansValue=null;
            for(var j=0;j<answers.length;j++) {
                if(answers[j].checked){
                    ansValue=answers[j].value;               
                }
            }
            if(ansValue==null) {
                showErrMsg("נא לענות על כל השאלות או לדלג על השאלון ");
                return false;
            }
        }
    }
    return true;
}

function retrieveAnswers() {
    if (!jsonData[dataArrIndex].isSkiped) {
        document.getElementById('comment').value = jsonData[dataArrIndex].textAnswer;
        for (var i = 0; i < jsonData[dataArrIndex].questions.length; i++) {
            var answers = document.getElementsByName(jsonData[dataArrIndex].questions[i].id);
            var checked = jsonData[dataArrIndex].questions[i].numericAnswer;
            if (checked != 0)
                answers[Number(checked - 1)].checked = true;
        }
    }
}

function cleanPage() {
    var questionsTable = document.getElementById('questionnaire');
    for (let i = questionsTable.rows.length - 1; i > 0; i--) {
        questionsTable.deleteRow(i);
    }
    document.getElementById('comment').value = "";
}

//create table cell with radio button and label
function makeTableCell(name, value) {
    var td = document.createElement("td");

    var radio = document.createElement("input");
    radio.type = "radio";
    radio.name = name;
    radio.value = value;

    td.appendChild(radio);

    return td;
}

function loadData() {
    spinnerStart();
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            jsonData = JSON.parse(this.responseText);
            insertDataToForm(dataArrIndex);
            prepareDataArr();
            setBtnsVisibility();
            spinnerStop();
        }
    };
    xhttp.open("GET", "../Controller/survey_get_data.php", true);
    xhttp.send();
}

function setCookie(cName, cVal, cExp) {
    var expDate=new Date(cExp).toUTCString();
    document.cookie=cName+"="+cVal+";"+"expires="+expDate;
}

function sendResults() {
    var results = JSON.stringify(jsonData);
    var data = new FormData();
    data.append('results', results);

    //AJAX request
    spinnerStart();
    var xhttp = new XMLHttpRequest();
    xhttp.onload = function () {
        var res = JSON.parse(this.response);
        spinnerStop();
        if (res.redirect != ""){
            setCookie(res.code,true,res.expire);
            window.location.href = res.redirect;
        }
            
    };
    xhttp.open('POST', '../Controller/survey_save_data.php', true);
    xhttp.send(data);
}

//set onclick functions and its parametrs for buttons
function setBtnsEvents() {
    document.getElementById("btn_send").addEventListener('click', sendResults);
    document.getElementById("btn_return").addEventListener('click', returnToSurvey);

    document.getElementById("btn_next").addEventListener('click', function () {
        if(validateAnswers()) {
            getAnswers();
            dataArrIndex++;
            changePageContent();
        }
    });

    document.getElementById("btn_prev").addEventListener('click', function () {
            getAnswers();
            dataArrIndex--;
            changePageContent();
    });

    document.getElementById('skip_toggle').addEventListener('change', function () {
        if (this.checked) {
            jsonData[dataArrIndex].isSkiped = true;
            changePageContent();
        }
        else {
            jsonData[dataArrIndex].isSkiped = false;
            changePageContent();
        }
    });
}


//set buttons visibility
function setBtnsVisibility() {
    document.getElementById("btn_prev").style.display = (dataArrIndex > 0 && dataArrIndex < jsonData.length) ? "" : "none";
    document.getElementById("btn_next").style.display = (dataArrIndex < jsonData.length) ? "" : "none";
    document.getElementById("finish").style.display = (dataArrIndex < jsonData.length) ? "none" : "";
    document.getElementById("toggle_div").style.display = (dataArrIndex < jsonData.length) ? "" : "none";
    document.getElementById('line').style.display = (dataArrIndex < jsonData.length) ? "" : "none";
}

//add keys with starting values to data arr
function prepareDataArr() {
    for (i = 0; i < jsonData.length; i++) {
        jsonData[i]['isSkiped'] = false;
        jsonData[i]['textAnswer'] = '';

        for (j = 0; j < jsonData[i].questions.length; j++)
            jsonData[i].questions[j]['numericAnswer'] = 0;
    }

}

function counter(comment){
    document.getElementById('char_count').innerHTML = comment.value.length+'/'+MAX_TXT;
}

window.onload = function () {
    dataArrIndex = 0;
    loadData();
    setBtnsEvents();
}

