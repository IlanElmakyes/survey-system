/*
    Unicode characters:
    ***to access and use unicode chracters the hex value needs to be wrapped in '\u{}'
        example: '\u{2BC5}' - is the up-triangle symble
    up-triangle - 2BC5
    down-triangle - 2BC6
    lower right pencil - 270E
    bold small x - 1D605
    check mark - 2713
*/
const TRIANGLE_UP = '\u{2BC5}';
const TRIANGLE_DWON = '\u{2BC6}';
const TABLE_ID = "_table";
const HIDE_ID = "_hide";
const RIGHT_PENCIL = '\u{270E}';
const BOLD_X = '\u{1D605}';
const SELECT_MENU_ID = "_select";
const SEARCH_INPUT_ID = "_search";
const MORE = "more";
const CHECKMARK = '\u{2713}';
const MAX_QUEST_LEN = 120;
/*
    ********************NOTE****************************
    when adding HTML to an element using .innerHTML, all the child elements in that element are *replaced*
    (think of it like "adding" to a string using += , you can't add to a string, you create a whole new string with the added value)
    that means that any eventHandlers that were tied to those element are GONE.
    in order to avoid that, use DOM methods like .insertbefore() and the like, to append child elements without changing the exesting ones
    ****************************************************
*/
//#region Table building
/**
 * 
 * @param {string} divId - the DIV id that the table will be injected to 
 * @param {boolean} addExtra - flg that determine if the table will have delete&edit buttons 
 * @param {function} deleteFunction - custom delete function
 * @param {function} editFunction - custom edit function
 * Builds an empty table tag with a tHead in a given div (by id)
 */
function tableBuilder(divId, addExtra = false, deleteFunction = null, editFunction = null) {
    return new Promise(function (reslove) {
        spinnerStart();
        let xhttp = new XMLHttpRequest();
        let sendData = new FormData();
        sendData.append("title", document.getElementsByTagName("title")[0].text);
        xhttp.onload = function () {
            let data = JSON.parse(this.response);
            for (var key in data) {
                document.getElementById(divId).appendChild(createStatRow(data, key));
                let td = document.createElement("td");
                td.className = "container_cell";
                td.colSpan = 6;
                td.appendChild(createKeyDiv(key));
                document.getElementById(divId).appendChild(td);
                let table = document.getElementById(key + TABLE_ID);
                fillTable(table, data[key][1]);
                if (addExtra) {
                    if (deleteFunction !== null && editFunction !== null)
                        addDeleteEdit(table, deleteFunction, editFunction);
                    else if (deleteFunction !== null)
                        addDeleteEdit(table, deleteFunction);
                    else addDeleteEdit(table);
                }
                addCheckBox(table);
                addSearchBar(table);
                correctHeader(table);
            }
            let tables = document.getElementsByTagName("table");
            for (let i = 0; i < tables.length; i++) {
                //setting click eventHandler on th tags
                tables[i].addEventListener('click', function (e) {
                    if (e.target.nodeName.toUpperCase() !== "TH") return;
                    header_click(e.target);
                });
            }
            spinnerStop();
            reslove();
        };
        xhttp.open("POST", "../Controller/FillManagerTableData.php", true);
        xhttp.send(sendData);
    });
}

//function creates row with basic info for faculty 
function createStatRow(data, key) {
    let row = document.createElement("tr");
    row.className = "statistic";
    for (let i = 0; i < data[key][0].length; i++) {
        let c = document.createElement("td");
        c.innerHTML = data[key][0][i];
        c.addEventListener('click', function () {
            spoilerAction(key + HIDE_ID);
        });
        c.className = "clickable";
        row.appendChild(c);
    }
    return row;
}
/**
 * 
 * @param {string} key - the div key string to be used in the id of the various elements 
 */
function createKeyDiv(key) {
    let keyDiv = document.createElement("div");
    keyDiv.id = key;
    keyDiv.innerHTML = "<div id='" + key + HIDE_ID + "' style='display: none;'><table id='" + key + TABLE_ID + "'></table></div>";
    // keyDiv.innerHTML += "<h3>"+key+"</h3><button title='Click to show facultiy information' \
    // type='button' onclick='spoilerAction(\""+key+HIDE_ID+"\")'>Show/Hide</button> \
    // <div id='"+key+HIDE_ID+"' style='display: none;'><table border='1' id='"+key+TABLE_ID+"'></table></div>";
    return keyDiv;
}
/**
 * 
 * @param {string} continerId - the id of the continer element
 * hides or shows a container element
 */
function spoilerAction(continerId) {
    let container = document.getElementById(continerId);
    if (container.style.display == 'none')
        container.style.display = ''
    else
        container.style.display = 'none';
}
/**
 * 
 * @param {HTMLTableElement} table 
 * @param {Array} tableDataArr - an array with data to fill the table with
 * uses a controller file to get data and populate the table according to that data
 *  *note: table size will be relative to the data.
 *      e.g:
 *      if there are 4 catagories there will be 4 columns to the table and each column will
 *      be filled with the catagory relevent data
 */
function fillTable(table, tableDataArr) {

    if (Object.keys(tableDataArr).length > 0) {
        let i, j;
        let MAX_ROWS = Object.keys(tableDataArr).length;
        let MAX_COLUMNS = Object.keys(tableDataArr[0]).length;
        let row;
        //populating the tBody tag
        for (i = 1; i < MAX_ROWS; i++) {
            row = table.insertRow();
            for (j = 0; j < MAX_COLUMNS; j++) {
                let cell = row.insertCell();
                cell.appendChild(document.createTextNode(tableDataArr[i][j]));
            }
        }
        //populating the tHead tag
        let tHead = table.createTHead();
        row = tHead.insertRow();
        for (i = 0; i < MAX_COLUMNS; i++) {
            let th = document.createElement("th");
            th.appendChild(document.createTextNode(tableDataArr[0][i]));
            row.appendChild(th);
        }
    }
}
//similar to fillTbable function
//Takes fisrt element in data array as ID and sets it as row value 
//So ID is not shown and stored for each row
function fillTableWithId(table, tableDataArr) {
    if (Object.keys(tableDataArr).length > 0) {
        let i, j;
        let MAX_ROWS = Object.keys(tableDataArr).length;
        let MAX_COLUMNS = Object.keys(tableDataArr[0]).length;
        let row;
        //populating the tBody tag
        for (i = 1; i < MAX_ROWS; i++) {
            row = table.insertRow();
            //row.setAttribute("value",tableDataArr[i][0]);
            row.value = tableDataArr[i][0];
            for (j = 1; j < MAX_COLUMNS; j++) {
                let cell = row.insertCell();
                cell.appendChild(document.createTextNode(tableDataArr[i][j]));
            }
        }
        //populating the tHead tag
        let tHead = table.createTHead();
        row = tHead.insertRow();
        for (i = 1; i < MAX_COLUMNS; i++) {
            let th = document.createElement("th");
            th.appendChild(document.createTextNode(tableDataArr[0][i]));
            row.appendChild(th);
        }
    }
}

function getRowByValue(table, value) {
    if (table !== null) {
        for (let i = 0; i < table.rows.length; i++) {
            if (table.rows[i].value == value)
                return (table.rows[i]);
        }
    }
    return null;
}
/**
 * 
 * @param {HTMLTableCellElement} tableCell
 * Tied onclick function to table header cells
 * this function is used to sort the table by column values 
 */
function header_click(tableCell) {
    let table = getParentTable(tableCell);
    if (table !== null)
        sortTable(table, tableCell);
}
//#endregion

//#region Table sorting
/**
 * 
 * @param {HTMLTableElement} table - the table to be sorted
 * @param {HTMLTableCellElement} tableCell - the table header cell of the column that is to be sorted
 * the function sorts an HTMLTable by column and adds unicode symboles to the sorted header cells innerHTML 
 */
function sortTable(table, tableCell) {
    let rowIndex = tableCell.parentElement.rowIndex;
    let columns = table.rows[rowIndex].cells.length;
    let columnIndex = tableCell.cellIndex;
    if (!tableCell.innerHTML.includes(TRIANGLE_UP) && !tableCell.innerHTML.includes(TRIANGLE_DWON)) {
        //passing any cells -in the column to be sorted- that holds value, to check it's data type
        quickSortDownTableByColumn(table, columnIndex);
    }
    else
        tableFlip(table);
    addSortUnicodeSymboles(table, tableCell);

}

/**
 * 
 * @param {HTMLTableElement} table 
 * flips the values of a table.
 * table must have the same column number on each row.
 */
function tableFlip(table) {
    let rows = table.rows.length;
    let i, j;
    for (i = 1, j = rows - 1; i < j; i++ , j--)
        switchRow(table, i, j);
}
/**
 * 
 * @param {HTMLTableElement} table - the table to be sorted
 * @param {HTMLTableCellElement} tableCell - the column header cell to be sorted
 *     the function will add,change and remove unicode sort-symbles to indicate the state of the column
    e.g:
    - if the column was unsorted (and thous, no unicode symble attached) it will attach the down-triangle, as this is the default
    - if a column has the down or up symbles it will switch them, to indicate in what order they need to be organized
    all sorting symbles are removed from all the other cells in the row(because the new order will be by the selected cell's row)
 */
function addSortUnicodeSymboles(table, tableCell) {
    //switching or adding the sort symble
    if (tableCell.innerHTML.includes(TRIANGLE_UP))
        tableCell.innerHTML = tableCell.innerHTML.replace(TRIANGLE_UP, TRIANGLE_DWON);
    else {
        if (tableCell.innerHTML.includes(TRIANGLE_DWON))
            tableCell.innerHTML = tableCell.innerHTML.replace(TRIANGLE_DWON, TRIANGLE_UP);
        else tableCell.innerHTML += TRIANGLE_DWON;
    }
    //removing any sort symboles from the other columns
    let rowNumber = tableCell.parentElement.rowIndex;
    let columns = table.rows[rowNumber].cells.length;
    for (i = 0; i < columns; i++) {
        if (table.rows[rowNumber].cells[i] != tableCell) {
            table.rows[rowNumber].cells[i].innerHTML = table.rows[rowNumber].cells[i].innerHTML.replace(TRIANGLE_DWON, "");
            table.rows[rowNumber].cells[i].innerHTML = table.rows[rowNumber].cells[i].innerHTML.replace(TRIANGLE_UP, "");
        }
    }
}
/**
 * 
 * @param {any} val1 - value to be compared
 * @param {any} val2 - value to be compared
 * makes sure that the compared values are either both numbers or both strings
 * returns -1 if val1<val2
 * returns 0 if val1==val2
 * returns 1 if val1>val2
 */
function compareData(val1, val2) {
    if (!isNaN(val1) && !isNaN(val2)) {
        val1 = parseFloat(val1);
        val2 = parseFloat(val2);
    }
    else {
        val1 = val1.toString();
        val2 = val2.toString();
    }
    if (val1 < val2)
        return -1;
    else if (val1 == val2)
        return 0;
    else return 1;
}
/**
 * 
 * @param {HTMLTableElement} table 
 * @param {number} columnIndex - column index to sort by
 * @param {number} left - left side index of the table to check (top side)
 * @param {number} right - the right side index of the table to check (bottom side) 
 * sorts the table elements in ascending order from the top of the table to the bottom
 */
function quickSortDownTableByColumn(table, columnIndex, left = 1, right = 0, ) {
    if (right === 0)
        right = table.rows.length - 1;
    if (right - left < 1)
        return;
    //pivot is chosen at the end of the array
    let pivotCellValue = table.rows[right].cells[columnIndex].innerHTML;
    //the loop does not cound the pivot in its iteration 
    let rightIndex = right - 1;
    let leftIndex = left;
    //while left(smaller numbers) and our right (bigger numbers) don't meet, the loop keeps going
    //when they meet, that's where the pivot needs to be
    while (leftIndex < rightIndex) {
        while (leftIndex < rightIndex && compareData(table.rows[leftIndex].cells[columnIndex].innerHTML, pivotCellValue) <= 0)
            leftIndex++;
        while (rightIndex > leftIndex && compareData(table.rows[rightIndex].cells[columnIndex].innerHTML, pivotCellValue) >= 0)
            rightIndex--;
        if (leftIndex < rightIndex &&
            compareData(table.rows[rightIndex].cells[columnIndex].innerHTML, table.rows[leftIndex].cells[columnIndex].innerHTML) == -1)
            switchRow(table, leftIndex, rightIndex);
    }
    //switching the pivot point with the last place that's bigger
    if (compareData(table.rows[leftIndex].cells[columnIndex].innerHTML, pivotCellValue) == 1)
        switchRow(table, leftIndex, right);
    quickSortDownTableByColumn(table, columnIndex, left, rightIndex);
    quickSortDownTableByColumn(table, columnIndex, leftIndex + 1, right);
}
/**
 * 
 * @param {HTMLTableElement} table 
 * @param {number} rowIndex1 - first row index to switch
 * @param {number} rowIndex2 - second row index to switch
 * swap two rows as objects not as text, without loosing event handlers 
 */
function switchRow(table, rowIndex1, rowIndex2) {
    //rowIndex1 need to be smaller than rowIndex2 for swap to work correctly
    if (rowIndex1 > rowIndex2) {
        let tmpI = rowIndex1;
        rowIndex1 = rowIndex2;
        rowIndex2 = tmpI;
    }

    let tmp;
    tmp = table.rows[rowIndex2];
    tmp.parentNode.insertBefore(table.rows[rowIndex1], tmp);
    tmp = table.rows[rowIndex1];
    tmp.parentNode.insertBefore(table.rows[rowIndex2], tmp);
    //let MAX_COLUMN = table.rows[rowIndex1].cells.length;
    // for(i=0;i<MAX_COLUMN;i++)
    // {        
    //     tmp = table.rows[rowIndex1].cells[i].innerHTML;
    //     table.rows[rowIndex1].cells[i].innerHTML = table.rows[rowIndex2].cells[i].innerHTML;
    //     table.rows[rowIndex2].cells[i].innerHTML = tmp;
    // }
}
//#endregion

//#region Add delete and edit
/**
 * 
 * @param {HTMLTableElement} table 
 * @param {function} deleteFunction - delete function to tie to the delete click
 * @param {function} editFunction - edit function to tie to the edit click
 * adds clickable edit and delete symbols to a table
 */
function addDeleteEdit(table, deleteFunction = clickDelete, editFunction = clickEdit) {
    if (!table)
        return;
    //getting all the row tags in the tBody of the table
    //rows[0] is a header row
    let cell;
    for (i = 1; i < table.rows.length; i++) {
        cell = table.rows[i].insertCell();
        cell.appendChild(getDeleteElement());
        cell = table.rows[i].insertCell();
        cell.appendChild(getEditElement());
    }
    table.addEventListener('click', function (e) {
        switch (e.target.title.toUpperCase()) {
            case "DELETE":
                deleteFunction(getParentCell(e.target));
                break;
            case "EDIT":
                editFunction(getParentCell(e.target));
                break;
        }
    })
}

/**
 * 
 * @param {HTMLTableCellElement} tableCell - a table cell that was clicked
 * the default delete symbol function 
 */
function clickDelete(tableCell) {
    if (tableCell === null)
        return;
    alert("clicked delete of item in row " + tableCell.parentElement.rowIndex + "\n\
    the item id is: "+ getParentTable(tableCell).rows[tableCell.parentElement.rowIndex].cells[0].innerHTML);
}
/**
 * 
 * @param {HTMLTableCellElement} tableCell - a table cell that was clicked 
 * the default edit symbol function 
 */
function clickEdit(tableCell) {
    if (tableCell === null)
        return;
    alert("clicked edit of item in row " + tableCell.parentElement.rowIndex + "\n\
    the item id is: "+ getParentTable(tableCell).rows[tableCell.parentElement.rowIndex].cells[0].innerHTML);
}
/**
 * creates an <a> tag with an edit symbol
 */
function getEditElement() {
    let aEditTag = document.createElement("a");
    let editSpan = document.createElement("span");
    editSpan.innerHTML = "edit";
    editSpan.classList.add("material-icons", "edit");
    editSpan.title = "edit";
    editSpan.style.fontSize = "16px";
    aEditTag.appendChild(editSpan);
    return aEditTag;
}
/**
 * creates an <a> tag with a delete symbol
 */
function getDeleteElement() {
    let aDeleteTag = document.createElement("a");
    let deleteSpan = document.createElement("span");
    deleteSpan.innerHTML = "delete_forever";
    deleteSpan.classList.add("material-icons", "delete");
    deleteSpan.style.fontSize = "16px";
    deleteSpan.title = "delete";
    aDeleteTag.appendChild(deleteSpan);
    return aDeleteTag;
}


//#endregion

//#region ViewMore

function addViewMore(table, viewMoreFunc) {
    if (!table)
        return;
    //getting all the row tags in the tBody of the table
    //rows[0] is a header row
    let cell;
    for (i = 1; i < table.rows.length; i++) {
        cell = table.rows[i].insertCell();
        cell.appendChild(getViewMoreElement());
    }
    table.addEventListener('click', function (e) {
        if (e.target.parentElement.className.toUpperCase() == "MORE") {
            viewMoreFunc(getParentCell(e.target));
        }
    });
}

/**
* creates an <a> tag with a more icon using google icons 
* and event listener viewMoreFunc
* NOTE: to use google icons add 
* <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
* to html <head> and set class name of span that contains icon to "material-icons"
*/
function getViewMoreElement() {
    let aMoreTag = document.createElement("a");
    let moreSpan = document.createElement("span");
    moreSpan.innerHTML = MORE;
    moreSpan.className += "material-icons";
    moreSpan.title = "view more";
    moreSpan.style.fontSize = "16px";
    aMoreTag.className += "more";
    aMoreTag.appendChild(moreSpan);
    return aMoreTag;
}

//#endregion

//#region Utility functions
/**
 * 
 * @param {HTMLTableElement} table
 * @param {boolean} multiRow - a flag to determine if the added functionality should support multi row selection
 * adds highlight class to a table row
 * if multiRow is true, the user can select multiple row pressing the 'shift','ctrl' or 'meta' key (meta on mac)
 * if one of those keys are not pressed, or multiRow is false, when a row is pressed, all the other marked row will
 * be de-marked, and the clicked row highlight will be toggled (will switch it's current state)
 */
function addRowHighlight(table, multiRow = false) {
    if (multiRow) {
        table.addEventListener("click", function (e) {
            if (e.ctrlKey || e.shiftKey || e.metaKey)
                getParentCell(e.target).parentElement.classList.toggle("highlight");
            else {
                for (let i = 0; i < table.rows.length; i++) {
                    if (getParentCell(e.target).parentElement.rowIndex == table.rows[i].rowIndex)
                        table.rows[i].classList.toggle("highlight");
                    else
                        table.rows[i].classList.remove("highlight");
                }
            }
        });
    }
    else {
        table.addEventListener("click", function (e) {
            for (let i = 0; i < table.rows.length; i++) {
                if (getParentCell(e.target).parentElement.rowIndex == table.rows[i].rowIndex)
                    table.rows[i].classList.toggle("highlight");
                else
                    table.rows[i].classList.remove("highlight");
            }
        });
    }
}
/**
 * 
 * @param {HTMLElement} element - any element inside a table cell
 * returns the parent cell of a given element.
 * if the element is not in an HTMLTableCellElement, a null will be returned
 * if the element sent is an HTMLTbleCellElement, it will be returned
 */
function getParentCell(element) {
    while (element !== null && !(element instanceof HTMLTableCellElement))
        element = element.parentElement;
    return element;
}

/**
 * 
 * @param {HTMLTableCellElement} tableCell 
 * finds the parent table of a table cell element, and returned the table element
 * if the table is not found a null will be returnred
 */
function getParentTable(tableCell) {
    //this loop is used to get to the TableElement of a table cell
    //the hierarchy of parentElements can change between tables according to it's structure
    //this loop keeps asking for a parentElemet until an HTMLTable is returned
    //**a string comparison is used because instenceof will return true for sub-classes of HTMLTableElement**
    let table = tableCell.parentElement;
    while (table !== null && !(table.toString().includes("HTMLTableElement")))
        table = table.parentElement;
    return table;
}
/**
 * 
 * @param {HTMLElement} element
 * returns the element first parent DIV element
 * if the element does not have a parent div, a null is returned 
 */
function getParentDiv(element) {
    if (element !== null)
        element = element.parentElement;
    while (element !== null && !(element.toString().includes("HTMLDivElement")))
        element = element.parentElement;
    return element;
}
function getParentContainer(element) {
    if (element !== null)
        element = element.parentElement;
    while (element !== null && !(element.toString().includes("HTMLDivElement") || element.toString().includes("HTMLFormElement")))
        element = element.parentElement;
    return element;
}
/**
 * 
 * @param {string} idString - an element id
 * removes the added const string from an element id key
 * (added strings after the key always start with '_' )
 */
function getKeyFromId(idString) {
    if (idString.indexOf("_") > 0)
        return idString.substring(0, idString.indexOf('_'));
    else
        return idString;
}

//#endregion

//#region Search bar
/**
 * 
 * @param {string} tableDivId - the div that containes the actual table element
 * adds a select menu and an input field that allows to search a string in a chosen column of the table
 * THIS SHOULD BE THE LAST THING TO ADD TO A TABLE!
 */
function addSearchBar(table) {
    //if there is no table, do nothing
    if (!table)
        return;
    //creating the select and input elements
    let selectMenu = getSelectMenuElement(table);
    let tableContainer = getParentContainer(table);
    let searchInput = getSearchInputElement(table, "חפש לפי " + selectMenu.options[selectMenu.selectedIndex].text);
    //inserting the elements to the div without deleting the current eventHandlers of existing elements
    if (tableContainer.childNodes.length > 0) {
        tableContainer.insertBefore(selectMenu, table);
        tableContainer.insertBefore(searchInput, selectMenu.nextSibling);
    }
}
/**
 * 
 * @param {HTMLTableElement} table - table to create select menu options from
 * returns a select menu with option that represents the header columns of the table
 * each option in the menu has a text that matches a header text, and it's value is the header index in the table
 */
function getSelectMenuElement(table) {
    //creating a select menu, with options that are the headers of the table
    //each option will set in what column to search for the user input
    let selectMenu = document.createElement("select");
    selectMenu.id = getKeyFromId(table.id) + SELECT_MENU_ID;
    //creating and appending the options according to the columns in the table
    for (let i = 0; i < table.rows[0].cells.length; i++) {
        //this if is to avoid the checkbox column
        if (table.rows[0].cells[i].innerHTML !== "" && !table.rows[0].cells[i].innerHTML.includes("<input")) {
            let option = document.createElement("option");
            option.value = i;
            option.text = table.rows[0].cells[i].innerHTML;
            selectMenu.appendChild(option);
        }
    }
    //sets the selected option and event handler to the select menu
    selectMenu.selectedIndex = 0;
    selectMenu.addEventListener("change", function (e) {
        setSearchInputPlaceholder(e.target);
    });
    return selectMenu;
}
/**
 * 
 * @param {HTMLTableElement} table - table to tie the search input to
 * @param {string} firstPlaceholderText - the default text to first appear in the input field
 * creates and returnes a text input element that, on key up, activates searchTable function 
 */
function getSearchInputElement(table, firstPlaceholderText = null) {
    let searchInput = document.createElement("input");
    searchInput.type = "text";
    searchInput.id = getKeyFromId(table.id) + SEARCH_INPUT_ID;
    searchInput.placeholder = firstPlaceholderText !== null ? firstPlaceholderText : "Search here...";
    searchInput.addEventListener("keyup", function (e) {
        searchTable(table, e.target.value, document.getElementById(getKeyFromId(table.id) + SELECT_MENU_ID).value);
    });
    return searchInput;
}
/**
 * 
 * @param {HTMLTableElement} table - table to search
 * @param {string} searchQuery - string to search in the table
 * @param {number} columnIndex - column index to search the string in
 * searches a string in a specific column of the table and hides 
 * all the rows that does not contain that string
 */
function searchTable(table, searchQuery, columnIndex) {
    if (table.rows.length > 0 && table.rows[0].cells.length >= columnIndex) {
        for (i = 1; i < table.rows.length; i++) {
            if (searchQuery == "")
                table.rows[i].style.display = "";
            else {
                if (table.rows[i].cells[columnIndex].innerHTML.indexOf(searchQuery) > -1)//searching for a substring in the table cell
                    table.rows[i].style.display = "";
                else table.rows[i].style.display = "none";
            }
        }
    }
}
/**
 * 
 * @param {HTMLSelectElement} selectMenu - the select menu that changed
 * sets the place holder to match the select menu selected option
 */
function setSearchInputPlaceholder(selectMenu) {
    let input = document.getElementById(getKeyFromId(selectMenu.id) + SEARCH_INPUT_ID);
    input.placeholder = "חפש לפי " + selectMenu.options[selectMenu.selectedIndex].text;
}

function correctHeader(table) {
    if (table === null || table.rows.length <= 1)
        return;
    let diff = table.rows[1].cells.length - table.rows[0].cells.length;
    if (diff > 0) {
        let cell = table.rows[0].cells[table.rows[0].cells.length - 1];
        let cellIndex = table.rows[0].cells.length;
        cell.colSpan = diff + 1;
        for (let i = 1; i < table.rows.length; i++) {
            for (let j = 0; j < diff; j++)
                table.rows[i].cells[cellIndex + j].style.borderStyle = "none";
        }
    }
}
//#endregion

//#region question edit and delete functions


//#region Questions table
function buildQuestionsTable(divId, addExtra = false, deleteFunction = null, editFunction = null) {
    spinnerStart();
    let xhttp = new XMLHttpRequest();
    let sendData = new FormData();
    sendData.append("title", document.getElementsByTagName("title")[0].text);
    xhttp.onload = function () {
        let data = JSON.parse(this.response);
        for (var key in data) {
            let div = document.getElementById(divId);
            let table = document.createElement("table");
            table.id = "questions_table";
            div.appendChild(table);
            fillTable(table, data[key]);
            if (addExtra) {
                if (deleteFunction !== null && editFunction !== null)
                    addDeleteEdit(table, deleteFunction, editFunction);
                else if (deleteFunction !== null)
                    addDeleteEdit(table, deleteFunction);
                else addDeleteEdit(table);
            }
            addSearchBar(table);
            correctHeader(table);
        }
        let tables = document.getElementsByTagName("table");
        for (let i = 0; i < tables.length; i++) {
            //setting click eventHandler on th tags
            tables[i].addEventListener('click', function (e) {
                if (e.target.nodeName.toUpperCase() !== "TH") return;
                header_click(e.target);
            });
        }
        spinnerStop();
    };
    xhttp.open("POST", "../Controller/FillManagerTableData.php");
    xhttp.send(sendData);
}
//#endregion

//#region Delete
/**
 * 
 * @param {HTMLTableCellElement} tableCell - the click delete table cell
 * asks the user for confirmation and then deletes the selected question from the DB
 */
function questionDelete(tableCell) {
    if (tableCell === null)
        return;
    if(!confirm("למחוק את השאלה?"))
        return;
    spinnerStart();
    let xhttp = new XMLHttpRequest();
    let sendData = new FormData();
    sendData.append("action", "delete");
    sendData.append("dataType", "question");
    sendData.append("questionId", getParentTable(tableCell).rows[tableCell.parentElement.rowIndex].cells[0].innerHTML);
    xhttp.onload = function () {
        if (!JSON.parse(this.response))
            alert("משהו השתבש במחיקת שאלה...");
        spinnerStop();
        document.location.reload(true);
    };
    xhttp.open("POST", "../Controller/UserIOHandler.php");
    xhttp.send(sendData);
}
//#endregion

//#region Edit
/**
 * 
 * @param {HTMLTableCellElement} tableCell - the clicked edit table cell
 * preforms the edit click action - 
 * changes the row to accept input from the user and update the question information in the DB
 */
function questionEdit(tableCell) {
    if (tableCell === null)
        return;
    let table = getParentTable(tableCell);
    let originalRow = table.rows[tableCell.parentElement.rowIndex];
    let rowIndex = tableCell.parentElement.rowIndex;
    table.deleteRow(tableCell.parentElement.rowIndex);
    let editRow = table.insertRow(rowIndex);
    convertToQuestionEditRow(editRow, originalRow);
    table.rows[rowIndex].addEventListener("click", function (e) {
        switch (e.target.title.toUpperCase()) {
            case "CANCEL":
                deepCopyTableRow(editRow, originalRow);
                break;
            case "CONFIRM":
                if(editRow.cells[1].firstChild.reportValidity()){
                    if(confirm("לשמור שינויים?")) {
                        updateQuestion(editRow.cells[0].innerHTML,editRow.cells[1].firstChild.value);
                        convertToQuestionRow(editRow);
                    }                       
                    else
                        location.reload();
                }
                break;
        }
    });
}
/**
 * 
 * @param {string} questionId - the question id to be updated
 * @param {string} newQuestionText - the updated question text
 * updates question text in the db.
 * checks that the new question text is not empty string or a string of whitespaces beforehand
 */
function updateQuestion(questionId,newQuestionText)
{
    spinnerStart();
    let xhttp = new XMLHttpRequest();
    let sendData = new FormData();
    sendData.append("action", "edit");
    sendData.append("dataType", "question");
    sendData.append("questionData[]", questionId);
    sendData.append("questionData[]", newQuestionText);
    xhttp.onload = function () {
        if (!JSON.parse(this.response))
            alert("משהו השתבש בעדכון שאלה...");
        spinnerStop();
    }
    xhttp.open("POST", "../Controller/UserIOHandler.php");
    xhttp.send(sendData);
}
/**
 * 
 * @param {HTMLTableRowElement} rowToConvert - row to be converted to an edit row
 * @param {HTMLTableRowElement} rowWithOriginalData - row with the original question data to copy to the edit row
 * makes rowToConvert into an edit row with the data from rowWithOriginalData
 */
function convertToQuestionEditRow(rowToConvert, rowWithOriginalData) {
    if (!(rowToConvert instanceof HTMLTableRowElement) || !(rowWithOriginalData instanceof HTMLTableRowElement))
        return;
    //empty the row to convert from all child nodes
    while (rowToConvert.firstChild)
        rowToConvert.removeChild(rowToConvert.firstChild);
    let idCell = rowToConvert.insertCell();
    let textCell = rowToConvert.insertCell();
    let cancelCell = rowToConvert.insertCell();
    let confirmCell = rowToConvert.insertCell();

    idCell.innerHTML = rowWithOriginalData.cells[0].innerHTML;

    let input = document.createElement("input");
    input.setAttribute("type", "text");
    input.required=true;
    input.setAttribute("maxLength",MAX_QUEST_LEN);
    input.pattern='^.*\\S+.*$';
    input.addEventListener('invalid',textValidation);
    input.defaultValue = rowWithOriginalData.cells[1].innerHTML.toString();
    input.size=rowWithOriginalData.cells[1].innerHTML.toString().length;
    textCell.appendChild(input);

    cancelCell.innerHTML = "<a><span class='material-icons cancel' title='cancel' style='color: red;'>close</span></a>";

    confirmCell.innerHTML = "<a><span class='material-icons confirm' title='confirm' style='color: green;'>check</span></a>";
}
/**
 * 
 * @param {HTMLTableRowElement} editRowToConvert - the row that was turned into an edit row
 * converts an edit row into a regular row
 */
function convertToQuestionRow(editRowToConvert) {
    let questionId = editRowToConvert.cells[0].innerHTML;
    let questionText = editRowToConvert.cells[1].firstChild.value;

    while (editRowToConvert.firstChild)
        editRowToConvert.removeChild(editRowToConvert.firstChild);

    let cell = editRowToConvert.insertCell();
    cell.innerHTML = questionId;

    cell = editRowToConvert.insertCell();
    cell.innerHTML = questionText;

    cell = editRowToConvert.insertCell();
    cell.appendChild(getDeleteElement());

    cell = editRowToConvert.insertCell();
    cell.appendChild(getEditElement());
}
/**
 * 
 * @param {HTMLTableRowElement} destRow - destination Table Row - to be copied to
 * @param {HTMLTableRowElement} sourceRow - source Table Row - to be copied from
 * deep copies the cells and thier content from source row to dest row
 */
function deepCopyTableRow(destRow, sourceRow) {
    if (!(destRow instanceof HTMLTableRowElement) || !(sourceRow instanceof HTMLTableRowElement))
        return;
    //this loop is used to remove any child element that were in the destRow before the copy
    while (destRow.firstChild)
        destRow.removeChild(destRow.firstChild);
    for (i = 0; i < sourceRow.cells.length; i++) {
        let cell = destRow.insertCell();
        cell.innerHTML = sourceRow.cells[i].innerHTML;
    }
    destRow.value = sourceRow.value;
}
//#endregion

//#region checkboxes for table
/**
 * function adds checkboxes column to each row in table except for header row
 * each checkbox value is ID (row.cells[0])
 * 
 * @param {object} table 
 */
function addCheckBox(table) {
    for (let i = 0; i < table.rows.length; i++) {
        let check = document.createElement("input");
        check.type = "checkbox";
        check.value = table.rows[i].cells[0].innerHTML;
        cell = document.createElement("td");
        if (i > 0)
            cell.appendChild(check);
        else if (i == 0)
            cell.appendChild(createCheckAll(table.id));
        table.rows[i].insertBefore(cell, table.rows[i].cells[0]);
    }
}

/**
 * function creates checkbox element that checks all and unchecks all checkboxes in table by given id
 * @param {string} tableId 
 */
function createCheckAll(tableId) {
    checkAll = document.createElement("input");
    checkAll.type = "checkbox";
    checkAll.addEventListener('change', function (e) {
        table = document.getElementById(tableId);
        checkboxes = table.querySelectorAll('input[type=checkbox]');
        for (let i = 0; i < checkboxes.length; i++) {
            if (e.target.checked) {
                if (getParentCell(checkboxes[i]).parentElement.style.display != "none")//checks only visable table rows
                    checkboxes[i].checked = true;
            }
            else checkboxes[i].checked = false;
        }
    });
    return checkAll;
}
//#endregion

//#region validation and errors 
//function show error message for 4 seconds
function showErrMsg(msg) {
    let errMsg=document.getElementById("errmsg_div");
    let errIcon = document.createElement("span");
    errIcon.innerHTML = "error_outline";
    errIcon.classList.add("material-icons","error");
    errIcon.style.fontSize="20px";
    let msg_txt=document.createTextNode(' '+msg);
    errMsg.appendChild(errIcon);
    errMsg.appendChild(msg_txt);
    errMsg.style.display="";
    window.scrollTo(0,0);
    setTimeout(function(){
        errMsg.innerHTML="";
        errMsg.style.display="none";},4000);
}

function textValidation(event) {
    var msg='';
    if(event.target.validity.valueMissing){
        msg="לא ניתן להשיר את השדה ריק";
    }
    else if(event.target.validity.tooLong){
        msg="הטקסט שהוכנס ארוך מדי";
    }
    else if(event.target.validity.patternMismatch) {
        msg="טקסט לא יכול להכיל רק רווחים";
    }
    event.target.setCustomValidity(msg);
}

function nameValidation(event) {
    var msg='';
    if(event.target.validity.valueMissing){
        msg="לא ניתן להשיר את השדה ריק";
    }
    else if(event.target.validity.tooLong){
        msg="שם שהוכנס ארוך מדי";
    }
    else if(event.target.validity.patternMismatch) {
        msg="שם לא יכול להכיל רק רווחים";
    }
    event.target.setCustomValidity(msg);
}

//#endregiom

//#region UNUSED
/**************** UNUSED *****************
//returnes an object with the cell's index in a table
//the object is build as:
// ret_obj:
//          obj.row => the row index
//          obj.column => the column index
//if the cell is not found, -1 will be returned as values
function findCellIndexTable(table,cell)
{
    let ret_obj = {"row": -1, "column":-1};
    let MAX_ROW = table.rows.length;
    let MAX_COLUMN = table.rows[0].cells.length;
    for(i=0;i<MAX_ROW;i++)
        for(j=0;j<MAX_COLUMN;j++)
        {
            if(table.rows[i].cells[j] == cell)
            {
                ret_obj.row = i;
                ret_obj.column = j;
            }
        }
    return ret_obj;
}

//
//  *
//  * @param {an HTMLTable cell element} tableCell
//  * check the  data type of a cell's innerHTML string (if it's a number or a regular string)
//  * and returns an appropriate function to help get a uniform value
//  * e.g:
//  *  - if it's a string it will always be lowercase
//  *  - if it's a number string it will always be a float value
//  */
// function dataConvertingFunction(tableCell)
// {
//     if(isNaN(tableCell.innerHTML))
//         return getLowercaseString;
//     return getFloatValueFromString;
// }
// /**
//  * 
//  * @param {any} value
//  * returns a lowercase strting of value
//  */
// function getLowercaseString(value)
// {
//     return value.toLowerCase();
// }
// /**
//  * 
//  * @param {numaric string} value 
//  * returns a float from value
//  */
// function getFloatValueFromString(value)
// {
//     return parseFloat(value);
// }

// /**
//  * 
//  * @param {the row that needs to be chaned to an edit row} originalRow
//  * returns a table row with input fields to update a question
//  */
// function getQuestionEditRow(originalRow)
// {
//     let editRow = document.createElement("tr");
//     let idCell = editRow.insertCell();
//     let textCell = editRow.insertCell();
//     let cancelCell = editRow.insertCell();
//     let confirmCell = editRow.insertCell();

//     idCell.innerHTML = originalRow.cells[0].innerHTML;

//     let input = document.createElement("textarea");
//     input.setAttribute("type","text");
//     input.defaultValue = originalRow.cells[1].innerHTML.toString();
//     textCell.appendChild(input);

//     cancelCell.innerHTML = "<a><span class='cancel' title='cancel' style='color: red;'>"+BOLD_X+"</span></a>";

//     confirmCell.innerHTML = "<a><span class='confirm' title='confirm' style='color: green;'>"+CHECKMARK+"</span></a>";

//     return editRow;
// }
//#endregion