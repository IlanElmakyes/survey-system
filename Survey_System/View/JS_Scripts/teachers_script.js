const MAX_NAME_LENGTH = 50;

//function run on page body load- initialize all elements
function pageInitialize() {
    xhttpPostRequest("TeachersAction=getTeachers", buildTeachersTable);
    setViewClassesButtonsEvents();

    document.getElementById("add_teacher_btn").addEventListener('click', function () {
        addNewTeacher();
    });

    document.getElementById("cancel").addEventListener('click', function () {
        document.getElementById("edit_classes").style.display = "none";
        document.getElementById("all_classes").style.display = "none";
    });

    document.getElementById("save_btn").addEventListener('click', function () {
        saveChanges(this.value);
    });

    document.getElementById("remove_classes_btn").addEventListener('click', removeClasses);
    document.getElementById("add_classes_btn").addEventListener('click', addClasses);
}

/**
 *
 * @param {string} params - string with parametrs to be send in request
 * @param {function} successFunc - function will be caled when request successfully finished
 * request data from  "../Controller/FacultiesClassesHandler.php"
 */
function xhttpPostRequest(params, successFunc) {
    spinnerStart();
    let xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            let data = JSON.parse(this.response);
            spinnerStop();
            successFunc(data);
        }
    };
    xhttp.open("POST", "../Controller/TeachersHandler.php", true);
    xhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhttp.send(params);
}

/**
 * Function send json data in POST request to server
 * @param {object} data  - object with fields (assoc array) - data to be send to server
 * @param {string} action  - action to be performed on server
 * @param {*} successFunc  - function to be called after server respond
 */
function xhttpSendJsonPost(data, action, successFunc) {
    spinnerStart();
    let jsonData = JSON.stringify(data);
    var toSend = new FormData();
    toSend.append("dataToSave", jsonData);
    toSend.append("TeachersAction", action);
    //AJAX request
    var xhttp = new XMLHttpRequest();
    xhttp.onload = function () {
        var res = JSON.parse(this.response);
        spinnerStop();
        successFunc(res);
    };
    xhttp.open('POST', "../Controller/TeachersHandler.php", true);
    xhttp.send(toSend);
}

//build table with all teachers
function buildTeachersTable(data) {
    let div = document.getElementById("teachers_div");
    let table = document.createElement("table");
    table.id = "teachers_table";
    div.appendChild(table);
    fillTable(table, data);
    addCheckBox(table);
    //setting click eventHandler on th tags
    table.addEventListener('click', function (e) {
        if (e.target.nodeName.toUpperCase() !== "TH") return;
        header_click(e.target);
    });
    addDeleteEdit(table,
        function (tableCell) {//delete onclick function
            deleteTeacher(tableCell.parentElement.cells[1].innerHTML);
        },
        function (tableCell) {//edit onclick function
            editTeacher(tableCell.parentElement.cells[1].innerHTML);
        });
    addViewMore(table, teachersOnViewMoreClick);
    correctHeader(table);
    addSearchBar(table);
}

//function delete teacher
function deleteTeacher(id) {
    if (confirm("למחוק את המרצה?")) {
        let params = "TeachersAction=deleteTeacher&TeacherId=" + id;
        xhttpPostRequest(params, function (res) {
            if (res != 0) {
                alert(res);
            }
            else {
                document.location.reload(true);
            }
        });
    }
}

//function open teacher info for editing
function editTeacher(id) {
    let params = "TeachersAction=editTeacher&TeacherId=" + id;
    xhttpPostRequest(params, function (res) {
        if (res[0] != 0) {
            alert(res[0]);
        }
        else {
            document.getElementById("view_classes").style.display = "none";
            document.getElementById("teacher_name_input").value = res[1].name;
            setButtonsValue(document.getElementById("edit_buttons_holder"), id);
            buildEditClasses(res[2]);
            buildAllClasses(res[3]);
        }

    });
}

//function builds table with all teacher classes for editing classes list
function buildEditClasses(data) {
    var div = document.getElementById("editclasses_div");
    cleanElement(div);
    var table = document.createElement("table");
    table.id = "editclasses_table";
    div.appendChild(table);
    fillTableWithId(table, data);
    table.addEventListener('click', function (e) {
        if (e.target.nodeName.toUpperCase() !== "TH") return;
        header_click(e.target);
    });
    addCheckBox(table);
    addSearchBar(table);
    document.getElementById("edit_classes").style.display = "";
    window.scrollTo(0,0);
}

//function builds table with all classes
function buildAllClasses(data) {
    var div = document.getElementById("classes_div");
    cleanElement(div);
    var table = document.createElement("table");
    table.id = "classes_table";
    div.appendChild(table);
    fillTableWithId(table, data);
    table.addEventListener('click', function (e) {
        if (e.target.nodeName.toUpperCase() !== "TH") return;
        header_click(e.target);
    });
    addCheckBox(table);
    addSearchBar(table);
    document.getElementById("all_classes").style.display = "";
}

//function opens table with classes list for given teacher
function teachersOnViewMoreClick(cell) {
    if (cell === null)
        return;
    document.getElementById("edit_classes").style.display = "none";
    document.getElementById("all_classes").style.display = "none";
    var row = cell.parentElement;
    var div = document.getElementById("viewclasses_div");
    cleanElement(div);
    var table = document.createElement("table");
    table.id = "viewclasses_table";
    div.appendChild(table);
    let params = "TeachersAction=getClasses&TeacherId=" + row.cells[1].innerHTML;
    xhttpPostRequest(params, function (data) {
        document.getElementById("teacher_header").innerHTML = "מרצה " + row.cells[2].innerHTML;
        fillTableWithId(table, data);
        table.addEventListener('click', function (e) {
            if (e.target.nodeName.toUpperCase() !== "TH") return;
            header_click(e.target);
        });
        addSearchBar(table);
        setButtonsValue(document.getElementById("view_buttons_holder"), row.cells[1].innerHTML);
        document.getElementById("view_classes").style.display = "";
    });
    window.scrollTo(0,0);
}

/**
 * Function removes all childs of element
 * @param {object} element - DOM element to be cleaned
 * 
 */
function cleanElement(element) {
    while (element.firstChild) {
        element.removeChild(element.firstChild);
    }
}

//fucntion sets vaue to all buttons in given div
function setButtonsValue(div, val) {
    btns = div.getElementsByTagName("button");
    for (let i = 0; i < btns.length; i++) {
        btns[i].value = val;
    }
}

//function sets events to all buttons in view classes div
function setViewClassesButtonsEvents() {
    document.getElementById("delete_teacher").addEventListener('click', function () {
        deleteTeacher(this.value);
    });
    document.getElementById("edit_classes_btn").addEventListener('click', function () {
        editTeacher(this.value);
    });
    document.getElementById("close_view_classes").addEventListener('click', function () {
        document.getElementById("view_classes").style.display = "none";
    });
}

//function send request to server to add new teacher
function addNewTeacher() {
    if(document.getElementById("new_teacher_input").reportValidity()) {
    let newTeacherName = document.getElementById("new_teacher_input").value;
            let params = "TeachersAction=addNewTeacher&TeacherName=" + newTeacherName;
            xhttpPostRequest(params, function (res) {
                if (res != 0) {
                    alert(res);
                }
                document.location.reload(true);
            });
        }
}

//function saves all changes for teacher to database
function saveChanges(teacherId) {
    if(document.getElementById("teacher_name_input").reportValidity()) {
    let teacherName = document.getElementById("teacher_name_input").value;
            let teacherClasses = getTeacherClasses();
            let data = {
                id: teacherId,
                name: teacherName,
                classes: teacherClasses
            };
            xhttpSendJsonPost(data, "updateTeacher", function (res) {
                if (res == 0)
                    document.location.reload(true);
            });
        }
}

//function gets all teacher classes from table after editing
function getTeacherClasses() {
    let table = document.getElementById("editclasses_table");
    let classes = [];
    for (let i = 1; i < table.rows.length; i++) {
        classes.push(table.rows[i].value);
    }
    return classes;
}

//function removes chosen classes from given teacher classes list
function removeClasses() {
    let table = document.getElementById("editclasses_table");
    let checkbox = table.querySelectorAll('input[type=checkbox]');//get all checkboxes from table
    for (let i = checkbox.length - 1; i >= 1; i--) { // for each checked - loop backwards     
        if (checkbox[i].checked) {
            table.deleteRow(i);
        }
    }
}

//function adds all chosen classes to given teacher
function addClasses() {
    let sourceTable = document.getElementById("classes_table");
    let destTable = document.getElementById("editclasses_table");
    let isDuplicates = false;
    let checkbox = sourceTable.querySelectorAll('input[type=checkbox]');//get all checkboxes from table
    for (let i = 1; i < checkbox.length; i++) { // for each checked     
        if (checkbox[i].checked) {
            let sourceRow = sourceTable.rows[i];
            let exist = false;
            for (let j = 0; j < destTable.rows.length; j++) {//check for duplicates
                if (destTable.rows[j].value == sourceRow.value) {
                    exist = true;
                    isDuplicates = true;
                }
            }
            if (!exist) {
                let destRow = destTable.insertRow(-1);//add row to table
                deepCopyTableRow(destRow, sourceRow);
            }
        }
    }
    if (isDuplicates) {//show error message if there duplicates
        showErrMsg("חלק מהכיתות כבר קיימות ברשימה, נוספו רק כיתות חדשות");
    }
}
