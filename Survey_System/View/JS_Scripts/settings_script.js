//function run on page body load- initialize all elements
function pageInitialize() {
    //show current timer setting
    xhttpPostRequest("SettingsAction=getCurrentTimer", function (res) {
        if (res != 0) {
            updateCurrentTimerSetting(res);
        }
    });

    document.getElementById("set_timer_btn").addEventListener('click', updateTimer);

    credentialsModalInitialization();

}

function initModalOpenAndClose(modal, openModal, closeModal) {
    openModal.onclick = () => modal.style.display = "block";
    closeModal.onclick = () => modal.style.display = "none";
}

function credentialsModalInitialization() {
    initModalOpenAndClose(
        document.getElementById("changeCredentialsModal"),
        document.getElementById("btn_openNewCredentialsModal"),
        document.getElementById("btn_closeNewCredentialsModal")
    );
    let inputs = {
        oldUsername: document.getElementById("oldUsernameInput"),
        newUsername: document.getElementById("newUsernameInput"),
        oldPassword: document.getElementById("oldPasswordInput"),
        newPassword: document.getElementById("newPasswordInput"),
        confirmPassword: document.getElementById("confirmPasswordInput")
    }
    inputs.newUsername.onchange = validateInputPattern;
    inputs.newPassword.onchange = validateInputPattern;
    inputs.confirmPassword.onchange = confirmPasswordMatch;

    document.getElementById("changeCredentialsForm").onsubmit = submitChangeCredentials;
}

function validateInputPattern() {
    let input = this;
    if (!input)
        return;
    if (!input.value.match(/[A-Za-z0-9]{3,16}/))
        input.setCustomValidity(`Use only latin letters and numbers.
        Min 3 characters, 16 max`);
    else input.setCustomValidity('');
}

function confirmPasswordMatch(newPassword, confirmPassword) {
    if (!newPassword || !confirmPassword)
        return;
    if (newPassword.value !== confirmPassword.value)
        confirmPassword.setCustomValidity("Passwords must match");
    else confirmPassword.setCustomValidity('');
}

function submitChangeCredentials() {
    let inputsValue = {
        oldUsername: document.getElementById("oldUsernameInput").value,
        newUsername: document.getElementById("newUsernameInput").value,
        oldPassword: document.getElementById("oldPasswordInput").value,
        newPassword: document.getElementById("newPasswordInput").value,
        confirmPassword: document.getElementById("confirmPasswordInput").value
    };
    spinnerStart();
    let xhttp = new XMLHttpRequest();
    let toSend = new FormData();
    var response;
    toSend.append("inputs", JSON.stringify(inputsValue));
    console.log(JSON.stringify(inputsValue));
    toSend.append("SettingsAction", "changeCredentials");
    xhttp.onload = function () {
        response = JSON.parse(this.response);
        response ? alert("הנתונים עודכנו בהצלחה!") : alert("הנתונים לא עדכנו, אנא וודא/י שהנתונים שהוכנסו הינם תקינים");
        spinnerStop();
        location.reload();
    };
    xhttp.open("POST", "../Controller/SettingsHandler.php", true);
    xhttp.send(toSend);

    return false;
}

/**
 *
 * @param {string} params - string with parametrs to be send in request
 * @param {function} successFunc - function will be caled when request successfully finished
 * request data from  "../Controller/SettingsHandler.php"
 */
function xhttpPostRequest(params, successFunc) {
    spinnerStart();
    let xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            let data = JSON.parse(this.response);
            successFunc(data);
            spinnerStop();
        }
    };
    xhttp.open("POST", "../Controller/SettingsHandler.php", true);
    xhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhttp.send(params);
}

//function sending data to server for saving
function xhttpSendJsonPost(data, action, successFunc) {
    let jsonData = JSON.stringify(data);
    var toSend = new FormData();
    toSend.append("dataToSave", jsonData);
    toSend.append("SettingsAction", action);
    //AJAX request
    spinnerStart();
    var xhttp = new XMLHttpRequest();
    xhttp.onload = function () {
        var res = JSON.parse(this.response);
        successFunc(res);
        spinnerStop();
    };
    xhttp.open('POST', "../Controller/SettingsHandler.php", true);
    xhttp.send(toSend);
}

//update current timer setting on page
function updateCurrentTimerSetting(timerStr) {
    var curTimer = document.getElementById('current_timer_setting');
    curTimer.innerHTML = timerStr;
}

//update timer step setting on server 
function updateTimer() {
    if (validateTimerInput()) {
        var data = prepareTimerData();
        xhttpPostRequest("SettingsAction=updateTimer&TimerStep=" + data, function (res) {
            if (res != 0) {
                updateCurrentTimerSetting(res);
            }
            else
                alert("משהו השתבש בעדכון טיימר...");
        })
    }
}


//check input before sending to server
function validateTimerInput() {
    var days = document.getElementById("days").value;
    var hours = document.getElementById("hours").value;
    var minutes = document.getElementById("minutes").value;
    if (document.getElementById("days").reportValidity()) {
        if (document.getElementById("hours").reportValidity()) {
            if (document.getElementById("minutes").reportValidity()) {
                    return true;
            }
        }
    }
    return false;
}


//take user input for timer step settings and convert to DatetimeInterval format readble by php
function prepareTimerData() {
    var days = document.getElementById("days").value;
    var hours = document.getElementById("hours").value;
    var minutes = document.getElementById("minutes").value;
    if (days.length == 0)
        days = 0;
    if (hours.length == 0)
        hours = 0;
    if (minutes.length == 0)
        minutes = 0;
    return "P" + days + "DT" + hours + "H" + minutes + "M";
}


function timerValidation(event) {
    var msg='';
    if(event.target.validity.valueMissing){
        msg="לא ניתן להשיר את השדה ריק";
    }
    else if(event.target.validity.typeMismatch){
        msg="נא להכניס מספר שלם חיובי";
    }
    else if(event.target.validity.rangeOverflow) {
        msg="ערך גדול מדי, נא להכניס מספר בתווך "+event.target.min+"-"+event.target.max;
    }
    else if(event.target.validity.rangeUnderflow) {
        msg="ערך קטן מדי, נא להכניס מספר בתווך "+event.target.min+"-"+event.target.max;
    }
    event.target.setCustomValidity(msg);
}