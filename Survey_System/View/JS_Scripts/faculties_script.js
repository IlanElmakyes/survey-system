const MAX_NAME_LEN=50;


//function run on page body load- initialize all elements
function pageInitialize() {
    xhttpPostRequest("FacultiesAction=getFaculties",buildFacultiesTable);
    setViewClassesButtonsEvents();
    
    document.getElementById("close_teachers_btn").addEventListener('click',function(){
        document.getElementById("view_teachers").style.display="none";
    });
    
    document.getElementById("add_faculty_btn").addEventListener('click',onClickAddFaculty);
    
    document.getElementById("cancel").addEventListener('click',function(){
        document.getElementById("view_teachers").style.display="none";
        document.getElementById("edit_classes").style.display="none";
    });

    document.getElementById("save_btn").addEventListener('click',function(){
        onClickSaveFaculty(this.value);
    });

    document.getElementById("add_class_btn").addEventListener('click',function() {
        onClickAddClass(this.value);
    });
}

//build table with all faculties
function buildFacultiesTable(data) {
    let div=document.getElementById("faculties_div");
    let table=document.createElement("table");
    table.id="faculties_table";
    div.appendChild(table);
    fillTableWithId(table,data);
    //setting click eventHandler on th tags
    table.addEventListener('click',function(e){
        if(e.target.nodeName.toUpperCase() !== "TH") return;
        header_click(e.target);
        });
    addDeleteEdit(table,
        function(tableCell){//delete onclick function
        deleteFaculty(tableCell.parentElement.value);
    },
        function(tableCell){//edit onclick function
        editFaculty(tableCell.parentElement.value);
    });
    addViewMore(table,facultiesOnViewMoreClick);
    correctHeader(table);
    addCheckBox(table);
    addSearchBar(table);
}

/**
 *
 * @param {string} params - string with parametrs to be send in request
 * @param {function} successFunc - function will be caled when request successfully finished
 * request data from  "../Controller/FacultiesClassesHandler.php"
 */
function xhttpPostRequest(params,successFunc) {
    spinnerStart();
    let xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function(){
        if (this.readyState == 4 && this.status == 200){
            let data = JSON.parse(this.response);
            successFunc(data);
            spinnerStop();
        }
    };
    xhttp.open("POST","../Controller/FacultiesClassesHandler.php",true);
    xhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhttp.send(params);    
}

//function recieve faclty Id and deletes it if its not part of active survey
function deleteFaculty(id) {
    if(confirm("למחוקת את המגמה ואת כל הכיתות השייכות?")) {
        let params="FacultiesAction=deleteFaculty&FacultyId="+id;
        xhttpPostRequest(params,function(res){
            if(res!=0) {
                alert(res);
            }
            else{
                let table=document.getElementById("faculties_table");
                let row=getRowByValue(table,id);
                table.deleteRow(row.rowIndex);
                document.getElementById("view_teachers").style.display="none";
                document.getElementById("view_classes").style.display="none";
                document.getElementById("edit_classes").style.display="none";                
            }
        });
    }
}

//function recieve faculty ID and open it for editing if its not part of active survey
function editFaculty(id) {
    let params="FacultiesAction=editFaculty&FacultyId="+id;
    xhttpPostRequest(params,function(res){
        if(res[0]==0) {
            document.getElementById("view_classes").style.display="none";
            document.getElementById("view_teachers").style.display="none";
            document.getElementById("new_class_input").value="";
            document.getElementById("faculty_name_input").value=res[1].name;
            buildEditClassesTable(res[1].id);
        }
        else alert(res[1]);           
    });
}

//function builds edit div 
function buildEditClassesTable(facultyId) {
    div=document.getElementById("editclasses_div");
    cleanElement(div);
    table=document.createElement("table");
    table.id="editclasses_table";
    div.appendChild(table);
    let params="FacultiesAction=getClasses&FacultyId="+facultyId;
    xhttpPostRequest(params,function(data){
        fillTableWithId(table,data);
        table.addEventListener('click',function(e){
            if(e.target.nodeName.toUpperCase() !== "TH") return;
            header_click(e.target);
            });
        addDeleteEdit(table,
            function(tableCell){//delete onclick function
            deleteClass(tableCell.parentElement.value);
        },
            function(tableCell){//edit onclick function
            editClass(tableCell.parentElement.value);
        });
        addViewMore(table,classesOnViewMoreClick);
        correctHeader(table);
        addSearchBar(table);
        setButtonsValue(document.getElementById("edit_buttons_holder"),facultyId);
        document.getElementById("add_class_btn").value=facultyId;
        document.getElementById("edit_classes").style.display="";
    });
}


//function send request to delete class to server
//if class was deleted - delete class from table on page
function deleteClass(id) {
    document.getElementById("view_teachers").style.display="none";
    if(confirm("למחוק את הכיתה?")) {
        let params="FacultiesAction=deleteClass&ClassId="+id;
        xhttpPostRequest(params,function(res){
            if(res==0){
                table=document.getElementById("editclasses_table");
                let row=getRowByValue(table,id);
                table.deleteRow(row.rowIndex);
            }
        });
    }
}

//function edits class name if name is not empty and not too long
//shows error messages if can't edit class name
function editClass(id) {
    document.getElementById("view_teachers").style.display="none";
    let table = document.getElementById("editclasses_table");
    let row=getRowByValue(table,id);

    convertToClassEditRow(row);
    table.rows[row.rowIndex].addEventListener("click",function(e){
        switch (e.target.className.toUpperCase()) {
            case "CANCEL":
               convertToRegularRow(row);
                break;
            case "CONFIRM":               
                if(document.getElementById("editclass_input").reportValidity()){
                    if(confirm("לשמור שינויים?")){
                        let name=document.getElementById("editclass_input").value;
                        updateClass(row.value,name);
                        convertToRegularRow(row);
                    }
                    else    
                        convertToRegularRow(row);
                }
                break;
        }
    });
}

//function send request to server to update specific class
//shows error message if class wasn't updated 
function updateClass(id,name) {
    let classToUpdate={'id':id,'name':name};
    xhttpSendJsonPost(classToUpdate,"updateClass",function(res){
        if(res==0) {
            let table=document.getElementById("editclasses_table");
            let row=getRowByValue(table,id);
            row.cells[0].innerHTML=name;            
        }
        else alert(res);
    });
}


//function converst table row from edtinig row to regular row
function convertToRegularRow(row) {
    let editCells=row.querySelectorAll("td[class='editCell']");
    for(let i=0;i<editCells.length;i++){
        row.deleteCell(editCells[i].cellIndex);
    }
    for(let i=0;i<row.cells.length;i++) {
        row.cells[i].style.display="";
    }
}


//change row to allow inrow editing
function convertToClassEditRow(row) {
    for(let i=0;i<row.cells.length;i++) {
        row.cells[i].style.display="none";
    }

    let input=document.createElement("input");
    input.id="editclass_input";
    input.type="text";
    input.required=true;
    input.setAttribute("maxLength",MAX_NAME_LEN);
    input.pattern='^.*\\S+.*$';
    input.addEventListener('invalid',nameValidation);
    input.value=row.cells[0].innerHTML;
    let inputCell=row.insertCell();
    let cancelCell = row.insertCell();
    let confirmCell = row.insertCell();
    inputCell.setAttribute("class","editCell");
    inputCell.appendChild(input);
    cancelCell.innerHTML="<a><span class='cancel' title='cancel' style='color: red;'>"+BOLD_X+"</span></a>";
    cancelCell.setAttribute("class","editCell");
    confirmCell.innerHTML= "<a><span class='confirm' title='confirm' style='color: green;'>"+CHECKMARK+"</span></a>";
    confirmCell.setAttribute("class","editCell");
}

//function send request to server to add new faculty if entered faculty name is not empty or too long
//show error message if faculty name is not valid
//alert message from server if faculty wasn't added
function onClickAddFaculty() {
    if(document.getElementById("new_faculty_input").reportValidity()) {
        let newFacultyName=document.getElementById("new_faculty_input").value;
        let params="FacultiesAction=addNewFaculty&FacultyName="+newFacultyName;
        xhttpPostRequest(params,function(res){
            if(res!=0) {
                alert(res);
            }
            document.location.reload(true);
        });
    }
}

//function send faculty id and name to de updated if name is not empty or too long
//shows error messages if can't send name to update or name wasn't updated on server
function onClickSaveFaculty(facultyId) {   
    if(document.getElementById("faculty_name_input").reportValidity()) {
        let newFacultyName=document.getElementById("faculty_name_input").value;
        let data={
            id: facultyId,
            name: newFacultyName
        };
        xhttpSendJsonPost(data,"editFacultyName",function(res){
            if(res!=0) {
                alert(res);
            }
            document.location.reload(true);
        });
    }
}


//function send request to server to add new class to faculty if class name is not empty and not too long
//show error message if can't send request
//alert message from server if class wasn't added
function onClickAddClass(facultyId) {    
    if(document.getElementById("new_class_input").reportValidity()) {
        let newClassName=document.getElementById("new_class_input").value;
        let newClass={
            'name':newClassName,
            'facultyId':facultyId
        }
        xhttpSendJsonPost(newClass,"addNewClass",function(res){
            if(res===0){
                buildEditClassesTable(facultyId);
                document.getElementById("new_class_input").value="";
            }
            else alert(res);
        });
    }
}

//funciton recieve clicked cell in facuities table 
//shows all classes for given faculty
function facultiesOnViewMoreClick(cell) {
    if(cell === null)
        return;
    document.getElementById("edit_classes").style.display="none";
    document.getElementById("view_teachers").style.display="none";
    var row=getParentTable(cell).rows[cell.parentElement.rowIndex];
    var div=document.getElementById("viewclasses_div");
    cleanElement(div);
    var table=document.createElement("table");
    table.id="viewclasses_table";
    div.appendChild(table);
    let params="FacultiesAction=getClasses&FacultyId="+row.value;
    xhttpPostRequest(params,function(data){
        document.getElementById("faculty_header").innerHTML="מגמת "+row.cells[1].innerHTML;
        fillTableWithId(table,data);
        table.addEventListener('click',function(e){
            if(e.target.nodeName.toUpperCase() !== "TH") return;
            header_click(e.target);
            });
        addViewMore(table,classesOnViewMoreClick);
        addSearchBar(table);
        correctHeader(table);
        setButtonsValue(document.getElementById("view_buttons_holder"),row.value);
        document.getElementById("view_classes").style.display="";
    });
}

/**
 * Function removes all childs of element
 * @param {object} element - DOM element to be cleaned
 * 
 */
function cleanElement(element) {
    while (element.firstChild) {
        element.removeChild(element.firstChild);
    }
}

//fucntion sets vaue to all buttons in given div
function setButtonsValue(div,val) {
    btns=div.getElementsByTagName("button");
    for(let i=0;i<btns.length;i++) {
        btns[i].value=val;
    }
}

//function recieve clicked cell in classes table
//shows all teachers for given class
function classesOnViewMoreClick(cell) {
    if(cell === null)
        return;
    var row=getParentTable(cell).rows[cell.parentElement.rowIndex]; 
    var div=document.getElementById("teachers_div");
    cleanElement(div);
    var table=document.createElement("table");
    table.id="teachers_table";
    div.appendChild(table);
    let params="FacultiesAction=getTeachers&ClassId="+row.value; 
    xhttpPostRequest(params,function(data){
        document.getElementById("class_header").innerHTML=row.cells[0].innerHTML;
        fillTableWithId(table,data);
        addSearchBar(table);
        correctHeader(table);
        table.addEventListener('click',function(e){
            if(e.target.nodeName.toUpperCase() !== "TH") return;
            header_click(e.target);
            });
        document.getElementById("view_teachers").style.display="";
    });
}

//function sets events to all buttons in view classes div
function setViewClassesButtonsEvents() {
    document.getElementById("delete_faculty").addEventListener('click',function(){
        deleteFaculty(this.value);
    });
    document.getElementById("edit_classes_btn").addEventListener('click',function(){
        editFaculty(this.value);
    });
    document.getElementById("close_view_classes").addEventListener('click',function(){
        document.getElementById("view_teachers").style.display="none";
        document.getElementById("view_classes").style.display="none";
    });
}

function xhttpSendJsonPost(data,action,successFunc) {
    spinnerStart();
    let jsonData=JSON.stringify(data);
    var toSend=new FormData();
    toSend.append("dataToSave",jsonData);
    toSend.append("FacultiesAction",action);
    //AJAX request
    var xhttp = new XMLHttpRequest();
    xhttp.onload = function(){
        var res =JSON.parse(this.response);
        successFunc(res);
        spinnerStop();
    };
    xhttp.open('POST',"../Controller/FacultiesClassesHandler.php",true);
    xhttp.send(toSend);
}

