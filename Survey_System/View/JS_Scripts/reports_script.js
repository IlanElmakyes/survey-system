const TEACHER = "teacher";
const CLASS = "class";
const FACULTY = "faculty";
const EXTRA_CATEGORYS = "קטגוריות נוספות";
//#region Short Report
/**
 * 
 * @param {HTMLTableElement} teacherTable - pass other then null to create reports for teachers 
 * @param {HTMLTableElement} facultyTable - pass other then null to create reports for faculties
 * 
 * **note: if both arguments passed other then null, the function will try to create for teachers**
 * the function starts the process of creating reports according to passed arguments
 * also, it un-hides the spoiler button for the newly created "reports_div"
 */
function createReports(teacherTableId = false, facultyTable = false) {
    if (!teacherTableId && !facultyTable)
        return;
    if (teacherTableId)
        fillReportDiv(document.getElementById(teacherTableId), null);
    else
        fillReportDiv(null, document.getElementById(facultyTable));
    document.getElementById("reports_spoiler").style.display = "";
    document.getElementById("report_download").style.display = "";
}

function getDateHeader() {
    var h2 = document.createElement("h2");
    h2.id = "report_date";
    h2.innerHTML = 'דו"ח ל' + new Date().toLocaleDateString('he-IL', { year: 'numeric', month: 'long' });
    return h2;
}
function getDownloadbutton(type) {
    let aDownload = document.createElement("a");
    aDownload.setAttribute("href", "#");
    aDownload.onclick = function () {
        getCSVFile(type);
        return false;//this false is to stop the broswer from redirecting after the click
    };
    let downloadSpan = document.createElement("span");
    downloadSpan.innerHTML = "save_alt";
    downloadSpan.classList.add("material-icons", "download");
    downloadSpan.title = "download";
    downloadSpan.style.fontSize = "16px";
    aDownload.appendChild(downloadSpan);
    return aDownload;
}
/**
 * 
 * @param {HTMLTableElement} teacherTable - pass other then null to create reports for teachers 
 * @param {HTMLTableElement} facultyTable - pass other then null to create reports for faculties
 * 
 * **note: if both arguments passed other then null, the function will try to create for teachers**
 * creates - or if existes, clean and fill -"reports_div" element
 * that fill be filled with teachers/faculties answer data accoding to the arguments passesed
 */
async function fillReportDiv(teacherTable = null, facultyTable = null) {
    if (teacherTable == null && facultyTable == null)
        return null;
    let proxyTable = teacherTable ? teacherTable : facultyTable;
    let type = teacherTable ? TEACHER : FACULTY;
    if (document.getElementById("report_div"))
        document.getElementById("report_div").parentElement.removeChild(document.getElementById("report_div"));
    let div = document.createElement("div");
    div.className = "report_container";
    div.id = "report_div";
    div.appendChild(getDateHeader());
    let rowCount = proxyTable.rows.length;
    switch (type) {
        case TEACHER:
            for (let i = 1; i < rowCount; i++) {
                let data = {
                    id: proxyTable.rows[i].cells[1].innerHTML,
                    name: proxyTable.rows[i].cells[2].innerHTML
                };
                if (proxyTable.rows[i].cells[0].children[0].checked)
                    teacherReport(data, type, div);

            }
            break;
        case FACULTY:
            for (let i = 1; i < rowCount; i++) {
                let data = {
                    id: proxyTable.rows[i].value,
                    name: proxyTable.rows[i].cells[1].innerHTML
                };

                if (proxyTable.rows[i].cells[0].children[0].checked)
                    facultyReport(data, div);
            }
    }
    document.getElementById("reports").append(div);
}
async function teacherReport(data, type, div) {
    await getSingleReportDiv(data, type).then((resolvedDiv) => div.append(resolvedDiv));
}
/**
 * 
 * @param {object} facultyData - {id: string of the faculty id,
 *                                name: string of the faculty name} 
 * @param {HTMLDivElement} div - the div that will contain the reports
 * 
 * appends a created div into the given div.
 * the new div is filled with a headline of the faculty and divs contaning
 * each faculty's classes answer information in tables
 */
function facultyReport(facultyData, div) {
    let facultyDiv = document.createElement("div");
    let h3 = document.createElement("h3");
    h3.textContent = "מגמת " + facultyData.name;
    facultyDiv.append(h3);
    spinnerStart();
    let xhttp = new XMLHttpRequest();
    let sendData = new FormData();
    sendData.append("reportAction", "faculty");
    sendData.append("id", facultyData.id);
    xhttp.onload = async function () {
        classesData = JSON.parse(this.response);
        for (classData of classesData) {
            await getSingleReportDiv(classData, CLASS).then((resolvedDiv) => facultyDiv.append(resolvedDiv));
        }
        spinnerStop();
    };
    xhttp.open("POST", "../Controller/ReportHandler.php");
    xhttp.send(sendData);
    div.append(facultyDiv);
}
/**
 * 
 * @param {object} data - {name: will be a headline above the table,
 *                         id: - the id of the teacher/class}
 * @param {string} type - should be either "teacher" or "class", depending on what data to fetch
 * 
 * creates and returns a div with a headline and a table filled with answer data, according to the object and type passed 
 */
function getSingleReportDiv(data, type) {
    return new Promise((resolve, reject) => {
        if (data.id == null || data.id <= 0)
            return null;
        let div = document.createElement("div");
        div.className = "single_report";
        div.id = type + data.id;
        let h3 = document.createElement("h3");
        h3.textContent = data.name;
        let table = document.createElement("table");
        div.append(h3);
        div.append(table);
        table.border = 1;
        spinnerStart();
        let xhttp = new XMLHttpRequest();
        let sendData = new FormData();
        sendData.append("reportAction", type);
        sendData.append("id", data.id);
        xhttp.onload = function () {
            answerData = JSON.parse(this.response);
            fillTable(table, answerData.numeric);
            if (type == TEACHER)
                div.append(getTextAnsweresDiv(answerData.text));
            spinnerStop();
            resolve(div);
        };
        xhttp.open("POST", "../Controller/ReportHandler.php");
        xhttp.send(sendData);
    });
}
//#endregion

//#region CSV Download
/**
 * deletes all reports file on the server.
 * use this function so the server will not have any leftover reports, the user downloads them anyway
 */
function deleteAllReportsOnServer() {
    spinnerStart();
    xhttp = new XMLHttpRequest();
    sendData = new FormData()
    sendData.append("reportAction", "deleteReports");
    xhttp.onload = function () {
        spinnerStop();
    };
    xhttp.open("POST", "../Controller/ReportHandler.php");
    xhttp.send(sendData);
}
/**
 * 
 * @param {string} fileText - the raw text to save to the file 
 * @param {string} fileName - file name with extention
 * 
 * creates an anchor element and simulates a click on it to start a download
 * of a file
 * the anchor is removed from the page after the download has started
 */
function downloadCSV(fileText, name) {
    spinnerStart();
    xhttp = new XMLHttpRequest();
    sendData = new FormData()
    sendData.append("reportAction", "DownloadCSV");
    sendData.append("fileText", JSON.stringify(fileText));
    sendData.append("fileName", name);
    xhttp.onload = function () {
        let fileLocation = JSON.parse(this.response);
        if (!fileLocation) {
            alert("There was a problem generating a report file...");
            return false;
        }
        let fileName = fileLocation.split(/[\\\/]/).pop();
        let a = document.createElement("a");
        a.setAttribute("href", fileLocation);
        a.setAttribute("download", fileName);
        a.style.display = "none";
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
        deleteAllReportsOnServer();
        spinnerStop();
    };
    xhttp.open("POST", "../Controller/ReportHandler.php");
    xhttp.send(sendData);
}
/**
 * 
 * @param {HTMLTableElement} table
 * returns a string of CSV formatted text from a table
 * the function will skip table cells that have tags in them
 * (so if a table cells has an element inside of it and not text) 
 */
function getCSVStringFromTable(table) {
    let csvText = "";
    let rowCount = table.rows.length;
    let columnCount = 0;
    if (rowCount > 0)
        columnCount = table.rows[0].cells.length;
    for (let i = 0; i < rowCount; i++) {
        for (let j = 0; j < columnCount; j++)
            if (!table.rows[i].cells[j].innerHTML.includes("<"))
                csvText += table.rows[i].cells[j].innerHTML + ",";
        csvText += "\n";
    }
    return csvText;
}
/**
 * 
 * @param {string} type - should be "teacher" or "class"
 * 
 * creates a csv file from information in "report_div"
 * and downloads it
 */
function getCSVFile(type) {
    let reportDiv = document.getElementById("report_div");
    type = type == TEACHER ? "מרצים" : "כיתות";
    let fileName = "דוח_" + type + "_" + new Date().toLocaleDateString("he-IL");
    //the file header
    let csvText = ["דו\"ח " + type + "\n\n"];
    for (child of reportDiv.children) {
        for (innerChild of child.children)
            csvText.push(innerChildItirator(innerChild));
    }
    downloadCSV(csvText, fileName);
}
/**
 * 
 * @param {HTMLElement} element
 * 
 * checks if an element is a heading,table or a div
 * then acts accordingly to extract the information from the element in CSV format
 */
function innerChildItirator(element) {
    let csvText = "";
    if (element instanceof HTMLHeadingElement || element instanceof HTMLParagraphElement)
        csvText = element.textContent + "\n";
    else if (element instanceof HTMLTableElement)
        csvText = "\n" + getCSVStringFromTable(element) + "\n";
    else if (element instanceof HTMLParagraphElement)
        csvText = element.textContent + "\n";
    else if (element instanceof HTMLDivElement) {
        for (child of element.children)
            csvText += innerChildItirator(child);
    }
    return csvText;
}
//#endregion


//#region Long Report
/**
 * 
 * @param {string} type - type of the report 
 * @param {*} tableId - the table id to get information from
 * redirects to managerReports page while passing relevent data
 */
function fullReportLoad(type, tableId) {
    let table = document.getElementById(tableId);
    let rowCount = table.rows.length;
    //creating an array with all the checked rows ids to pass to the next page
    let dataArray = { id: [], name: [] };
    if (type == TEACHER) {
        for (let i = 1; i < rowCount; i++) {
            if (table.rows[i].cells[0].children[0].checked) {
                dataArray.id.push(table.rows[i].cells[1].innerHTML);
                dataArray.name.push(table.rows[i].cells[2].innerHTML);
            }
        }
    }
    else if (type == FACULTY) {
        for (let i = 1; i < rowCount; i++) {
            if (table.rows[i].cells[0].children[0].checked) {
                dataArray.id.push(table.rows[i].value);
                dataArray.name.push(table.rows[i].cells[1].innerHTML);
            }
        }
    }
    spinnerStart();
    let xhttp = new XMLHttpRequest();
    let sendData = new FormData();
    sendData.append("reportAction", "fullReportRedirect");
    sendData.append("type", type);
    sendData.append("dataArray", JSON.stringify(dataArray));
    xhttp.onload = function () {
        spinnerStop();
        $location = JSON.parse(this.response);
        window.location.href = $location;
    };
    xhttp.open("POST", "../Controller/ReportHandler.php");
    xhttp.send(sendData);
}
/**
 * getting the session data from the sending page and building the currect report div
 */
function initializeReportPage() {
    spinnerStart();
    let xhttp = new XMLHttpRequest();
    let sendData = new FormData();
    sendData.append("reportAction", "ReportSessionData");
    xhttp.onload = async function () {
        let reportData = JSON.parse(this.response);
        let length = reportData.id.length;
        let reportDiv = document.getElementById("report_div");
        reportDiv.appendChild(getDateHeader());
        for (let i = 0; i < length; i++)
            await createFullReportDiv({ id: reportData.id[i], name: reportData.name[i] }, i, reportData.type)
                .then((resolvedSingleReportDiv) => reportDiv.appendChild(resolvedSingleReportDiv));
        document.getElementById("report_download").style.display = "";
        spinnerStop();
    };
    xhttp.open("POST", "../Controller/ReportHandler.php");
    xhttp.send(sendData);
}
/**
 * 
 * @param {Object} reportData - holds the ID and name of the item to get the report information to
 * @param {number} index - number that represents the current number of reports
 * @param {string} type - TEACHER or FACULTY
 */
function createFullReportDiv(reportData, index, type) {
    return new Promise((resolve, reject) => {
        let h2;
        let tableDiv;
        let singleReportDiv;
        singleReportDiv = document.createElement("div");
        singleReportDiv.className = "single_report";
        singleReportDiv.id = type + index;
        h2 = document.createElement("h2");
        h2.textContent = reportData.name;
        tableDiv = document.createElement("div");
        tableDiv.id = reportData.name + "_tables";
        singleReportDiv.appendChild(h2);
        singleReportDiv.appendChild(tableDiv);
        spinnerStart();
        let xhttp = new XMLHttpRequest();
        let sendData = new FormData();
        sendData.append("reportAction", type == TEACHER ? "fullTeacher" : "fullFaculty");
        sendData.append("id", reportData.id)
        xhttp.onload = function () {
            fillFullReportTableDiv(tableDiv, JSON.parse(this.response), type);
            spinnerStop();
            resolve(singleReportDiv)
        };
        xhttp.open("POST", "../Controller/ReportHandler.php");
        xhttp.send(sendData);
    });
}
/**
 * 
 * @param {HTMLDivElement} tableDiv - the div that holds the reports tables
 * @param {Array} reportDataArray - hold all the report information with the headers as keys
 * @param {string} type - TEACHER or FACULTY
 * fills a given div with all the report data in the given array by the given type
 */
function fillFullReportTableDiv(tableDiv, reportDataArray, type) {
    for (key in reportDataArray) {
        //skipping the text answers property
        if (key === "text")
            continue;
        let generalAvg = 0;
        let avgDivider = 0;
        let h3 = document.createElement("h3");
        h3.textContent = key;
        tableDiv.append(h3);
        list = reportDataArray[key];
        let p = document.createElement("p");
        tableDiv.append(p);
        for (innerKey in list) {
            //tmpSum hold the average from a single key
            let tmpSum = fillSingleKeyForTableDiv(tableDiv, list[innerKey], innerKey, type, key);
            if (type == FACULTY && key == EXTRA_CATEGORYS)
                tableDiv.append(getTextAnsweresDiv(list[innerKey]['text']));

            /*
                generalAvg will hold the total average of all innerKey values
                avgDivider is used to keep track how many values there are - 
                if there is no valid report data the function will return 0 as average, and this will not count
            */
            if (tmpSum > 0) {
                generalAvg += tmpSum;
                avgDivider++;
            }
        }
        if (avgDivider === 0) avgDivider = 1;//this is to prevent division by zero

        if (type == TEACHER || ((type == FACULTY && key == EXTRA_CATEGORYS))) {
            p.textContent = "ממוצע מגמתי - " + (generalAvg / avgDivider).toFixed(2);
        }
        else {
            p.textContent = "ממוצע כיתתי - " + (generalAvg / avgDivider).toFixed(2);
        }
    }
}
/**
 * 
 * @param {HTMLDivElement} tableDiv - the div that holds the reports tables
 * @param {Array} answerArray - an assoc array with 'numeric' elements that holds the data to display
 * @param {string} name - the key for the list object's element
 * @param {string} type - TEACHER or FACULTY
 * fills the given div with the information in the answerArray.
 * if there are numeric answers - creates and appends a table with the information
 * **returns the average of all the questions in the given asnwerArray**
 */
function fillSingleKeyForTableDiv(tableDiv, answerArray, name, type, outerKey) {
    let h4 = document.createElement("h4");
    h4.textContent = name;
    tableDiv.append(h4);
    let avg = 0;
    if (answerArray['numeric'].length > 1) {
        avg = getAverageReportGrade(answerArray['numeric']);
        let p = document.createElement("p");
        if (outerKey == EXTRA_CATEGORYS)
            p.textContent = "ממוצע לקטגוריה - " + avg.toFixed(2);
        else p.textContent = (type == TEACHER ? "ממוצע כיתתי - " : "ממוצע למורה - ") + avg.toFixed(2);
        tableDiv.append(p);
        let table = document.createElement("table");
        tableDiv.append(table);
        fillTable(table, answerArray['numeric']);
    }
    else {
        let p = document.createElement("p");
        p.textContent = "אין מידע זמין";
        tableDiv.append(p);
    }
    return avg;
}
/**
 * 
 * @param {array} tableData - an array with report data information
 * returns the average grade for a single report table
 */
function getAverageReportGrade(tableData) {
    let columnCount = tableData[0].length;
    let rowCount = tableData.length;
    let sum = 0;
    for (let i = 1; i < rowCount; i++)
        sum += Number(tableData[i][columnCount - 1]);
    return sum / (rowCount - 1);
}

//#endregion

/**
 * 
 * @param {Array} textArray - hold an array of all the text answers
 * returns a div with the text answers in the array
 */
function getTextAnsweresDiv(textArray) {
    let textDiv = document.createElement("div");
    textDiv.className = "text_answer";
    let h4 = document.createElement("h4");
    h4.textContent = "תגובות טקסט חופשי";
    textDiv.append(h4);
    for (text of textArray) {
        let p = document.createElement("p");
        p.className = "comments";
        p.textContent = text;
        textDiv.append(p);
    }
    return textDiv;
}