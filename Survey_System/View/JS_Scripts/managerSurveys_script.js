var NEW_SURVEY_ID = -1;
const MAX_NAME_LEN = 50;
function initializeSurveyPage() {
    buildSurveysTable("table_div");
}
/**
 * 
 * @param {HTMLDivElement} divId - the div element that should contain the survey table
 * creates and injects a table with all survey data
 */
function buildSurveysTable(divId) {
    spinnerStart();
    let xhttp = new XMLHttpRequest();
    let sendData = new FormData();
    let table;
    sendData.append("title",document.getElementsByTagName("title")[0].text);
    xhttp.onload = function(){
        let data = JSON.parse(this.response);
        let key = Object.keys(data)[0];
        keyDiv = document.createElement("div");
        keyDiv.id = key + "_div";
        table = document.createElement("table");
        table.id = key + TABLE_ID;
        keyDiv.appendChild(table);
        div = document.getElementById(divId);
        div.appendChild(keyDiv);
        fillTable(table, data[key]);
        addDeleteEdit(table, deleteSurvey, editSurvey);
        addSearchBar(table);
        addViewMore(table, viewMoreSurvey);
        correctHeader(table);
        table.addEventListener('click',function(e){
            if(e.target.nodeName.toUpperCase() !== "TH") return;
                header_click(e.target);
        });
        spinnerStop();
    };
    xhttp.open("POST", "../Controller/FillManagerTableData.php");
    xhttp.send(sendData);
}
//#region Element Creation

/**
 * 
 * @param {HTMLDivElement} parentDiv - the div that will contain the button
 * returns a button that removes all viewMore_tables (all the tables that opened with view more click)
 * and then removes itself
 */
function getCloseViewMoreButton(parentDiv) {
    if (parentDiv.querySelector(".close_viewMore"))
        return null;
    let button = document.createElement("button");
    button.setAttribute("type", "button");
    button.className = "close_viewMore";
    button.textContent = "סגור תצוגה מורחבת";
    button.addEventListener("click",function(){
        while(parentDiv.querySelector(".viewMore_table"))
            parentDiv.removeChild(parentDiv.querySelector(".viewMore_table"));
        parentDiv.removeChild(this);
    });
    return button;
}

/**
 * 
 * @param {string} defaultNameText - default value to appear in the name input field
 * returns an array of 2 elements - a label and an input 
 */
function getSurveyLabelAndNameInput(defaultNameText = "") {
    let label = document.createElement("label");
    label.setAttribute("for", "new_survay_name");
    label.appendChild(document.createTextNode("שם הסקר: "));

    let survayNameInput = document.createElement("input");
    survayNameInput.setAttribute("type", "text");
    survayNameInput.required = true;
    survayNameInput.id = "new_survay_name";
    survayNameInput.setAttribute("maxLength",MAX_NAME_LEN);
    survayNameInput.pattern='^.*\\S+.*$';
    survayNameInput.addEventListener('invalid',nameValidation);
    survayNameInput.defaultValue = defaultNameText;

    return [label, survayNameInput];
}
/**
 * 
 * @param {HTMLTableElement} table - table with rows that jave checkboxes in cells indexed [0]
 * returnes a button that has an onclick function that removes the selected rows from the table
 */
function getButtonToRemovedCheckRows(table) {
    let buttonRemoveChecked = document.createElement("button");
    buttonRemoveChecked.setAttribute("type", "button");
    buttonRemoveChecked.id = "removeCheckedRows";
    buttonRemoveChecked.textContent = "הסר שורות נבחרות";
    buttonRemoveChecked.addEventListener("click",function(){
        removedCheckedLinesFromTable(table);
    });
    return buttonRemoveChecked;
}
/**
 * 
 * @param {string} surveyName - the name of the survey
 * @param {HTMLTableElement} questionnaireTable - table of questionnaires to add to the survey
 * @param {number} surveyId - id of the survey, optinal for new surveys
 */
function getButtonToSubmitSurvey(surveyNameInputElement, questionnaireTable, surveyId) {
    let buttonSubmit = document.createElement("button");
    //buttonSubmit.setAttribute("type","submit");
    buttonSubmit.id = "submitSurveyForm";
    buttonSubmit.textContent = "שמור סקר";
    buttonSubmit.addEventListener("click",function(){
        submitSurvey(surveyNameInputElement,questionnaireTable,surveyId)
    });
    return buttonSubmit;
}
/**
 * 
 * @param {HTMLElement} containerId - a container element that will hold the form 
 * @param {string} defaultNameText  - default value to appear in the name input field
 * @param {*} surveyQuesyionnairesTable - array with questionnaire information
 * appends a headline and a form to submit a new survey to the DB
 */
function fillSurveyCreationForm(containerId, defaultNameText = "", surveyQuesyionnairesDataArr = [["מס'", "שם", "קטגוריה"]]) {
    let container = document.getElementById(containerId);
    let h3 = document.createElement("h3");
    h3.innerHTML = "צור סקר חדש";
    container.appendChild(h3);

    let form = document.createElement("form");
    form.setAttribute("action", "#");
    form.className = "popup_form";

    let labelAndInput = getSurveyLabelAndNameInput(defaultNameText);
    form.append(labelAndInput[0]);
    form.append(labelAndInput[1]);

    let surveyQuesyionnairesTable = document.createElement("table");
    surveyQuesyionnairesTable.id = container.id + TABLE_ID;
    fillTable(surveyQuesyionnairesTable, surveyQuesyionnairesDataArr);
    addCheckBox(surveyQuesyionnairesTable);
    addViewMore(surveyQuesyionnairesTable, viewMoreQuestionnaire);
    correctHeader(surveyQuesyionnairesTable);
    form.append(surveyQuesyionnairesTable);

    form.append(getButtonToRemovedCheckRows(surveyQuesyionnairesTable));
    form.append(getButtonToSubmitSurvey(labelAndInput[1], surveyQuesyionnairesTable, NEW_SURVEY_ID));

    container.append(form);
}

/**
 * 
 * @param {HTMLElement} ContainerId - container to hide 
 * @param {HTMLButtonElement} openButtonId - button the shows the hidden container
 * returns a button that hides a container and shows the button that shows that container
 */
function getCloseFormButton(containerId, openButtonId = null) {
    let button = document.createElement("button");
    button.setAttribute("type", "button");
    button.className = "close_form";
    button.textContent = "סגור";
    button.onclick = function () {
        closeForm(openButtonId, containerId);
        this.remove();
    };
    return button;
}
/**
 * 
 * @param {HTMLTableElemnt} destTable 
 * @param {HTMLTableElemnt} sourceTable
 * creates a button that copies all checked rows from source table to destination table 
 */
function getButtonAddCheckRows(destTable, sourceTable) {
    if (destTable == null || sourceTable == null)
        return;
    let button = document.createElement("button");
    button.setAttribute("type", "button");
    button.id = "addCheckedRows";
    button.textContent = "הוסף שורות נבחרות";
    button.addEventListener('click', function () {
        addCheckedRowsToTable(destTable, sourceTable);
    });
    return button;
}

//#endregion

//#region Utility functions
/**
 * 
 * @param {HTMLTableCellElement} tableCell - delete cell that was clicked
 * tries to delete a survey from the db.
 * if the survay is a part of an active survey that isn't done the delete will fail 
 */
function deleteSurvey(tableCell) {
    if (tableCell === null)
        return;
    if (!confirm("למחוק את הסקר?"))
        return;
    spinnerStart();
    let xhttp = new XMLHttpRequest();
    let sendData = new FormData();
    sendData.append("action", "delete");
    sendData.append("dataType", "survey");
    sendData.append("surveyId", getParentTable(tableCell).rows[tableCell.parentElement.rowIndex].cells[0].innerHTML);
    xhttp.onload = function () {
        if (JSON.parse(this.response) == -1)
            alert("לא ניתן למחוק סקר פעיל");
        else if (JSON.parse(this.response) == 0) alert("משהו השתבש במחיקת סקר...");
        spinnerStop();
        document.location.reload(true);
    };
    xhttp.open("POST", "../Controller/UserIOHandler.php");
    xhttp.send(sendData);
}
/**
 * Function removes all childs of element
 * @param {object} element - DOM element to be cleaned
 * 
 */
function cleanElement(element) {
    while (element.firstChild) {
        element.removeChild(element.firstChild);
    }
}
/**
 * 
 * @param {HTMLTableElement} table - table to be filled 
 * @param {string/number} surveyId - the survey id
 * fills a table with questionnaires that are linked to a survey
 */
function fillSurveyQuestionnairesTable(table, surveyId) {
    spinnerStart();
    let xhttp = new XMLHttpRequest();
    let sendData = new FormData();
    sendData.append("managerSurveyAction", "getSurveyQuestionnaires");
    sendData.append("SurveyId", surveyId);
    xhttp.onload = function () {
        fillTable(table, JSON.parse(this.response));
        if (!table.className.includes("noChecked"))
            addCheckBox(table);
        addViewMore(table, viewMoreQuestionnaire);
        correctHeader(table);
        spinnerStop();
    };
    xhttp.open("POST", "../Controller/ManagerSurveyHandler.php")
    xhttp.send(sendData);
}
/**
 * 
 * @param {HTMLTableElement} table - table to be filled 
 * @param {string/number} questionnaireId - the questionnaire id
 * fills a table with questionnaires that are linked to a survey
 */
function fillQuestionnaireQuestionsTable(table, questionnaireId) {
    spinnerStart();
    let xhttp = new XMLHttpRequest();
    let sendData = new FormData();
    sendData.append("QuestionnaireAction", "getQuestions");
    sendData.append("QuestionnaireID", questionnaireId);
    xhttp.onload = function () {
        fillTable(table, JSON.parse(this.response));
        spinnerStop();
    };
    xhttp.open("POST", "../Controller/QuestionnairesHandler.php")
    xhttp.send(sendData);
}
/**
 * 
 * @param {HTMLTableElement} table - table to search in
 * @param {string} str - string to search
 * @param {*} columnIndex - column index to search the string in
 * returns null if table or column index are invalid
 * returns true if str is in one of rows of the table in the selected column
 * else returns false
 */
function checkIfStringExist(table, str, columnIndex) {
    if (table == null || columnIndex > table.rows[0].cells.length)
        return null;
    let rowCount = table.rows.length;
    for (let i = 0; i < rowCount; i++) {
        if (table.rows[i].cells[columnIndex].innerHTML == str)
            return true;
    }
    return false;
}
/**
 * 
 * @param {HTMLTableElemnt} destTable 
 * @param {HTMLTableElemnt} sourceTable
 * copies all checked rows from source table to destination table 
 */
function addCheckedRowsToTable(destTable, sourceTable) {
    let rowCount = sourceTable.rows.length;
    let sameRowFlag = false;
    for (let i = 1; i < rowCount; i++) {
        if (sourceTable.rows[i].cells[0].children[0].checked)
            if (!checkIfStringExist(destTable, sourceTable.rows[i].cells[1].innerHTML, 1)) {
                let tmpRow = destTable.insertRow();
                deepCopyTableRow(tmpRow, sourceTable.rows[i]);
            }
            else sameRowFlag = true;
    }
    if (sameRowFlag) showErrMsg("לא ניתן להוסיף שאלונים שכבר קיימים בסקר. רק שאלונים חדשים נוספו");
}
/**
 * 
 * @param {HTMLTableElemant} table
 * removes checked line from a table 
 */
function removedCheckedLinesFromTable(table) {
    if (table == null)
        return;
    for (i = 1; i < table.rows.length; i++) {
        if (table.rows[i].cells[0].children[0].checked) {
            table.deleteRow(i);
            i--;//when a row is deleted, all the indexes of the rows are decremented by 1
        }
    }
}

/**
 * 
 * @param {string} surveyName - the name of the survey
 * @param {HTMLTableElement} questionnaireTable - table of questionnaires to add to the survey
 * @param {number} surveyId - id of the survey, optinal for new surveys
 */
function submitSurvey(surveyNameInputElement,questionnaireTable,surveyId)
{
    if(surveyNameInputElement.reportValidity()){
        if(questionnaireTable == null || questionnaireTable.rows.length <= 1)
        {
            showErrMsg("צריך להיות לפחות שאלון אחד בסקר");
            return;
        }
        let surveyName = surveyNameInputElement.value;
    spinnerStart();
    let xhttp = new XMLHttpRequest();
    let sendData = new FormData();
    sendData.append("action", surveyId == NEW_SURVEY_ID ? "insert" : "edit");
    sendData.append("dataType", "survey");
    let questionnaireIds = new Array();
    for (let i = 1; i < questionnaireTable.rows.length; i++)
        questionnaireIds[i - 1] = questionnaireTable.rows[i].cells[1].innerHTML;
    surveyJsonObj = { name: surveyName, id: surveyId, questionnaires: questionnaireIds };
    sendData.append("surveyData", JSON.stringify(surveyJsonObj));
    xhttp.onload = function () {
        if (!JSON.parse(this.response))
            alert("משהו השתבש בשמירת סקר...");
        spinnerStop();
        document.location.reload(true);
    }
    xhttp.open("POST", "../Controller/UserIOHandler.php");
    xhttp.send(sendData);
}
}

//#endregion

//#region New Survey
function newSurveyPopupOpen(divId, openButtonId) {
    document.getElementById(openButtonId).style.display = "none";

    let popupDivHeader = document.getElementById(divId).getElementsByTagName("div")[0];
    let popupDivArticle = document.getElementById(divId).getElementsByTagName("div")[1];
    cleanElement(popupDivHeader);
    cleanElement(popupDivArticle);
    //this part is used to delete duplicate close buttons
    if (document.querySelector(".close_form"))
        document.querySelector(".close_form").parentNode.removeChild(document.querySelector(".close_form"));

    fillSurveyCreationForm(popupDivHeader.id);
    let topTable = popupDivHeader.getElementsByTagName("table")[0];

    let bottomTable = buildAllQuestionnairesTable(popupDivArticle.id);

    popupDivArticle.append(getButtonAddCheckRows(topTable, bottomTable));

    document.getElementById(divId).append(getCloseFormButton(divId, openButtonId));

    document.getElementById(divId).style.display = "block";

}

function buildAllQuestionnairesTable(bottomDiv) {
    spinnerStart();
    let xhttp = new XMLHttpRequest();
    let sendData = new FormData();
    let table = document.createElement("table");
    document.getElementById(bottomDiv).appendChild(table);
    sendData.append("managerSurveyAction", "getAllQuestionnaires");
    xhttp.onload = function () {
        let data = JSON.parse(this.response);
        let key = Object.keys(data)[0];
        table.id = bottomDiv + TABLE_ID;
        table.style.border = 1;
        fillTable(table, data[key]);

        addViewMore(table, viewMoreQuestionnaire);
        addCheckBox(table);
        correctHeader(table);
        addSearchBar(table);

        let h4 = document.createElement("h4");
        h4.textContent = "רשימת שאלונים";
        document.getElementById(bottomDiv).prepend(h4);

        table.addEventListener('click', function (e) {
            if (e.target.nodeName.toUpperCase() !== "TH") return;
            header_click(e.target);
        });
        spinnerStop();
    };
    xhttp.open("POST", "../Controller/ManagerSurveyHandler.php");
    xhttp.send(sendData);
    return table;
}
//#endregion

//#region Edit Survey

/**
 * 
 * @param {HTMLTableCellElement} tableCell
 * opens a form in "surveyForm" div to edit the selected survey
 */
function editSurvey(tableCell) {
    let divId = "surveyForm";
    let popupDivHeader = document.getElementById(divId).getElementsByTagName("div")[0];
    let popupDivArticle = document.getElementById(divId).getElementsByTagName("div")[1];
    cleanElement(popupDivHeader);
    cleanElement(popupDivArticle);
    //this part is used to delete duplicate close buttons
    if (document.querySelector(".close_form"))
        document.querySelector(".close_form").parentNode.removeChild(document.querySelector(".close_form"));
    //this part is used to display the hidden "create new survey" button on case of an edit click
    document.querySelector(".open_form").style.display = "";

    fillSurveyEditForm(popupDivHeader.id, tableCell.parentElement.cells[1].innerHTML, tableCell.parentElement.cells[0].innerHTML);
    let topTable = popupDivHeader.getElementsByTagName("table")[0];

    let bottomTable = buildAllQuestionnairesTable(popupDivArticle.id);

    popupDivArticle.append(getButtonAddCheckRows(topTable, bottomTable));

    document.getElementById(divId).append(getCloseFormButton(divId));

    document.getElementById(divId).style.display = "block";

}

/**
 * 
 * @param {HTMLElement} containerId - a container element that will hold the form 
 * @param @param {string} defaultNameText  - default value to appear in the name input field - the survey current name 
 * @param {string/number} surveyId - the survey id that is being edited
 * appends a headline and a form to submit an edited survey to the DB
 */
function fillSurveyEditForm(containerId, defaultNameText, surveyId) {
    let container = document.getElementById(containerId);
    let h3 = document.createElement("h3");
    h3.innerHTML = "עריכת סקר";
    container.appendChild(h3);

    let form = document.createElement("form");
    form.setAttribute("action", "#");
    form.className = "popup_form_form";

    let labelAndInput = getSurveyLabelAndNameInput(defaultNameText);
    form.append(labelAndInput[0]);
    form.append(labelAndInput[1]);

    let surveyQuesyionnairesTable = document.createElement("table");
    surveyQuesyionnairesTable.id = container.id + TABLE_ID;
    fillSurveyQuestionnairesTable(surveyQuesyionnairesTable, surveyId);
    form.append(surveyQuesyionnairesTable);

    form.append(getButtonToRemovedCheckRows(surveyQuesyionnairesTable));
    form.append(getButtonToSubmitSurvey(labelAndInput[1], surveyQuesyionnairesTable, surveyId));

    container.append(form);
}

//#endregion

//#region viewMore Functions
/**
 * 
 * @param {HTMLTableCellElement} tableCell 
 * builds and appends a table with the survey questionnaires
 * removes any viewMore_tables that exist in the same div
 */
function viewMoreSurvey(tableCell) {
    let surveysTable = getParentTable(tableCell);
    let parentDiv = surveysTable.parentNode;
    let viewMoreTable;
    //cleaning existing viewMore tables
    while (viewMoreTable = parentDiv.querySelector(".viewMore_table"))
        parentDiv.removeChild(viewMoreTable);
    let questionnairesTable = document.createElement("table");
    questionnairesTable.classList.add("viewMore_table", "questionnaires_table", "noChecked");
    questionnairesTable.id = "surveyViewMore_table";
    fillSurveyQuestionnairesTable(questionnairesTable, tableCell.parentElement.cells[0].innerHTML);
    addViewMore(questionnairesTable, viewMoreQuestionnaire);
    parentDiv.insertBefore(questionnairesTable, surveysTable.nextSibling);
    let button = getCloseViewMoreButton(parentDiv);
    if (button)
        parentDiv.insertBefore(button, questionnairesTable.nextSibling);
}
/**
 * 
 * @param {HTMLTableCellElement} tableCell 
 * builds and appends a table with the questionnaire questions
 * removes any existing viewMore questions table in the div
 */
function viewMoreQuestionnaire(tableCell) {
    let questionnairesTable = getParentTable(tableCell);
    let parentDiv = questionnairesTable.parentNode;
    let viewMoreTable;
    //searching for any viewMore questions table in the div and removing them
    while (viewMoreTable = parentDiv.querySelector(".viewMore_table.questions_table"))
        parentDiv.removeChild(viewMoreTable);
    let questionsTable = document.createElement("table");
    //tables get double classes to help idintify aquestionnaires table from questions table
    questionsTable.classList.add("viewMore_table", "questions_table", "noChecked");
    questionsTable.id = "questionnaireViewMore_table";
    //if the table has no checkbox the id column is in a diffrent index
    let questionnaireId = questionnairesTable.className.includes("noChecked") ? tableCell.parentElement.cells[0].innerHTML : tableCell.parentElement.cells[1].innerHTML
    fillQuestionnaireQuestionsTable(questionsTable, questionnaireId);
    addViewMore(questionsTable, viewMoreQuestionnaire);
    questionnairesTable.parentNode.insertBefore(questionsTable, questionnairesTable.nextSibling);
    let button = getCloseViewMoreButton(parentDiv);
    if (button)
        parentDiv.insertBefore(button, questionsTable.nextSibling);
}

//#endregion
