function codeCheck()
{
    if(document.getElementById('code').reportValidity()) {
        if(document.getElementById('password') == null)
            codeSubmit();
        else if(document.getElementById('password').reportValidity())
            codeSubmit();
    }

}

function codeValidation(event) {
    var msg='';
    if(event.target.validity.valueMissing){
        msg="נא להכניס קוד";
    }
    else if(event.target.validity.tooLong){
        msg="הקוד שהוכנס ארוך מדי";
    }
    else if(event.target.validity.patternMismatch) {
        msg="הקוד צריך להכיל אותיות וספרות בלבד";
    }
    event.target.setCustomValidity(msg);
}

function codeSubmit() {
    //Data from input
    var data = new FormData();
    var code=document.getElementById('code').value;
    data.append('code',code);
    if(document.getElementById('password') !== null)
        data.append('password',document.getElementById('password').value);
    else
        data.append('password',"");

    //AJAX request
    var xhttp = new XMLHttpRequest();
    spinnerStart();
    xhttp.onload = function(){
        var res = JSON.parse(this.response);
        if (res.redirect != "") {
            if(res.manager==false) {
                if(!checkCookie(code)) {
                    window.location.href = res.redirect;
                }
                else document.getElementById('message').innerHTML = "<h3>לא ניתן להתחבר שנית</h3><br>";
            }
            else window.location.href = res.redirect;
        }
        if(res.manager == true)
            document.getElementById('managerSpace').innerHTML = 
            "<input type='password' id='password' placeholder='סיסמא...' pattern='[A-Za-z0-9]+' maxLength=50 required oninvalid='codeValidation(event)'>";
        if(res.redirect == "" && res.manager == false)
            document.getElementById('message').innerHTML ="<h3>הקוד שהוכנס שגוי</h3><br>";
        spinnerStop();
    };
    xhttp.open('POST','../Controller/login_handler.php',true);
    xhttp.send(data);
}

function setCookie(cName, cVal, cExp) {
    var expDate=new Date(cExp).toUTCString();
    document.cookie=cName+"="+cVal+";"+"expires="+expDate;
}

function checkCookie(cName) {
    var decodedCookie = decodeURIComponent(document.cookie);
    var cookiesArr = decodedCookie.split(';');
    for(var i = 0; i <cookiesArr.length; i++) {
      var c = cookiesArr[i];
      if (c.indexOf(cName) !== -1) {
        return true;
      }
    }
    return false;
}

function cleanMsg(){
    document.getElementById('message').innerHTML = "";
}