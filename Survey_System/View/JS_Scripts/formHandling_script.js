/**
 * 
 * @param {Container Element} containerId - the continer id of the form (like a div element)
 * displays a continer element
 */
function openForm(openButtonId, containerId) {
    if (openButtonId)
        document.getElementById(openButtonId).style.display = "none";
    document.getElementById(containerId).style.display = "block";
}
/**
 * 
 * @param {Container Element} containerId - the continer id of the form (like a div element)
 * hides a continer element
 */
function closeForm(openButtonId, containerId) {
    if (openButtonId)
        document.getElementById(openButtonId).style.display = "";
    document.getElementById(containerId).style.display = "none";
}
/**
 * 
 * @param {Input Element} inputElementId - the input element used
 * creates a new question in the DB via user input.
 * shows a message if the opartion succeeded or failed, and refreshes the page 
 */
function newQuestionSubmit(inputElementId) {
    spinnerStart();
    let xhttp = new XMLHttpRequest();
    let sendData = new FormData();
    sendData.append("action", "insert");
    sendData.append("dataType", "question");
    sendData.append("questionText", document.getElementById(inputElementId).value);
    xhttp.onload = function () {
        if (!JSON.parse(this.response))
            alert("משהו השתבש בשמירת שאלה...");
        spinnerStop();
        document.location.reload(true);
    };
    xhttp.open("POST", "../Controller/UserIOHandler.php");
    xhttp.send(sendData);
}