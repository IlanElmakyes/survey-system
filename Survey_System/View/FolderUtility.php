<?php
namespace View;
class FolderUtility
{
	public static function getHomePath($fileDir)
	{
		$foldersArray = explode(DIRECTORY_SEPARATOR,$fileDir);
		$home_path= "";
		for($i=0;$i<sizeof($foldersArray) && strcmp($foldersArray[$i],"Survey_System") != 0;$i++)
			$home_path .= $foldersArray[$i].DIRECTORY_SEPARATOR;
		$home_path .= "Survey_System";
		return $home_path;
	}
}
?>