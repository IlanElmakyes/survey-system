<?php
namespace View;
class Navigation
{
    public static function generateNav()
    {
        return "<div class='nav'>
            <a href='managerIndex.php'>סטטוס והפעלת סקרים</a>
            <a href='managerQuestion.php'>שאלות</a>
            <a href='managerQuestionnaire.php'>שאלונים</a>
            <a href='managerSurvey.php'>סקרים</a>
            <a href='managerFacultiesAndClasses.php'>מגמות וכיתות</a>
            <a href='managerTeachers.php'>מרצים</a>
            <a href='managerExcel.php'>העלת קובץ Excel</a>
            <a href='managerBackup.php'>שחזור וגיבוי</a>
            <a href='managerSettings.php'>הגדרות</a>
        </div>";
    }
}
?>
