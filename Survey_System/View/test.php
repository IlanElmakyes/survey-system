<?php
namespace View;
use Model\Classes\Data_Objects as objects;
use Model\Classes\DB_Connections as dbConnect;

require_once "FolderUtility.php";
require_once(FolderUtility::getHomePath(__DIR__).DIRECTORY_SEPARATOR."autoload.php");
session_start();
$_SESSION['HOME_PATH'] = FolderUtility::getHomePath(__DIR__);
$_SESSION['AUTOLOAD_PATH'] = FolderUtility::getHomePath(__DIR__).DIRECTORY_SEPARATOR."autoload.php";
$managerDb=new dbConnect\ManagerDB();
$manager=new objects\Manager('admin','admin');
$managerDb->insert($manager);
?>
<html dir='rtl'>
    <head>
        <script src="JS_Scripts\index_script.js"></script>
        <link rel="stylesheet" href="CSS\login_page.css">
        <meta name='viewport' content='width=device-width, initial-scale=1'>
    </head>

    <body>

    </body>
</html>