<?php
namespace View;
session_start();
if(!isset($_SESSION['MANAGER']))
    header("Location: index.php");
//the user should be redirected here from a specific action, if the user came from a diffrent place, send it back to manager index
//if the user refreshed the page, the report session should have expired, and the user will be redirected to the page he came from
//**Report session expires after body.onload function makes a call to the server */
if(!isset($_SESSION['REPORT']))
{
    $prev = isset($_SESSION['PREV_PAGE'])? $_SESSION['PREV_PAGE']:"../View/managerIndex.php";
    unset($_SESSION['PREV_PAGE']);
    unset($_SESSION['REPORT']);
    header("Location: ".$prev);
}
require $_SESSION['AUTOLOAD_PATH'];
?>
<html dir='rtl'>
    <meta charset='UTF-8'>
    <head>
        <title>Manager-Reports</title>
        <link rel="stylesheet" href="CSS\clickable_elements.css">
        <link rel="stylesheet" href="CSS\manager_pages.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="CSS\spinner.css"> 
        <script src="JS_Scripts\tableHandling_script.js"></script>
        <script src="JS_Scripts\reports_script.js"></script>
        <script src="JS_Scripts\spinnerActions.js"></script>
    </head>
    <body onload="initializeReportPage()">
        <header>
            <div class="spinner"></div>
            <h2 class="header_inline">דו"חות</h2>
            <hr class="header_block">
        </header>
        <?php echo (Navigation::generateNav());?>
        <main>
        <div class = "main_block" id="reports">
            <button class="spoiler" id="reports_spoiler" onclick="spoilerAction('report_div')" style="display:none;">הצג\הסתר דוחות</button>
            <a href="#" id="report_download" style="display:none;">
                <span class="material-icons download" title="download CSV file" style="font-size:26px;" onclick="getCSVFile('<?php echo $_SESSION['reportData']['type'] ?>')">save_alt</span>
            </a>
            <h2 id="report_date" style="display:none;"></h2>
            <div class="report_container" id="report_div"></div>
        </div>
        </main>
        <footer></footer>
    </body>
</html>