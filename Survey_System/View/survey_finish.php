<html dir='rtl'>
        <head>
            <link rel='stylesheet' href="CSS\survey_style.css">
            <meta name='viewport' content='width=device-width, initial-scale=1'>
        </head>
    <header></header>
    <body>
        <main>
        <?php
        session_start();

        if(isset($_SESSION['SURVEY_CODE']))
        {
            echo("<p id='survey_message'>התשובות שלך נשלחו, תודה על שיתוף פעולה!</p>");
            session_destroy();
        }
        else header('Location: index.php');        
        ?>
        </main>        
    </body>
    <footer></footer>
</html>