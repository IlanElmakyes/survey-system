<?php
namespace View;
session_start();
if(!isset($_SESSION['MANAGER']))
    header("Location: index.php");
require $_SESSION['AUTOLOAD_PATH'];
?>

<html dir='rtl'>
    <meta charset='UTF-8'>
    <head>
        <title>Questionnaires</title>
        <link rel="stylesheet" href="CSS\clickable_elements.css">
        <link rel="stylesheet" href="CSS\manager_pages.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="CSS\spinner.css">
        <script src="JS_Scripts\tableHandling_script.js"></script>
        <script src="JS_Scripts\questionnaires_script.js"></script>
        <script src="JS_Scripts\spinnerActions.js"></script>
    </head>
    <body onload="pageInitialize()">
        <header>
            <div class="spinner"></div>
            <h2 class="header_inline">שאלונים</h2>
            <hr class="header_block">
            <div id="errmsg_div" style='display:none;'>
            </div>     
        </header>
        <?php echo (Navigation::generateNav());?>

        <main>       
            <div class = "main_block">
                <button id="new_questionnaire_btn">צור חדש</button>
                <div id="questionnaires_div">                
                    <table id="questionnaires_table">
                    </table>
                </div>
            </div>
            <div class = "main_block" id="questions_div">
            </div>
            <div class = "main_block" id="allquestions_div">
            </div>
        </main>
        <footer></footer>
    </body>
</html>