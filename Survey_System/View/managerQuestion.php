<?php
namespace View;
session_start();
if(!isset($_SESSION['MANAGER']))
    header("Location: index.php");
require $_SESSION['AUTOLOAD_PATH'];
?>
<html dir='rtl'>
    <meta charset='UTF-8'>
    <head>
        <title>Manager-Question</title>
        <link rel="stylesheet" href="CSS\clickable_elements.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="CSS\manager_pages.css">
        <link rel="stylesheet" href="CSS\spinner.css">
    </head>
    <body onload="buildQuestionsTable('questions_div','true',questionDelete,questionEdit)">
        <header>
        <div class="spinner"></div>
        <h2 class="header_inline">שאלות</h2>
        <hr class="header_block">
        <div id="errmsg_div" style="display : none;">
        </div>
        </header>
        <?php echo (Navigation::generateNav());?>
        <main>
        <div class = "main_block">
        <div id="new_question_form">
            <form action="#" onsubmit="newQuestionSubmit('question_text_input');return false">
                <label for="question_text_input"><b>שאלה חדשה:</b></label>
                <textarea placeholder="הכנס שאלה" id="question_text_input" required oninvalid='textValidation(event)' maxLength=120></textarea>
                <button type="submit" class="popup_form_button">הוסף</button>
            </form>
           </div> 
        
            <div id="questions_div">
            </div>
        </div>  

           </main>
        <footer></footer>
        <script src="JS_Scripts\tableHandling_script.js"></script>
        <script src="JS_Scripts\formHandling_script.js"></script>
        <script src="JS_Scripts\spinnerActions.js"></script>
    </body>
</html>