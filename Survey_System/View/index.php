<?php
namespace View;
use Model\Classes\Data_Objects as objects;
use Model\Classes\DB_Connections as dbConnect;

require_once "FolderUtility.php";
require_once(FolderUtility::getHomePath(__DIR__).DIRECTORY_SEPARATOR."autoload.php");
session_start();
$_SESSION['HOME_PATH'] = FolderUtility::getHomePath(__DIR__);
$_SESSION['AUTOLOAD_PATH'] = FolderUtility::getHomePath(__DIR__).DIRECTORY_SEPARATOR."autoload.php";
$FIRST_LOAD = true;
?>
<html dir='rtl'>
    <head>
        <script src="JS_Scripts\index_script.js"></script>
        <script src="JS_Scripts\spinnerActions.js"></script>
        <link rel="stylesheet" href="CSS\spinner.css">
        <link rel="stylesheet" href="CSS\login_page.css">
        <meta name='viewport' content='width=device-width, initial-scale=1'>
    </head>

    <body>
        <header>
        </header>
        <article>
            <div id="login_form">
            <div class="spinner"></div>
            <h3>מערכת סקרים</h3>
               
            <?php if($FIRST_LOAD)
            {
                $FIRST_LOAD = false;
                echo "
                    <input type='text' id='code' placeholder='קוד כניסה...' pattern='[A-Za-z0-9]+' maxLength=50 required oninvalid='codeValidation(event)' oninput='cleanMsg()'>
                    <div id='managerSpace'></div>
                    <br><button onClick='codeCheck()'>התחבר</button>";
            } ?>
             <div id="message"></div>
            </div>
        </article>
        <footer><footer>
    </body>
</html>