<?php
namespace View;
session_start();
if(!isset($_SESSION['MANAGER']))
    header("Location: index.php");
require $_SESSION['AUTOLOAD_PATH'];
?>

<html dir='rtl'>
    <meta charset='UTF-8'>
    <head>
        <title>Faculties and Classes</title>
        <link rel="stylesheet" href="CSS\clickable_elements.css">
        <link rel="stylesheet" href="CSS\manager_pages.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
		<link rel="stylesheet" href="CSS\spinner.css">											  
        <script src="JS_Scripts\tableHandling_script.js"></script>
        <script src="JS_Scripts\faculties_script.js"></script>
        <script src="JS_Scripts\reports_script.js"></script>
		<script src="JS_Scripts\spinnerActions.js"></script>													
    </head>
    <body onload="pageInitialize()">
        <header>
            <div class="spinner"></div>
            <h2 class="header_inline">מגמות וכיתות</h2>
            <hr class="header_block">
			<div id="errmsg_div" style='display:none;'>
        </header>
        <?php echo (Navigation::generateNav());?>
        <main>        
            <div class = "main_block" id="faculties_all_div">
                <h3>מגמות</h3>
                <div id="new_faculty_div">
                    <label>הוסף מגמה חדשה</label>
                    <input type="text" id="new_faculty_input" required pattern='^.*\S+.*$' maxLength=50 oninvalid='nameValidation(event)'>
                    <button id="add_faculty_btn">הוסף</button>
                </div>
                <button id="open_report_btn" onclick="createReports(null,'faculties_table')">צור דו"ח מקוצר לרשומות נבחרות</button>
                <button id="open_report_btn" onclick="fullReportLoad(FACULTY,'faculties_table')">צור דו"ח מלא לרשומות נבחרות</button>
                <div id="faculties_div">
                </div>
            </div>
            <div class = "main_block" id="view_classes" style="display : none;">
                <h3 id="faculty_header"></h3>
                <h3>כיתות</h3>
                <div id="viewclasses_div">
                </div>
                <div id="view_buttons_holder">
                    <button id="delete_faculty">מחק מגמה</button>
                    <button id="edit_classes_btn">ערוך</button>
                    <button id="close_view_classes">סגור</button>
                </div>
            </div>
            <div class = "main_block" id="edit_classes" style="display : none;">
            <h3>עדכון מגמה וכיתות</h3>
                <label>מגמת</label>
                <input type="text" id="faculty_name_input" required pattern='^.*\S+.*$' maxLength=50 oninvalid='nameValidation(event)'>
                <div id="edit_buttons_holder">
                    <button id="save_btn">שמור</button>
                    <button id="cancel">סגור</button>
                </div>
                <h3>כיתות</h3>
                <div id="editclasses_div">
                </div>
                <div id="new_class_div">
                    <label>הוסף כיתה חדשה</label>
                    <input type="text" id="new_class_input" required pattern='^.*\S+.*$' maxLength=50 oninvalid='nameValidation(event)'>
                    <button id="add_class_btn">הוסף</button>
                </div>
            </div>
            <div class = "main_block" id="view_teachers" style="display : none;">
                <h3 id="class_header"><h3>
                <h3>מרצים</h3>
                <div id="teachers_div">
                </div>
                <button id="close_teachers_btn">סגור</button>
            </div>
            <div class = "main_block" id="reports">
                <button class="spoiler" id="reports_spoiler" onclick="spoilerAction('report_div')" style="display:none;">הצג\הסתר דוחות</button>
                <a href="#" id="report_download" style="display:none;">
                    <span class="material-icons download" title="download CSV file" style="font-size:26px;" onclick="getCSVFile('class')">save_alt</span>
                </a>
            </div>
        </main>
        <footer></footer>
    </body>
</html>