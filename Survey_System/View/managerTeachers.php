<?php
namespace View;
session_start();
if(!isset($_SESSION['MANAGER']))
    header("Location: index.php");
require $_SESSION['AUTOLOAD_PATH'];
?>

<html dir='rtl'>
    <meta charset='UTF-8'>
    <head>
        <title>Teachers</title>
        <link rel="stylesheet" href="CSS\clickable_elements.css">
        <link rel="stylesheet" href="CSS\manager_pages.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="CSS\spinner.css">
        <script src="JS_Scripts\tableHandling_script.js"></script>
        <script src="JS_Scripts\teachers_script.js"></script>
        <script src="JS_Scripts\reports_script.js"></script>
        <script src="JS_Scripts\spinnerActions.js"></script>
    </head>
    <body onload="pageInitialize()">
        <header>
            <div class="spinner"></div>
            <h2 class="header_inline">מרצים</h2>
            <hr class="header_block">
        <div id="errmsg_div" style='display:none;'></div>
        </header>
        <?php echo (Navigation::generateNav());?>
        <main>        
            <div class = "main_block" id="teachers_all_div">
                <div id="new_teacher_div">
                    <label>הוספת מרצה חדש\ה</label>
                    <input type="text" id="new_teacher_input" required pattern='^.*\S+.*$' maxLength=50 oninvalid='nameValidation(event)'>
                    <button id="add_teacher_btn">הוסף</button>
                </div>
                <button id="open_report_btn" onclick="createReports('teachers_table')">צור דו"ח מקוצר לרשומות נבחרות</button>
                <button id="open_report_btn" onclick="fullReportLoad(TEACHER,'teachers_table')">צור דו"ח מלא לרשומות נבחרות</button>
                <div id="teachers_div">
                </div>
            </div>
            <div class = "main_block" id="view_classes" style="display : none;">
                <h3 id="teacher_header"></h3>
                <h3>כיתות</h3>
                <div id="viewclasses_div">
                </div>
                <div id="view_buttons_holder">
                    <button id="delete_teacher">מחק מרצה</button>
                    <button id="edit_classes_btn">ערוך</button>
                    <button id="close_view_classes">סגור</button>
                </div>
            </div>
            <div class = "main_block" id="edit_classes" style="display : none;">
            <h3>עדכון פרטים</h3>
                <label>מרצה</label>
                <input type="text" id="teacher_name_input" required pattern='^.*\S+.*$' maxLength=50 oninvalid='nameValidation(event)'>
                <div id="edit_buttons_holder">
                    <button id="save_btn">שמור</button>
                    <button id="cancel">סגור</button>
                </div>
                <h3>רשימת כיתות</h3>
                <div id="editclasses_div">
                </div>
                <button id="remove_classes_btn">הסר</button>
            </div>
            <div class = "main_block" id="all_classes" style="display : none;">
                <h3>כל הכיתות</h3>
                <div id="classes_div">
                </div>
                <button id="add_classes_btn">הוסף</button>
            </div>
            <div class = "main_block" id="reports">
                <button class="spoiler" id="reports_spoiler" onclick="spoilerAction('report_div')" style="display:none;">הצג\הסתר דוחות</button>
                <a href="#" id="report_download" style="display:none;">
                    <span class="material-icons download" title="download CSV file" style="font-size:26px;" onclick="getCSVFile('teacher')">save_alt</span>
            </div>
        </main>
        <footer></footer>
    </body>
</html>