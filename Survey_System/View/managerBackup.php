<?php
namespace View;
session_start();
use Controller\BackupRestoreHandler as backup;
use Model\Classes\DB_Connections as dbConnect;
const UPLOAD_RESTORE = "1";
const EMPTY_RESTORE = "2";
const CREATE_BACKUP = "3";
const FRESH_START = "4";
const SUCCESS = 1;
const FAILURE = false;
if(!isset($_SESSION['MANAGER']))
    header("Location: index.php");
require_once $_SESSION['AUTOLOAD_PATH'];

if(isset($_POST['submit']))
{
    $ret = true;
    switch($_POST['submit'])
    {
        //the two restore cases check if the user wanted to backup the database, and in case that the back up fails
        //the operation stops and shows the correct message (that the backup failed) 
        case UPLOAD_RESTORE:
            if($_POST['backup'])
                $ret = backup::backupDatabase()? SUCCESS:FAILURE;
            if($ret) 
                $ret = backup::restoreDatabase($_FILES["fileToUpload"]["tmp_name"])? SUCCESS:FAILURE;
            else $_POST['submit'] = CREATE_BACKUP;
            break;
        case EMPTY_RESTORE:
            $restoreFile = "";
            if($_POST['backup'])
            {
                if($restoreFile=dbConnect\dbConnection::getLocalBackupFile())
                    $ret = backup::backupDatabase()? SUCCESS:FAILURE;
                else $ret = FAILURE;
            }
            if($ret) 
                $ret = backup::restoreDatabase($restoreFile)? SUCCESS:FAILURE;
            else $_POST['submit'] = CREATE_BACKUP;
            break;
        case CREATE_BACKUP:
            $ret = backup::backupDatabase();
            break;
        case FRESH_START:
            $ret = backup::backupDatabase();
            if($ret == FAILURE)
                break;
            backup::restoreDatabase(backup::FRESH_DB);
            break;
    }
}
?>
<html dir='rtl'>
    <meta charset='UTF-8'>
    <head>
        <title>Backup and Restore</title>
        <link rel="stylesheet" href="CSS\clickable_elements.css">
        <link rel="stylesheet" href="CSS\manager_pages.css">
        <link rel="stylesheet" href="CSS\spinner.css">
        <script src="JS_Scripts\spinnerActions.js"></script>
    </head>
    <body>
    <header>
        <div class="spinner"></div>
        <h2 class="header_inline">גיבוי ושחזור של מסד הנתונים</h2>
        <hr class="header_block">
    </header>
        <?php echo (Navigation::generateNav());?>
    <main>
        <div class="main_block">
        <h4>בחר קובץ לשחזור</h4>
        <form name="backupForm" id='restore_form' method="post" enctype="multipart/form-data" onsubmit="submitCheck(this.submited)">
            <input type="file" name="fileToUpload" id="fileToUpload">
            <button onclick="this.form.submited=this.value;" type="submit" name="submit" value="1">העלה קובץ שחזור</button><br>
            <label for="submit_restore"><b>שחזר מגיבוי בשרת</b></label>
            <button onclick="this.form.submited=this.value;"type="submit" name="submit" value="2">התחל שחזור</button><br><br>
            <label for="submit_backup"><b>צור קובץ גיבוי חדש</b></label>
            <button onclick="this.form.submited=this.value;" type="submit" name="submit" value="3">צור גיבוי</button><br>
            <button onclick="this.form.submited=this.value;" type="submit" name="submit" value="4">הכן מסד לשנה חדשה</button><br>
            <p>הכנה לשנה חדשה יוצרת ומורידה גיבוי של המסד הנוכחי, ומשחזרת את המסד לנקודה התחלתית</p>
            <input type="hidden" name="download" value="0">
            <input type="hidden" name="backup" value="0">
            </form>
        </div>
        <script>
        //if the user wanted to create a backup, ask if he would like to download a copy of the file
        //if any other action was chosen, do nothing
        //**always return true, so that the page will submit */
            function submitCheck(button)
            {
                if(button === "4")
                    document.backupForm.download.value="1";
                else if(button === "3") {
                    if(confirm("האם ברצונך להוריד קובץ גיבוי?"))
                        document.backupForm.download.value="1";
                }
                else {
                    if(confirm("האם ברצונך  לגבות בסיס נתונים נוכחי לפני שיחזור? \n אחרת בסיס נתונים נוכחי ימחק"))
                        document.backupForm.backup.value="1";
                }
                return true;
            }

            function downloadFile(filePath)
            {
                let a = document.createElement("a");
                a.href = filePath;
                a.download = filePath.split(/[\\\/]/).pop();
                a.style.display = "none";
                document.body.appendChild(a);
                a.click();
                document.body.removeChild(a);
            }
        </script>
        <?php
            if(isset($ret))
            {
                switch($ret)
                {
                    case FAILURE:
                       echo $_POST['submit'] != "3"? "<script>alert('There was a problem restoring the DB...');</script>" : "<script>alert('There was a problem creating a backup to the DB...');</script>";
                       break;
                    case SUCCESS:
                        echo "<script>alert('Database was restored!');</script>";
                        break;
                    //default is used to know if to start a download or not
                    //the result is already a success, so either download or just show a message
                    default:
                        if($_POST['submit'] == 3 || $_POST['submit'] == 4 )
                            echo $_POST['download'] == "1"? "<script>downloadFile('{$ret}');</script>" : "<script>alert('Database backup created successfuly!');</script>";
                            break;
                }
                unset($ret);
                unset($_POST['submit']);
            }
        ?>
    </main>
    </body>
</html>