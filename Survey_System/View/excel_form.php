<html>
<body>

<form id='excel_form' method="post" enctype="multipart/form-data">
    <input type="file" name="fileToUpload" id="fileToUpload">
    <input type="submit" value="Excel העלה קובץ" name="submit_upload">
</form>

<?php
session_start();
use Controller\ExcelHandler;
require_once $_SESSION['AUTOLOAD_PATH'];
if(isset($_POST['submit_upload'])) 
{
   ExcelHandler::handleUpload($_FILES["fileToUpload"]["tmp_name"]); 
}	
?>

</body>
</html>