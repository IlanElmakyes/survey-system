<?php
namespace View;
session_start();
if(!isset($_SESSION['MANAGER']))
    header("Location: index.php");
require $_SESSION['AUTOLOAD_PATH'];
?>
<html dir='rtl'>
    <meta charset='UTF-8'>
    <head>
        <title>Manager-Survey</title>
        <link rel="stylesheet" href="CSS\clickable_elements.css">
        <link rel="stylesheet" href="CSS\manager_pages.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">  
        <link rel="stylesheet" href="CSS\spinner.css">
    </head>
    <body onload="initializeSurveyPage()">
        <header>
            <div class="spinner"></div>
            <h2 class="header_inline">סקרים</h2>
            <hr class="header_block">
            <div id="errmsg_div" style='display:none;'></div>
        </header>
        <?php echo (Navigation::generateNav());?>
        <main>
            <div id="table_div" class="main_block"></div>
            <button type="button" class="open_form" id="new_survey_open_button" onclick="newSurveyPopupOpen('surveyForm','new_survey_open_button')">צור סקר חדש</button>
            <div class="popup_form_div" id="surveyForm" style="display : none;">
                <div id="surveyFormHeader" class="main_block"></div>
                <div id="surveyFormArticle" class="main_block"></div>
            </div>
        </main>
        <footer></footer>
        <script src="JS_Scripts\tableHandling_script.js"></script>
        <script src="JS_Scripts\formHandling_script.js"></script>
        <script src="JS_Scripts\managerSurveys_script.js"></script>
        <script src="JS_Scripts\spinnerActions.js"></script>
    </body>
</html>