<?php
namespace View;
session_start();
use Controller\ExcelHandler;
if(!isset($_SESSION['MANAGER']))
    header("Location: index.php");
require_once $_SESSION['AUTOLOAD_PATH'];
if(isset($_POST['submit_upload'])) 
{
    ExcelHandler::handleUpload($_FILES["fileToUpload"]["tmp_name"]); 
}
if(isset($_SESSION['RESULT']))
{
    echo "<script language='javascript'> alert('".$_SESSION['RESULT']."');</script>";
    unset($_SESSION['RESULT']);
}	
?>

<html dir='rtl'>
    <meta charset='UTF-8'>
    <head>
        <title>Upload Excel File</title>
        <link rel="stylesheet" href="CSS\clickable_elements.css">
        <link rel="stylesheet" href="CSS\manager_pages.css">
        <link rel="stylesheet" href="CSS\spinner.css">
        <script src="JS_Scripts\spinnerActions.js"></script>
    </head>
    <body>
    <header>
        <div class="spinner"></div>
        <h2 class="header_inline">העלת קובץ Excel</h2>
        <hr class="header_block">
    </header>
        <?php echo (Navigation::generateNav());?>
    <main>
        <div class="main_block">
        <h4>התחל בסיס נתונים בעזרת קובץ Excel שכולל פרטי מגמות, כיתות ומרצים</h4>
        <form id='excel_form' method="post" enctype="multipart/form-data">
            <input type="file" name="fileToUpload" id="fileToUpload" required>
            <button type="submit" value="Excel העלה קובץ" name="submit_upload" onclick="spinnerStart()">Excel העלה קובץ</button>
        </form>
        </div>
    </main>
    </body>
</html>