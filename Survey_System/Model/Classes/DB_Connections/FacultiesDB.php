<?php
namespace Model\Classes\DB_Connections;
use Model\Classes\Data_Objects as objects;
use \PDO;

require_once $_SESSION['AUTOLOAD_PATH'];
##class to connaect to and manipulate faculties and related tables
class FacultiesDB extends dbConnection implements iQuery
{
    //insert new faculty
    public function insert($toAdd)
    {
        $this->connect();
        $result = $this->connection->prepare("INSERT INTO faculties(`name`,`status`) VALUES(:name,:status)");
        $result->execute(["name"=>($toAdd->getName()),"status"=>($toAdd->getStatus())]);
        $this->disconnect();
    }

    
	//update faculty name and status
	public function update($toUpdate)
	{
		if(!empty($toUpdate))
		{
			$this->connect();
			$result = $this->connection->prepare("UPDATE faculties SET name=:updateName, status=:updateStatus WHERE id=:updateId");
			$result->execute(['updateName'=>($toUpdate->getName()), 'updateStatus'=>($toUpdate->getStatus()), 'updateId'=>($toUpdate->getID())]);
			$this->disconnect();
		}
    }
    
    //get all facuties with status 1
    public function getAll($dataType="object")
    {
        $arr = null;
        $this->connect();
        $result = $this->connection->prepare("SELECT * FROM faculties WHERE status=1");
		$result->execute();				   

        switch($dataType)
        {
            case "object":
                $arr = $result->fetchAll(\PDO::FETCH_CLASS,'Model\\Classes\\Data_Objects\\Faculties');
            break;
            case "assoc":
                $arr = $result->fetchAll(\PDO::FETCH_ASSOC);
            break;
        }
        $this->disconnect();
        return $arr;
    }

    //get faculty by name and status 1
    //if withDelete flag is true - return faculty even if its status is 0
    public function getByName($name, $withDelete=false)
    {
        $this->connect();
        if($withDelete)
            $result=$this->connection->prepare("SELECT * FROM faculties WHERE name= :nameToSearch");
        else
            $result=$this->connection->prepare("SELECT * FROM faculties WHERE name= :nameToSearch AND status=1");
        $result->execute(['nameToSearch'=>$name]);
		$this->disconnect();
		return $result->fetchObject('Model\\Classes\\Data_Objects\\Faculties');
    }

    //get faculty bu id
    public function getById($id)
    {
        $this->connect();
        $result=$this->connection->prepare("SELECT * FROM faculties WHERE id= :idToSearch AND status = 1");
		$result->execute(['idToSearch'=>$id]);
		$this->disconnect();
		return $result->fetchObject('Model\\Classes\\Data_Objects\\Faculties');
    }

    //function set status 0 to given faculty
    public function remove($primaryKeyToRemove)
    {
        $this->connect();
        $result = $this->connection->prepare("UPDATE faculties SET status = :value WHERE id = :input");
        $result->execute(["value"=>"0","input"=>$primaryKeyToRemove]);		 
        $this->disconnect();
    }
    
    //function return array of classes for given faculty
    public function getFacultyClasses($faculty,$dataType="object")											   
    {
        $arr = null;
        $this->connect();
        $result = $this->connection->prepare("SELECT * FROM classes WHERE faculty_id = :id AND status = 1");
        $result->execute(["id"=>($faculty->getId())]);
        switch($dataType)
        {
            case "object":
                $arr = $result->fetchAll(\PDO::FETCH_CLASS,'Model\\Classes\\Data_Objects\\Classes');
            break;
            case "assoc":
                $arr = $result->fetchAll(\PDO::FETCH_ASSOC);
            break;
        }
        $this->disconnect();
        return $arr;
    }
    public function getClassesByFacultyId($facultyId)
    {
        $arr;
        $this->connect();
        $result = $this->connection->prepare("SELECT * FROM classes WHERE faculty_id = :id AND status = 1");
        $result->execute(["id"=>$facultyId]);
        $this->disconnect();
        $arr = $result->fetchAll(\PDO::FETCH_CLASS,'Model\\Classes\\Data_Objects\\Classes');
        if(empty($arr))
            $arr = array();
        return $arr;
    }
	
}
?>