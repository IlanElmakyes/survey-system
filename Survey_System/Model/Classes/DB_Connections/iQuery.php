<?php
##interface for classes that connect to database
namespace Model\Classes\DB_Connections;
	interface iQuery
	{
		public function insert($toAdd);
		public function getAll($dataType="object");
		public function remove($primaryKeyToRemove);
	}
?>
