<?php ## class for connection to table questionnaire in database
namespace Model\Classes\DB_Connections;
use Model\Classes\Data_Objects as objects;
use \PDO;

require_once $_SESSION['AUTOLOAD_PATH'];

Class QuestionnaireDB extends dbConnection implements iQuery
{
	//insert new questionnaire
	//return inserted id
    public function insert($toAdd)
	{
		if(!empty($toAdd))
		{
			$this->connect();
			$result = $this->connection->prepare("INSERT INTO questionnaire (name, status, category_name) VALUES (:insertName, :insertStatus, :insertCategory)");
			$result->execute(['insertName'=>($toAdd->getName()), 'insertStatus'=>($toAdd->getStatus()), 'insertCategory'=>($toAdd->getCategory())]);
			$id=$this->connection->lastInsertId();
			$this->disconnect();
		}
		else $id=null;
		return $id;
	}

	//update questionnaire name and category by questionnaire ID
	public function update($toUpdate)
	{
		if(!empty($toUpdate))
		{
			$this->connect();
			$result = $this->connection->prepare("UPDATE questionnaire SET name=:updateName, category_name=:updateCategory WHERE id=:updateId");
			$result->execute(['updateName'=>($toUpdate->getName()), 'updateCategory'=>($toUpdate->getCategory()), 'updateId'=>($toUpdate->getID())]);
			$this->disconnect();
		}
	}
    //method returns all rows from questionnaire table
	//by default returns an array of objects
	//if flag "assoc" passed as parametr - returns an assoc array
	public function getAll($dataType="object")
	{
		$this->connect();
		$result = $this->connection->prepare("SELECT * FROM questionnaire WHERE status = 1");
		$result->execute();
		
		switch($dataType)
		{
		case "object":
			$arr = $result->fetchAll(\PDO::FETCH_CLASS,'Model\\Classes\\Data_Objects\\Questionnaire');
			break;

		case "assoc":
			$arr=$result->fetchAll(\PDO::FETCH_ASSOC);
			break;
		}
		$this->disconnect();
		return $arr;
    }
    
    //Change row status to 'false'
	public function remove($primaryKeyToRemove)
	{
		$this->connect();
		$result = $this->connection->prepare("UPDATE questionnaire SET status=0 WHERE id = :idToRemove");
		$result->execute(['idToRemove'=>$primaryKeyToRemove]);
		$this->disconnect();
	}

	//function returns object of questionnaire class by id
	public function getById($primaryKey)
	{
		$this->connect();
		$result=$this->connection->prepare("SELECT * FROM questionnaire WHERE id= :idToSearch");
		$result->execute(['idToSearch'=>$primaryKey]);
		$this->disconnect();
		return $result->fetchObject('Model\\Classes\\Data_Objects\\Questionnaire');
	}

	//function returns an assoc array(id, text) of questions for all questions in questionnaire with given id
	//in SELECT query check that status for all is active 
	public function getAllQuestions($primaryKey)
	{
		$this->connect();
		$result=$this->connection->prepare("SELECT q.id, q.text FROM questions q INNER JOIN questions_questionnaire qq ON q.id=qq.question_id INNER JOIN questionnaire ON qq.questionnaire_id=questionnaire.id WHERE q.status=1 AND questionnaire.status=1 AND qq.questionnaire_id=:idToSearch");
		$result->execute(['idToSearch'=>$primaryKey]);
		$arr=$result->fetchAll(\PDO::FETCH_ASSOC);
		$this->disconnect();
		return $arr;
	}

	//check if questionnaire is part of running active survey
	//recive questionnaire primary key
	//return true if questionnaire is a part of active survey that running now
	//otherwise return false
	public function isPartOfActiveSurvey($primaryKey)
	{
		$this->connect();
		$result=$this->connection->prepare("SELECT a.code FROM active_survey a INNER JOIN survey s ON a.survey_id=s.id INNER JOIN questionnaire_survey qs ON s.id=qs.survey_id WHERE qs.questionnaire_id=:idToSearch");
		$result->execute(['idToSearch'=>$primaryKey]);
		$arr=$result->fetchAll(\PDO::FETCH_ASSOC);
		$this->disconnect();
		$activeSurveyDb=new ActiveSurveyDB();
		for($i=0;$i<sizeof($arr);$i++)
		{
			if($activeSurveyDb->getActiveSurveyStatus($arr[$i]['code'])==ActiveSurveyDB::ACTIVE)
				return true;
		}
		return false;
	}

	//function return assoc array of all parent surveys (id and name) for questionnaire with given id
	public function getParentSurveys($primaryKey)
	{
		$this->connect();
		$result=$this->connection->prepare("SELECT s.id,s.name FROM survey s INNER JOIN questionnaire_survey qs ON s.id=qs.survey_id WHERE  s.status=1 AND qs.questionnaire_id=:idToSearch");
		$result->execute(['idToSearch'=>$primaryKey]);
		$arr=$result->fetchAll(\PDO::FETCH_ASSOC);
		$this->disconnect();
		return $arr;
	}

	//insert row to questions_questionnaire table
	public function addQuestion($questionnaireId,$questionId)
	{
		$this->connect();
		$result=$this->connection->prepare("INSERT INTO questions_questionnaire (question_id,questionnaire_id) VALUES (:questionId, :questionnaireId)");
		$result->execute(['questionId'=>$questionId,'questionnaireId'=>$questionnaireId]);
		$this->disconnect();
	}

	//delete row from questions_questionnaire table
	public function deleteQuestion($questionnaireId,$questionId)
	{
		$this->connect();
		$result=$this->connection->prepare("DELETE FROM questions_questionnaire WHERE question_id=:questionId AND questionnaire_id=:questionnaireId");
		$result->execute(['questionId'=>$questionId,'questionnaireId'=>$questionnaireId]);
		$this->disconnect();
	}

	//function removes all questions from given questionnaire
	public function deleteAllQuestions($questionnaireId)
	{
		$this->connect();
		$result=$this->connection->prepare("DELETE FROM questions_questionnaire WHERE questionnaire_id=:questionnaireId");
		$result->execute(['questionnaireId'=>$questionnaireId]);
		$this->disconnect();
	}
}
?>