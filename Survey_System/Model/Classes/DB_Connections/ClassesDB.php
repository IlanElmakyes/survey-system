<?php
namespace Model\Classes\DB_Connections;
use \PDO;
require_once $_SESSION['AUTOLOAD_PATH'];
##class to connect to and manipulate classes and related tables
class ClassesDB extends dbConnection implements iQuery
{
    //insert new class
    public function insert($toAdd)
    {
        $this->connect();
        $result = $this->connection->prepare("INSERT INTO classes(`name`,`faculty_id`,`status`) VALUES(:name,:facultyId,:status)");
        $result->execute(["name"=>($toAdd->getName()),"facultyId"=>($toAdd->getFacultyId()),"status"=>($toAdd->getStatus())]);
        $this->disconnect();
    }

    //update existing class
    public function update($toUpdate)
    {
        $this->connect();
        $result = $this->connection->prepare("UPDATE classes SET name=:name, faculty_id=:facultyId, status=:status WHERE id=:idToUpdate");
        $result->execute(["name"=>($toUpdate->getName()),"facultyId"=>($toUpdate->getFacultyId()),"status"=>($toUpdate->getStatus()),"idToUpdate"=>($toUpdate->getId())]);
        $this->disconnect();
    }

    //get all classes with status 1
    public function getAll($dataType="object")
    {
        $arr = null;
        $this->connect();
        $result = $this->connection->prepare("SELECT * FROM classes WHERE status = 1");
        $result->execute();

        switch($dataType)
        {
            case "object":
                $arr = $result->fetchAll(\PDO::FETCH_CLASS,'Model\\Classes\Data_Objects\\Classes');
            break;
            case "assoc":
                $arr = $result->fetchAll(\PDO::FETCH_ASSOC);
            break;
        }
        $this->disconnect();
        return $arr;

    }

    //function return assoc arr of all classes (class_id,class_name,faculty_name) 
    //with faculty names for each class to be represented in table 
    public function getAllForTable()
    {
        $this->connect();
        $result = $this->connection->prepare("SELECT c.id as class_id, c.name as class_name, f.name as faculty_name FROM classes c INNER JOIN faculties f ON c.faculty_id=f.id WHERE c.status = 1");
        $result->execute();
        $this->disconnect();
        return $result->fetchAll(\PDO::FETCH_ASSOC);
    }

    //function "removes" class - set class status to 0
    public function remove($primaryKeyToRemove)
    {
        $this->connect();
        $result = $this->connection->prepare("UPDATE classes SET status = :value WHERE id=:input");
        $result->execute(["value"=>"0","input"=>$primaryKeyToRemove]);
        $this->disconnect();
    }

    //get class by id
    public function getById($id)
    {
        $this->connect();
        $result=$this->connection->prepare("SELECT * FROM classes WHERE id= :idToSearch AND status = 1");
		$result->execute(['idToSearch'=>$id]);
		$this->disconnect();
		return $result->fetchObject('Model\\Classes\\Data_Objects\\Classes');
    }

    //get class by name
    public function getByName($name,$withDeleted=false)
    {
        $this->connect();
        if($withDeleted)
            $result=$this->connection->prepare("SELECT * FROM classes WHERE name= :nameToSearch");
        else
            $result=$this->connection->prepare("SELECT * FROM classes WHERE name= :nameToSearch AND status=1");
		$result->execute(['nameToSearch'=>$name]);
		$this->disconnect();
		return $result->fetchObject('Model\\Classes\\Data_Objects\\Classes');
    }

    //function returns an of all teachers for class with given id
    //in SELECT query check that status for all is active 
    //datatype is used to decide if an array of objects or an assoc array will be returned
    public function getAllTeachers($primaryKey,$dataType="assoc")
    {
        $this->connect();
		$result=$this->connection->prepare("SELECT t.id, t.name FROM teachers t INNER JOIN teachers_classes tc ON t.id=tc.teacher_id INNER JOIN classes c ON tc.class_id=c.id WHERE t.status=1 AND c.status=1 AND c.id= :idToSearch");
        $result->execute(['idToSearch'=>$primaryKey]);
        if($dataType=="assoc")
            $arr=$result->fetchAll(\PDO::FETCH_ASSOC);
        else if($dataType=="object")
            $arr = $result->fetchAll(\PDO::FETCH_CLASS,'Model\\Classes\Data_Objects\\Teachers');
		$this->disconnect();
		return $arr;
    }

    //add new teacher to class - 
    //add row to relation table teachers_classes
    public function addTeacherToClass($classId,$teacherId)
    {
        $this->connect();
        $result = $this->connection->prepare("INSERT INTO teachers_classes(teacher_id, class_id) VALUES(:teacherIdToAdd,:classIdToAdd)");
        $result->execute(['classIdToAdd'=>(int)$classId,"teacherIdToAdd"=>(int)$teacherId]);
        $this->disconnect();
    }

    //remove row from relation table teachers_classes
    public function removeTeacherFromClass($classId,$teacherId)
    {
        $this->connect();
        $result=$this->connection->prepare("DELETE FROM teachers_classes WHERE teacher_id=:teacherId AND class_id=:classId");
        $result->execute(['classId'=>(int)$classId,"teacherId"=>(int)$teacherId]);
        $this->disconnect();
    }

    //function return true if given class is participate in active survey 
    //otherwise return false
    public function isPartOfActiveSurvey($classId)
    {
        $this->connect();
        $result=$this->connection->prepare("SELECT * FROM active_survey WHERE class_id=:idToSearch");
        $result->execute(['idToSearch'=>$classId]);
        $arr=$result->fetchAll(\PDO::FETCH_ASSOC);
		$this->disconnect();
		$activeSurveyDb=new ActiveSurveyDB();
		for($i=0;$i<sizeof($arr);$i++)
		{
			if($activeSurveyDb->getActiveSurveyStatus($arr[$i]['code'])==ActiveSurveyDB::ACTIVE)
				return true;
		}
		return false;
    }
}
?>