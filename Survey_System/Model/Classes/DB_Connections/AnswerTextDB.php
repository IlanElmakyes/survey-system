<?php
namespace Model\Classes\DB_Connections;
use Model\Classes\Data_Objects as objects;
use \PDO;
require_once $_SESSION['AUTOLOAD_PATH'];
##class to connect to and manipulate answer_text table
class AnswerTextDB extends dbConnection implements iQuery
{
	//insert new text answer
	public function insert($toAdd)
	{
		$this->connect();
		$result = $this->connection->prepare("INSERT INTO answer_text(`text`, `activation_date_time`, `teacher_id`, `category_name`) VALUES(:text,:activationDateTime,:teacherId,:categoryName)");
		$result->execute(["text"=>($toAdd->getText()),"activationDateTime"=>($toAdd->getActivationDateTime()),"teacherId"=>($toAdd->getTeacherId()),
		"categoryName"=>($toAdd->getCategoryName())]);
		$this->disconnect();
	}
	//get all rows from answer_text table
	public function getAll($dataType="object")
	{
		$arr = null;
		$this->connect();
		$result = $this->connection->prepare("SELECT * FROM answer_text");
		$result->execute();
		
		switch($dataType)
		{
			case "object":
				$arr = $result->fetchAll(\PDO::FETCH_CLASS,'Model\\Classes\\Data_Objects\\AnswerText');
			break;
			case "assoc":
				$arr = $result->fetchAll(\PDO::FETCH_ASSOC);                  
			break;
		}
		$this->disconnect();
		return $arr;
	}
	//remove answer
	public function remove($primaryKeyToRemove)
	{
		$this->connect();
		$result = $this->connection->prepare("Delete FROM answer_text WHERE id = :id");
		$result->execute(["id"=>$primaryKeyToRemove]);
		$result->execute();
		$this->disconnect();
	}
	/**
	 * returns AnswerNumeric object array by category
	 * returns all answers from DONE surveys
	 */
	public function getAnswersByCategory($categoryName)
	{
		if(!isset($categoryName))
		return null;
		$allAnswers = array();
		$result;
		$activeSurveyDB = new ActiveSurveyDB();
		$activeSurveys = $activeSurveyDB->getReleventByCategory($categoryName);
		if(empty($activeSurvey))
			return null;
		//creating a quey that returns all answers from all surveys in the $activeSurveys array
		$query = "SELECT * FROM answer_text WHERE activation_date_time IN(:0";
		$surveyCount = count($activeSurveys);
		for($i=1;$i<$surveyCount;$i++)
			$query .= ",:".$i;
		$query .= ")";
		$this->connect();
		$result = $this->connection->prepare($query);
		for($i=0;$i<$surveyCount;$i++)
		{
			$date = activeSurveys[$i]->getDate();
			$result->bindParam(":".strval($i),$date);
		}
		$result->execute();
		$allAnswers = $result->fetchAll(\PDO::FETCH_CLASS,'Model\\Classes\\Data_Objects\\AnswerText');
		$this->disconnect();
		return $allAnswers;
	}
		/**
		 * returns AnswerText object array by teacher id
		 * returns all answers from the most relevent DONE surveys
		 */
		public function getAnswersByClassAndTeacher($classId,$teacherId)
		{
			$allAnswers = array();
			$result;
			$activeSurveyDB = new ActiveSurveyDB();
			$activeSurvey = $activeSurveyDB->getReleventByClassId($classId);
			if(empty($activeSurvey))
				return $allAnswers;
			$this->connect();
			$result = $this->connection->prepare("SELECT * FROM answer_text WHERE teacher_id=:teacherId AND activation_date_time=:surveyDateTime");
			$result->execute(["teacherId"=>$teacherId,"surveyDateTime"=>$activeSurvey->getDate()]);
			$allAnswers = $result->fetchAll(\PDO::FETCH_CLASS,'Model\\Classes\\Data_Objects\\AnswerText');
			$this->disconnect();
			return $allAnswers;
		}
		/**
		 * returns AnswerText object array by teacher id
		 * returns all answers from the most relevent DONE surveys
		 */
		public function getAnswersByTeacher($teacherId)
		{
			$allAnswers = array();
			if(!isset($teacherId))
				return $allAnswers;
			$result;
			$activeSurveyDB = new ActiveSurveyDB();
			$activeSurveys = $activeSurveyDB->getReleventByTeacher($teacherId);
			if(empty($activeSurveys))
				return $allAnswers;
			//creating a quey that returns all answers from all surveys in the $activeSurveys array
			$query = "SELECT * FROM answer_text WHERE teacher_id=:teacherId AND activation_date_time IN(:0";
			$surveyCount = count($activeSurveys);
			for($i=1;$i<$surveyCount;$i++)
				$query .= ",:".$i;
			$query .= ")";
			$this->connect();
			$result = $this->connection->prepare($query);
			$result->bindParam(":teacherId",$teacherId);
			for($i=0;$i<$surveyCount;$i++)
			{
				$date = $activeSurveys[$i]->getDate();
				$result->bindValue(":".strval($i),$date);
			}
			$result->execute();
			$allAnswers = $result->fetchAll(\PDO::FETCH_CLASS,'Model\\Classes\\Data_Objects\\AnswerText');
			$this->disconnect();
			return $allAnswers;
		}
		/**
		 * returns AnswerNumeric object array by class id AND category name
		 * returns all answers from DONE surveys
		 */
		public function getAnswersByClassAndCategory($classId,$categoryName)
		{
			$allAnswers = array();
			if(!isset($categoryName) || !isset($classId))
				return $allAnswers;
			$result;
			$activeSurveyDB = new ActiveSurveyDB();
			$activeSurvey = $activeSurveyDB->getReleventByClassId($classId);
			if(empty($activeSurvey))
				return $allAnswers;
			$this->connect();
			$result = $this->connection->prepare("SELECT * FROM answer_text WHERE category_name=:categoryName AND activation_date_time=:surveyDateTime");
			$result->execute(["categoryName"=>$categoryName,"surveyDateTime"=>$activeSurvey->getDate()]);
			$allAnswers = $result->fetchAll(\PDO::FETCH_CLASS,'Model\\Classes\\Data_Objects\\AnswerText');
			$this->disconnect();
			return $allAnswers;
		}
		public function getAnswersByFacultyAndCategory($facultyId,$categoryName)
		{
			$allAnswers= array();
			if(!isset($facultyId) || !isset($categoryName))
				return $allAnswers;
			$facultiesDB = new FacultiesDB();
			foreach($facultiesDB->getClassesByFacultyId($facultyId) as $class)
			{
				$allClassAnswers = self::getAnswersByClassAndCategory($class->getId(),$categoryName);
				foreach($allClassAnswers as $answer)
					if(!isset($allAnswers[$answer->getId()]))
						$allAnswers[$answer->getId()] = $answer;
			}
			return array_values($allAnswers);
		}
}
?>