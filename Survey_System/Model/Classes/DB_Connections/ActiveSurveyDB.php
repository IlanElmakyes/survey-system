<?php ##Class for connection to table active_survey in database
namespace Model\Classes\DB_Connections;
use Model\Classes\Data_Objects as objects;
use Model\Classes\Data_Objects\Questionnaire;
use \PDO;
require_once $_SESSION['AUTOLOAD_PATH'];
date_default_timezone_set("Asia/Jerusalem");
    class ActiveSurveyDB extends dbConnection implements iQuery
    {
        const DEFAULT_TIMER="1970-01-01T00:00:00";
        private $settings_path;
        private $timer_step;
        const DONE = -1;
        const INACTIVE = 0;
        const ACTIVE = 1;

        public function __construct()
        {
            parent::__construct();
            $this->settings_path=$_SESSION['HOME_PATH'].DIRECTORY_SEPARATOR."settings.ini";
            $this->setTimerStep();

        }
        //insert object ActiveSurvey as row in table
        public function insert($toAdd)
        {
            if(!empty($toAdd))
            {
                $this->connect();
                $result = $this->connection->prepare("INSERT INTO active_survey VALUES(:dateTime,:code,:classId,:surveyId,:timerStamp,:numberAnswered)");
                $result->execute(['dateTime'=>($toAdd->getDate()),'code'=>($toAdd->getCode()),'classId'=>($toAdd->getClassId()),'surveyId'=>($toAdd->getSurveyId()),
                'timerStamp'=>ActiveSurveyDB::DEFAULT_TIMER,'numberAnswered'=>($toAdd->getNumberAnswered())]);
                $this->disconnect();
            }
        }

        //method return all rows found in table
        //by default return array of objects
        //if passed flag "assoc" as parameter - return assoc array
        public function getAll($dataType="object")
        {
            $this->connect();
            $result = $this->connection->prepare("SELECT * FROM active_survey");
            $result->execute();

            switch($dataType)
            {
                case "object":
                    $arr = $result->fetchAll(\PDO::FETCH_CLASS,'Model\\Classes\\Data_Objects\\ActiveSurvey');
                break;
                case "assoc":
                    $arr = $result->fetchAll(\PDO::FETCH_ASSOC);
                break;
            }
            $this->disconnect();
            return $arr;
        }
        //delete row from table by primary key
        //parameter need to be assoc array with keys 'dateTime' and 'classId'
        //since the table has combined primary key
        public function remove($primaryKeyToRemove)
        {
            $this->connect();
            $result = $this->connection->prepare("Delete FROM active_survey WHERE date_time = :dateTime && class_id =:classId");
            $result->execute(["dateTime"=>$primaryKeyToRemove['dateTime'],"classId"=>$primaryKeyToRemove['classId']]);
            $this->disconnect();
        }
        /**
         * returnes an object or assoc array of an active survey by survey code.
         * $dataType - is used to decide if an object or an assoc array will be returned.
         */
        public function getByCode($code,$dataType="object")
        {
            $this->connect();
            $result=$this->connection->prepare("SELECT * FROM active_survey WHERE code= :codeToSearch");
            $result->execute(['codeToSearch'=>$code]);
            $this->disconnect();
            $ret = null;
            switch($dataType)
            {
                case "object":
                    $ret = $result->fetchAll(\PDO::FETCH_CLASS,'Model\\Classes\\Data_Objects\\ActiveSurvey');
                break;
                case "assoc":
                    $ret = $result->fetchAll(\PDO::FETCH_ASSOC);
                break;
            }
            //if the returned answer is of multiple rows, return an array
            //otherwise return a single answer
            if(count($ret) == 1)
                return $ret[0];
            else return $ret;
        }
        //get active survey by date_time value
        public function getByDateTime($dateTime)
        {
            $this->connect();
            $result=$this->connection->prepare("SELECT * FROM active_survey WHERE date_time= :dateTime");
            $result->execute(['dateTime'=>$dateTime]);
            $this->disconnect();
            return $result->fetchObject('Model\\Classes\\Data_Objects\\ActiveSurvey');
        }

        public function getTimerStep()
        {
            return $this->timer_step;
        }

        //take timer step value from settings file
        private function setTimerStep()
        {
            $settings=parse_ini_file($this->settings_path);
            $this->timer_step=new \DateInterval($settings['timer_step']);
        }

        //update timer step value in settings file
        public function updateTimerSetting($newTimerStep)
        {
            $settings=parse_ini_file($this->settings_path);
            $settings['timer_step']=$newTimerStep;
            $updateSet=fopen($this->settings_path,'w');
            if($updateSet===false)
                return false;
            else{
                foreach($settings as $key=>$value) {
                    if(fwrite($updateSet,$key."=".$value.PHP_EOL)===false)
                        return false;
                }
                fclose($updateSet);
            }
            return true;
        }


        //function checks if timer_start value of active survey with given code is equal to default timer value
        //if it is - update timer_start value in db for given active survey with current time and return true
        //if its not - check if current time smaller than timer_start value + timer step interval - if it is return true
        //return false otherwise
        public function checkTimer($code)
        {
            $state=$this->getActiveSurveyStatus($code);
            if($state==self::INACTIVE)
			{
				$this->updateTimer($code);
                return true;
			}
			else if($state==self::ACTIVE)
				return true;
			else
				return false;
        }

        public function getExpireDate($code)
        {
            $active=$this->getByCode($code);
            $surveyTimer = new \DateTime($active->getTimerStart());
            $expireDate=$surveyTimer->add($this->timer_step);
            $expireDate->add(new \DateInterval("PT1H"));
            return $expireDate->format("Y-m-d H:i:s");
        }

        //update timer_start value in active_survey table for survey with given code to be current time
		private function updateTimer($code)
		{
			$this->connect();
			$result=$this->connection->prepare("UPDATE active_survey SET timer_start=:timer WHERE code=:codeToSearch");
			$timer=new \DateTime();
			$res=$timer->format("Y-m-d H:i:s");
			$result->execute(['timer'=>$res,'codeToSearch'=>$code]);
			$this->disconnect();
        }
        //incrament the number of people answered by one
        public function updateAnswered($code)
        {
            $this->connect();
            $result = $this->connection->prepare("UPDATE active_survey SET number_answered = (number_answered+1) WHERE code=:codeToSearch");
            $result->execute(["codeToSearch"=>$code]);
            $this->disconnect();
        }
        /**
         * returns an object or an assoc array of an active survey of a single class.
         * $dataType - is used to decide if an object or an assoc array will be returned.
         */
        public function getActiveSurveyByClassId($classId,$dataType="object")
        {
            $this->connect();
            $result = $this->connection->prepare("SELECT * FROM active_survey WHERE class_id=:classId");
            $result->execute(["classId"=>$classId]);
            $this->disconnect();
            $ret = null;
            switch($dataType)
            {
                case "object":
                    $ret = $result->fetchAll(\PDO::FETCH_CLASS,'Model\\Classes\\Data_Objects\\ActiveSurvey');
                break;
                case "assoc":
                    $ret = $result->fetchAll(\PDO::FETCH_ASSOC);
                break;
            }
            //if the returned answer is of multiple rows, return the newest Active Survey by date/time
            //otherwise return a single answer
            if(empty($ret))
                return $ret;
            $newestActiveSurvey = $ret[0];
            foreach($ret as $activeSurvey)
            {
                if($activeSurvey->getdate() > $newestActiveSurvey->getdate())
                    $newestActiveSurvey = $activeSurvey;
            }
            return $newestActiveSurvey;
        }
        /**
         * returns the state of an active survey according to a timer
         * the status is defined by constants in the class
         */
        public function getActiveSurveyStatus($code)
        {
            $activeSurvey;
            $activeSurvey = $this->getByCode($code);
            //if the survey does not exist return null
            if(empty($activeSurvey))
                return null;
            //creating DateTime objects to use for comparision
            $defTime = new \DateTime(self::DEFAULT_TIMER);//this is the default timer of a new ActiveSurvey
            $surveyTimer = new \DateTime($activeSurvey->getTimerStart());//this is the current timer of the ActiveSurvey object
            $now = new \DateTime();//this is the current time
            if($surveyTimer == $defTime)
                return self::INACTIVE;
            else if($now > $surveyTimer->add($this->timer_step))
                return self::DONE;
            else return self::ACTIVE;
        }

        //function returns all active surveys for same survey id
        public function getBySurveyId($surveyId,$dataType="object")
        {
            $this->connect();
            $result = $this->connection->prepare("SELECT * FROM active_survey WHERE survey_id=:surveyId");
            $result->execute(["surveyId"=>$surveyId]);
            $this->disconnect();
            $ret = null;
            switch($dataType)
            {
                case "object":
                    $ret = $result->fetchAll(\PDO::FETCH_CLASS,'Model\\Classes\\Data_Objects\\ActiveSurvey');
                break;
                case "assoc":
                    $ret = $result->fetchAll(\PDO::FETCH_ASSOC);
                break;
            }
            return $ret;
        }

        //function returns all active surveys for same class by class name
        public function getByClassName($className)
        {
            $this->connect();
            $result = $this->connection->prepare("SELECT * FROM active_survey s INNER JOIN classes c WHERE c.name=:className AND c.id=s.class_id LIMIT 1");
            $result->execute(["className"=>$className]);
            $this->disconnect();
            $ret = $result->fetchObject('Model\\Classes\\Data_Objects\\ActiveSurvey');
            return empty($ret)?null:$ret;
        }
        /**
         * this function returns the relevent active survey (DONE survey with the highest number answered)
         */
        public function getReleventByClassId($classId)
        {
            $ret = null;
            $this->connect();
            $result = $this->connection->prepare("SELECT * FROM active_survey WHERE class_id=:classId");
            $result->execute(["classId"=>$classId]);
            $this->disconnect();
            $surveys = $result->fetchAll(\PDO::FETCH_CLASS,'Model\\Classes\\Data_Objects\\ActiveSurvey');
            if(empty($surveys)) return null;
            return $this->sortReleventActiveSurveys($surveys)[0];
        }
        /**
         * returns an array of all the most relevent active surveys that are tied to a teacher
         * that are also in the DONE state
         * **most relevent is with the highest number answered
         */
        public function getReleventByTeacher($teacherId)
        {
            $this->connect();
            $result = $this->connection->prepare("SELECT * FROM `active_survey` WHERE class_id IN(SELECT class_id FROM teachers_classes WHERE teacher_id=:teacherId)");
            $result->execute(["teacherId"=>$teacherId]);
            $this->disconnect();
            $surveys = $result->fetchAll(\PDO::FETCH_CLASS,'Model\\Classes\\Data_Objects\\ActiveSurvey');
            if(empty($surveys)) return null;
            return $this->sortReleventActiveSurveys($surveys);
        }

        public function getReleventByCategory($category)
        {
            $this->connect();
            $result = $this->connection->prepare("SELECT * FROM `active_survey` WHERE class_id IN(SELECT class_id FROM answer_numeric WHERE category_name=:categoryName)");
            $result->execute(["categoryName"=>$category]);
            $this->disconnect();
            $surveys = $result->fetchAll(\PDO::FETCH_CLASS,'Model\\Classes\\Data_Objects\\ActiveSurvey');
            if(empty($surveys)) return null;
            return $this->sortReleventActiveSurveys($surveys);
        }
        /**
         * gets an array of active surveys
         * returns an array of only the most relevent active survey for each class
         * (most relevent is only DONE surveys with the most number answered)
         */
        private function sortReleventActiveSurveys($activeSurveysArr)
        {
            $doneSurveys = array();
            foreach($activeSurveysArr as $survey)
                if($this->getActiveSurveyStatus($survey->getCode()) == self::DONE)
                    $doneSurveys[] = $survey;
            if(empty($doneSurveys))
                return null;
            $surveysByClass = array();
            foreach($doneSurveys as $survey)
            {
                if(isset($surveysByClass[$survey->getClassId()]))
                {
                    if($surveysByClass[$survey->getClassId()]->getNumberAnswered() < $survey->getNumberAnswered())
                        $surveysByClass[$survey->getClassId()] = $survey;
                    else if($surveysByClass[$survey->getClassId()]->getNumberAnswered() == $survey->getNumberAnswered())
                    {
                        if(new \DateTime($surveysByClass[$survey->getClassId()]->getDate()) < new \DateTime($survey->getDate()))
                            $surveysByClass[$survey->getClassId()] = $survey;
                    }
                }
                else $surveysByClass[$survey->getClassId()] = $survey;
            }
            return array_values($surveysByClass);
        }

        public function getAllCategorysByClassId($classId)
        {
            $ret = array();
            if(empty($classId))
                return $ret;
            $survey = $this->getReleventByClassId($classId);
            if(empty($survey))
                return $ret;
            $this->connect();
            $result = $this->connection->prepare("SELECT q.category_name FROM questionnaire as q INNER JOIN questionnaire_survey as qs ON qs.questionnaire_id=q.id WHERE qs.survey_id=:surveyId");
            $result->execute(["surveyId"=>($survey->getSurveyId())]);
            $ret = $result->fetchAll(PDO::FETCH_COLUMN);
            return $ret;

        }
    }
?>
