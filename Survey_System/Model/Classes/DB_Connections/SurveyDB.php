<?php
namespace Model\Classes\DB_Connections;
use Model\Classes\Data_Objects as objects;
use \PDO;
require_once $_SESSION['AUTOLOAD_PATH'];
##class to connect to and manipulate survey and related tables in database
class SurveyDB extends dbConnection implements iQuery
{
	//insert new survey
	public function insert($toAdd)
    {
        $this->connect();
        $result = $this->connection->prepare("INSERT INTO survey(`name`,`status`) VALUES(:name,:status)");
		$result->execute(["name"=>($toAdd->getName()),"status"=>($toAdd->getStatus())]);
		$id = $this->connection->lastInsertId();
		$this->disconnect();
		return $id;
	}
	//get all surveys with status 1
    public function getAll($dataType="object")
    {
        $this->connect();
		$result = $this->connection->prepare("SELECT * FROM survey WHERE status=1");
		$result->execute();
		
		switch($dataType)
		{
		case "object":
			$arr = $result->fetchAll(\PDO::FETCH_CLASS,'Model\\Classes\\Data_Objects\\Survey');
			break;

		case "assoc":
			$arr=$result->fetchAll(\PDO::FETCH_ASSOC);
			break;
		}
		$this->disconnect();
		return $arr;
	}
	//set status 0 to given survey
    public function remove($primaryKeyToRemove)
    {
        $this->connect();
		$result = $this->connection->prepare("UPDATE survey SET status = 0 WHERE id = :input");
		$result->execute(["input"=>$primaryKeyToRemove]);
		$this->disconnect();
	}
	
	//function returns an assoc array(id and category name) of all questionnaires in survey with given id
	//in SELECT query check that status for all is active 
	public function getAllQuestionnaires($primaryKey)
	{
		$this->connect();
		$result=$this->connection->prepare("SELECT q.id,q.category_name,q.name FROM questionnaire q INNER JOIN questionnaire_survey qs ON q.id=qs.questionnaire_id INNER JOIN survey ON qs.survey_id=survey.id WHERE q.status=1 AND survey.status=1 AND qs.survey_id= :idToSearch");
		$result->execute(['idToSearch'=>$primaryKey]);
		$arr=$result->fetchAll(\PDO::FETCH_ASSOC);
		$this->disconnect();
		return $arr;
	}

	//get survey with status 1 by id
	public function getSurveyById($surveyId,$dataType="object")
	{
		$this->connect();
		$result = $this->connection->prepare("SELECT * FROM survey WHERE status=1 AND id=:surveyId");
		$result->execute(["surveyId"=>$surveyId]);
		$res = null;
		switch($dataType)
		{
		case "object":
			$res = $result->fetchObject('Model\\Classes\\Data_Objects\\Survey');
			break;

		case "assoc":
			$res=$result->fetch(\PDO::FETCH_ASSOC);
			break;
		}
		$this->disconnect();
		return $res;
	}

	//add questionnaires to survey
	public function addQuestionnairesToSurvey($surveyId,$questionnairesIdArray)
	{
		$this->connect();
		$result = $this->connection->prepare("SELECT questionnaire_id FROM questionnaire_survey WHERE survey_id=:surveyId");
		$existingQuestionnairesId = $result->fetchAll(\PDO::FETCH_ASSOC);
		$this->disconnect();
		foreach($questionnairesIdArray as $questionnaireId)
		{
			if($this->checkIfExist("questionnaire","id",$questionnaireId) && !in_array($questionnaireId,$existingQuestionnairesId))
			{
				$this->connect();
				$result = $this->connection->prepare("INSERT INTO questionnaire_survey(survey_id,questionnaire_id) VALUES(:surveyId,:questionnaireId)");
				$result->execute(["surveyId"=>$surveyId,"questionnaireId"=>$questionnaireId]);
				$this->disconnect();
			}
		}
	}

	//remove all questionnaires from given survey
	public function deleteAllQuestionnairesFromSurvey($surveyId)
	{
		$this->connect();
		$result = $this->connection->prepare("DELETE FROM questionnaire_survey WHERE survey_id=:surveyId");
		$result->execute(["surveyId"=>$surveyId]);
		$this->disconnect();
	}

	//function updates survey name and questionnaires list
	public function updateSurvey($surveyObj,$surveyQuestionnairesIdArray)
	{
		$this->deleteAllQuestionnairesFromSurvey($surveyObj->getId());
		$this->addQuestionnairesToSurvey($surveyObj->getId(),$surveyQuestionnairesIdArray);
		$this->connect();
		$result = $this->connection->prepare("UPDATE survey SET name=:newName WHERE id=:surveyId");
		$result->execute(["newName"=>($surveyObj->getName()),"surveyId"=>($surveyObj->getId())]);
		$this->disconnect();
	}
}
?>