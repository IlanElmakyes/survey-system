<?php
namespace Model\Classes\DB_Connections;
use Model\Classes\Data_Objects as objects;
use \PDO;

require_once $_SESSION['AUTOLOAD_PATH'];
##class to connect to and manipulate answer_numeric table
class AnswerNumericDB extends dbConnection implements iQuery
{
		public function insert($toAdd)
		{
			if(!empty($toAdd))
			{
				$this->connect();
				$result = $this->connection->prepare("INSERT INTO answer_numeric (answer_1, answer_2, answer_3, answer_4, answer_5,
				 question_id, teacher_id, class_id, activation_date_time, category_name)
				  VALUES(:answer1,:answer2,:answer3,:answer4,:answer5,:questionId,:teacherId,:classId,:activationDateTime,:categoryName)");
				$result->execute(['answer1'=>($toAdd->getAnswer_1()),'answer2'=>($toAdd->getAnswer_2()),'answer3'=>($toAdd->getAnswer_3()),
				'answer4'=>($toAdd->getAnswer_4()),'answer5'=>($toAdd->getAnswer_5()),
				"questionId"=>($toAdd->getQuestionId()),"teacherId"=>($toAdd->getTeacherId()),"classId"=>($toAdd->getClassId()),
				"activationDateTime"=>($toAdd->getActivationDateTime()),"categoryName"=>($toAdd->getCategoryName())]);
				$this->disconnect();
			}
		}


		//function return all rows from answer_numeric table
		public function getAll($dataType="object")
		{
			$arr = null;
			$this->connect();
			$result = $this->connection->prepare("SELECT * FROM answer_numeric");
			$result->execute();
			
			switch($dataType)
            {
                case "object":
                    $arr = $result->fetchAll(\PDO::FETCH_CLASS,'Model\\Classes\\Data_Objects\\AnswerNumeric');
                break;
                case "assoc":
                    $arr = $result->fetchAll(\PDO::FETCH_ASSOC);                  
                break;
            }
            $this->disconnect();
            return $arr;
		}

		//delete answer
		public function remove($primaryKeyToRemove)
		{
			$this->connect();
            $result = $this->connection->prepare("Delete FROM answer_numeric WHERE id = :id");
            $result->execute(["id"=>$primaryKeyToRemove]);
            $this->disconnect();
		}
		/**
		 * updates an answer - by id - with an array of numeric values to add to the answer_x columns
		 * the query does - answer_x = answer_x + value -
		 * and the array should be automatic (keys are incrementing numerical values starting with 0)
		 */
		public function updateAnswerNumbersWithArr($answerId,$updatedAnswerArr)
		{
			$this->connect();
			$result = $this->connection->prepare("UPDATE answer_numeric SET answer_1=answer_1+:answer1,answer_2=answer_2+:answer2,
			answer_3=answer_3+:answer3,answer_4=answer_4+:answer4,answer_5=answer_5+:answer5
			WHERE id=:answerId");
			$result->execute(['answer1'=>($updatedAnswerArr[0]),'answer2'=>($updatedAnswerArr[1]),'answer3'=>($updatedAnswerArr[2]),
			'answer4'=>($updatedAnswerArr[3]),'answer5'=>($updatedAnswerArr[4]),'answerId'=>($answerId)]);
			$this->disconnect();

		}
		/**
		 * returns a single unique answer according to 4 keys:
		 * teacher id or category name - 
		 * 		if category name is not equal to special teacher category- category name will be used instead for search
		 * active survey activation date date
		 * question id
		 * class id
		 */
		public function getUniqueAnswer($teacherId,$activationDateTime,$questionId,$classId,$categoryName)
		{
			$this->connect();
			$result = null;
			if($categoryName===CategoryDB::teacher)
			{
				$result = $this->connection->prepare("SELECT * FROM answer_numeric WHERE question_id=:questionId AND teacher_id=:teacherId AND 
			 activation_date_time=:activeDateTime AND class_id=:classId");
				$result->execute(["questionId"=>$questionId,"teacherId"=>$teacherId,"activeDateTime"=>$activationDateTime,"classId"=>$classId]);
			}
			else
			{ 
				$result = $this->connection->prepare("SELECT * FROM answer_numeric WHERE question_id=:questionId AND activation_date_time=:activeDateTime AND class_id=:classId
				AND category_name=:categoryName");
				$result->execute(["questionId"=>$questionId,"activeDateTime"=>$activationDateTime,"classId"=>$classId,"categoryName"=>$categoryName]);
			}
			$this->disconnect();
			$ret = $result->fetchObject('Model\\Classes\\Data_Objects\\AnswerNumeric');
			return $ret?$ret:null;
		}
		/**
		 * takes an array of AnswerNumeric objects
		 * and returns an array containing only objects with unique ids
		 * after adding all the results of the similer answer objects
		 */
		public function unifySameAnswerResults($answerArray)
		{
			if(!is_array($answerArray))
				return array();
			$length = count($answerArray);
			$unified = array();
			for($i=0;$i<$length;$i++)
			{
				$currentId = $answerArray[$i]->getQuestionId();
				if(!isset($unified[$currentId]))
				{
					$unified[$currentId] = $answerArray[$i];
					for($j=$i+1;$j<$length;$j++)
					{
						if($currentId == $answerArray[$j]->getQuestionId())
							$unified[$currentId]->addAnswer($answerArray[$j]);
					}
				}
			}
			return $unified;
		}
		/**
		 * returns AnswerNumeric object array by teacher id
		 * returns all answers from DONE active surveys.
		 * will return all answers for that specific teacher
		 */
		public function getAnswersByTeacher($teacherId)
		{
			if(!isset($teacherId))
				return null;
			$allAnswers = array();
			$result;
			$activeSurveyDB = new ActiveSurveyDB();
			$activeSurveys = $activeSurveyDB->getReleventByTeacher($teacherId);
			if(empty($activeSurveys))
				return null;
			//creating a quey that returns all answers from all surveys in the $activeSurveys array
			$query = "SELECT * FROM answer_numeric WHERE teacher_id=:teacherId AND activation_date_time IN(:0";
			$surveyCount = count($activeSurveys);
			for($i=1;$i<$surveyCount;$i++)
				$query .= ",:".$i;
			$query .= ")";
			$this->connect();
			$result = $this->connection->prepare($query);
			$result->bindParam(":teacherId",$teacherId);
			for($i=0;$i<$surveyCount;$i++)
			{
				$date = $activeSurveys[$i]->getDate();
				$result->bindValue(":".strval($i),$date);
			}
			$result->execute();
			$allAnswers = $result->fetchAll(\PDO::FETCH_CLASS,'Model\\Classes\\Data_Objects\\AnswerNumeric');
			$this->disconnect();
			return $allAnswers;
		}
		/**
		 * returns AnswerNumeric object array by category
		 * returns all answers from the most relevent DONE surveys
		 */
		public function getAnswersByCategory($categoryName)
		{
			if(!isset($categoryName))
				return null;
			$allAnswers = array();
			$result;
			$activeSurveyDB = new ActiveSurveyDB();
			$activeSurveys = $activeSurveyDB->getReleventByCategory($categoryName);
			if(empty($activeSurvey))
				return null;
			//creating a quey that returns all answers from all surveys in the $activeSurveys array
			$query = "SELECT * FROM answer_numeric WHERE activation_date_time IN(:0";
			$surveyCount = count($activeSurveys);
			for($i=1;$i<$surveyCount;$i++)
				$query .= ",:".$i;
			$query .= ")";
			$this->connect();
			$result = $this->connection->prepare($query);
			for($i=0;$i<$surveyCount;$i++)
			{
				$date = $activeSurveys[$i]->getDate();
				$result->bindParam(":".strval($i),$date);
			}
			$result->execute();
			$allAnswers = $result->fetchAll(\PDO::FETCH_CLASS,'Model\\Classes\\Data_Objects\\AnswerNumeric');
			$this->disconnect();
			return $allAnswers;
		}
		/**
		 * returns AnswerNumeric object array by class id
		 * returns all answers from DONE surveys
		 */
		public function getAnswersByClass($classId)
		{
			if(!isset($classId))
				return null;
			$allAnswers = array();
			$result;
			$activeSurveyDB = new ActiveSurveyDB();
			$activeSurvey = $activeSurveyDB->getReleventByClassId($classId);
			if(empty($activeSurvey))
				return null;
			$this->connect();
			$result = $this->connection->prepare("SELECT * FROM answer_numeric WHERE class_id=:classId AND activation_date_time=:surveyDateTime");
			$result->execute(["classId"=>$classId,"surveyDateTime"=>$activeSurvey->getDate()]);
			$allAnswers = $result->fetchAll(\PDO::FETCH_CLASS,'Model\\Classes\\Data_Objects\\AnswerNumeric');
			$this->disconnect();
			return $allAnswers;
		}

			/**
		 * returns AnswerNumeric object array by class id AND teacher id
		 * returns all answers from DONE surveys
		 */
		public function getAnswersByClassAndTeacher($classId,$teacherId)
		{
			if(!isset($teacherId) || !isset($classId))
				return null;
			$allAnswers = array();
			$result;
			$activeSurveyDB = new ActiveSurveyDB();
			$activeSurvey = $activeSurveyDB->getReleventByClassId($classId);
			if(empty($activeSurvey))
				return null;
			$this->connect();
			$result = $this->connection->prepare("SELECT * FROM answer_numeric WHERE teacher_id=:teacherId AND class_id=:classId AND activation_date_time=:surveyDateTime");
			$result->execute(["teacherId"=>$teacherId,"classId"=>$classId,"surveyDateTime"=>$activeSurvey->getDate()]);
			$allAnswers = $result->fetchAll(\PDO::FETCH_CLASS,'Model\\Classes\\Data_Objects\\AnswerNumeric');
			$this->disconnect();
			return $allAnswers;
		}
		/**
		 * returns AnswerNumeric object array by class id AND category name
		 * returns all answers from DONE surveys
		 */
		public function getAnswersByClassAndCategory($classId,$categoryName)
		{
			$allAnswers = array();
			if(!isset($categoryName) || !isset($classId))
				return $allAnswers;
			$result;
			$activeSurveyDB = new ActiveSurveyDB();
			$activeSurvey = $activeSurveyDB->getReleventByClassId($classId);
			if(empty($activeSurvey))
				return $allAnswers;
			$this->connect();
			$result = $this->connection->prepare("SELECT * FROM answer_numeric WHERE category_name=:categoryName AND class_id=:classId AND activation_date_time=:surveyDateTime");
			$result->execute(["categoryName"=>$categoryName,"classId"=>$classId,"surveyDateTime"=>$activeSurvey->getDate()]);
			$allAnswers = $result->fetchAll(\PDO::FETCH_CLASS,'Model\\Classes\\Data_Objects\\AnswerNumeric');
			$this->disconnect();
			if(empty($allAnswers))
				$allAnswers = array();
			return $allAnswers;
		}
		public function getAnswersByFacultyAndCategory($facultyId,$categoryName)
		{
			if(!isset($facultyId) || !isset($categoryName))
				return array();
			$facultiesDB = new FacultiesDB();

			$allAnswers = array();
			foreach($facultiesDB->getClassesByFacultyId($facultyId) as $class)
				$allAnswers = array_merge($allAnswers,self::getAnswersByClassAndCategory($class->getId(),$categoryName));
			return self::unifySameAnswerResults($allAnswers);
		}

}
?>