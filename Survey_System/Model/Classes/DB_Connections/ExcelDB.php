<?php
##Class ExcelDB - insert array with prepared data from Excel file to DataBase
namespace Model\Classes\DB_Connections;
use Model\Classes\Data_Objects as objects;
use \PDO;

require_once $_SESSION['AUTOLOAD_PATH'];


Class ExcelDB extends dbConnection 
{
    //insert all data to DB
    public function insertData($data)
    {
        $classDB=new ClassesDB();
        foreach($data as $facultyName=>$classes)
        {
            $faculty=$this->addFaculty($facultyName);
            foreach($classes as $className=>$teachers)
            {
                $class=$this->addClass($className,$faculty);
                foreach($teachers as $teacherName=>$v)
                {
                    $teacher=$this->addTeacher($teacherName);
                    if(!$classDB->checkIfRelationExist("teachers_classes","teacher_id",$teacher->getId(),
                    "class_id",$class->getId())) {
                        $classDB->addTeacherToClass($class->getId(),$teacher->getId());
                    }                   
                }
            }
        }
    }

    //insert faculty to DB if its not exist
    //return existing or inserted faculty
    private function addFaculty($facultyName)
    {
        $facultyDB=new FacultiesDB();
        if(!$facultyDB->checkIfExist('faculties','name',$facultyName))
        {
            $faculty=new objects\Faculties(-1,$facultyName,1);
            $facultyDB->insert($faculty);
        }
        
        $faculty=$facultyDB->getByName($facultyName,true);
        if($faculty->getStatus()==0)
        {
            $faculty->setStatus(1);
            $facultyDB->update($faculty);
        }
        return $faculty;
    }

    //insert class to DB if its not exist
    //return existing or inserted class
    private function addClass($className,$faculty)
    {
        $classDB=new ClassesDB();
        if(!$classDB->checkIfExist('classes','name',$className))
        {
            $class=new objects\Classes(-1,$className,$faculty->getId(),1);
            $classDB->insert($class);
        }
         
        $class=$classDB->getByName($className,true);
        if($class->getStatus()==0)
        {
            $class->setStatus(1);
            $classDB->update($class);
        }
        return $class;
    }

    //insert teacher to DB if its not exist
    //return existing or inserted teacher
    private function addTeacher($teacherName)
    {
        $teacherDB=new TeachersDB();
        if(!$teacherDB->checkIfExist('teachers','name',$teacherName))
        {
            $teacher=new objects\Teachers(-1,$teacherName,1);
            $teacherDB->insert($teacher);
        }
        
        $teacher=$teacherDB->getByName($teacherName,true);
        if($teacher->getStatus()==0)
        {
            $teacher->setStatus(1);
            $teacherDB->update($teacher);
        }
        return $teacher;
    }
}
?>