<?php
namespace Model\Classes\DB_Connections;
use Model\Classes\Data_Objects as objects;
use \PDO;
require_once $_SESSION['AUTOLOAD_PATH'];
##class to connect to and manipulate teachers and related tables in database
class TeachersDB extends dbConnection implements iQuery
{
	//insert new teacher
	public function insert($toAdd)
    {
        $this->connect();
        $result = $this->connection->prepare("INSERT INTO teachers(`name`,`status`) VALUES(:name,:status)");
        $result->execute(["name"=>($toAdd->getName()),"status"=>($toAdd->getStatus())]);
        $this->disconnect();
	}
	
	//update teacher name and status
	public function update($toUpdate)
	{
        $this->connect();
        $result = $this->connection->prepare("UPDATE teachers SET name=:updateName, status=:updateStatus WHERE id=:updateId");
        $result->execute(['updateName'=>($toUpdate->getName()), 'updateStatus'=>($toUpdate->getStatus()), 'updateId'=>($toUpdate->getID())]);
        $this->disconnect();
	}

	//get all teachers with status 1
    public function getAll($dataType="object")
    {
        $this->connect();
		$result = $this->connection->prepare("SELECT * FROM teachers WHERE status=1");
		$result->execute();
		
		switch($dataType)
		{
		case "object":
			$arr = $result->fetchAll(\PDO::FETCH_CLASS,'Model\\Classes\\Data_Objects\\Teachers');
			break;

		case "assoc":
			$arr=$result->fetchAll(\PDO::FETCH_ASSOC);
			break;
		}
		$this->disconnect();
		return $arr;
	}
	
	//return teacher with given name and status 1
	//if withDelete flag is true - return teacher even if its status is 0
	public function getByName($name,$withDelete=false)
    {
		$this->connect();
		if($withDelete)
			$result=$this->connection->prepare("SELECT * FROM teachers WHERE name= :nameToSearch");
		else	
			$result=$this->connection->prepare("SELECT * FROM teachers WHERE name= :nameToSearch AND status=1");
		$result->execute(['nameToSearch'=>$name]);
		$this->disconnect();
		return $result->fetchObject('Model\\Classes\\Data_Objects\\Teachers');
	}
	
	//function return teacher with given id
	public function getById($id)
    {
        $this->connect();
        $result=$this->connection->prepare("SELECT * FROM teachers WHERE id= :idToSearch AND status=1");
		$result->execute(['idToSearch'=>$id]);
		$this->disconnect();
		return $result->fetchObject('Model\\Classes\\Data_Objects\\Teachers');
	}

	//function set status 0 to given teacher
    public function remove($primaryKeyToRemove)
    {
        $this->connect();
		$result = $this->connection->prepare("UPDATE teachers SET status=:value WHERE id = :input");
		$result->execute(["value"=>"false","input"=>$primaryKeyToRemove]);
		$this->disconnect();
	}
	
	//function return assoc arr of classes (class id, class name, faculty name) of given teacher
	public function getClasses($id)
	{
		$this->connect();
		$result=$this->connection->prepare("SELECT class_id,c.name as class_name, f.name as faculty_name FROM `teachers_classes` INNER JOIN classes c ON class_id=c.id INNER JOIN faculties f ON c.faculty_id=f.id WHERE teacher_id=:idToSearch AND c.status=1");
		$result->execute(["idToSearch"=>$id]);
		return $result->fetchAll(\PDO::FETCH_ASSOC);
	}

	//function removes all classes from classes list for given teacher - 
	//removes all rows from teachers_classes relation table where teacher id is equal to given id
	public function removeFromAllClasses($id)
	{
		$this->connect();
        $result=$this->connection->prepare("DELETE FROM teachers_classes WHERE teacher_id=:teacherId");
        $result->execute(["teacherId"=>(int)$id]);
        $this->disconnect();
	}

	/**
	 * returns an assoc array with information about
	 * faculty -> name and id
	 * classes -> name and id
	 * that the teacher is currently teaching
	 * the array looks like this:
	 * array(['faculty_name']=>array(...),['faculty_id']=>array(...),['class_id']=>array(...),['class_name']=>array(...))
	 */
	public function getClassesAndFacultyInformation($teacherId)
	{
		$this->connect();
		$result = $this->connection->prepare("SELECT f.name as faculty_name,f.id as faculty_id,c.id as class_id,c.name as class_name FROM classes c INNER JOIN faculties f ON c.faculty_id = f.id INNER JOIN teachers_classes tc ON tc.class_id=c.id WHERE tc.teacher_id=:teacherId");
		$result->execute(["teacherId"=>$teacherId]);
		$this->disconnect();
		return $result->fetchAll(\PDO::FETCH_ASSOC);
	}

}
?>