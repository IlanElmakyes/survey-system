<?php ##This class is used to create a connection to the DB
namespace Model\Classes\DB_Connections;
require_once $_SESSION['AUTOLOAD_PATH'];
use \PDO;
	class dbConnection
	{
		private const BACKUP_PATH = "../Model/Backups/";
		private const BACKUP_DIR = "../Model/Backups";
		private const CREATE_TABLES_FILE_PATH = "../Model/Backups/DB_FUNDAMENTALS/createTables.sql";
		private const TABLE_VALUE_SEPERATOR = "$!";
		private const TABLE_VALUES_END = "$@";
		private const TABLE_NAME_MARKER = "$?";
		private $host;
		private $db;
		private $charset;
		private $user;
		//these are some optinal flags we set to get data in a mannaer of our choosing
		private $opt = array(
			PDO::ATTR_ERRMODE=>PDO::ERRMODE_EXCEPTION,
			PDO::ATTR_DEFAULT_FETCH_MODE=>PDO::FETCH_ASSOC);
		protected $connection;
		
		//the constructor is used to initialize all the variables with the correct data to our DB
		public function __construct(string $host="localhost", string $db="user10DB",
		string $charset="utf8", string $user="user10DB_user", string $pass="12345678")
		{
			$this->host = $host;
			$this->db = $db;
			$this->charset = $charset;
			$this->user = $user;
			$this->pass = $pass;
		}
		
		//here a PDO object is created with a connection to the DB
		protected function connect()
		{
			$dns = "mysql:host=$this->host;dbname=$this->db;charset=$this->charset";
			$this->connection = new PDO($dns,$this->user,$this->pass,$this->opt);
		}
		
		public function disconnect()
		{
			$this->connection=null;
		}
		public function checkIfExist($tableName,$columnName,$value)
		{
			$this->connect();
			$result = $this->connection->prepare("SELECT $columnName FROM $tableName WHERE $columnName = :searchValue");
			$result->bindParam(":searchValue",$value);
			$result->execute();
			$this->disconnect();
			return ($result->fetchAll() != null);
		}

		//function to check rows in relation tables 
		//recieve table name, first column name and value, second column name and value
		//return true if row exist, otherwise return false 
		public function checkIfRelationExist($tableName,$clmnName1,$value1,$clmnName2,$value2)
		{
			$this->connect();
			$result=$this->connection->prepare("SELECT * FROM $tableName WHERE $clmnName1 = :v1 AND $clmnName2 = :v2");
			$result->execute(["v1"=>$value1,"v2"=>$value2]);
			$this->disconnect();
			return ($result->fetchAll()!=null);
		}
		/**
		 * returns the latest backup file path in the local backup folder.
		 * if no file is found return false
		 */
		public static function getLocalBackupFile()
		{
			$filePath="";
			$files = scandir(self::BACKUP_DIR,1);
			if(is_array($files))
				$length = count($files);
			else $length = 0;
			//searching only .sql files in the folder
			for($i=0;$i<$length && $filePath===""; $i++)
				//the first .sql file should be the newest, since the file list was created in decending order
				if(strpos($files[$i],".sql") !== false)
					$filePath = self::BACKUP_PATH.$files[$i];
			//if no .sql files were found, return false
			file_put_contents(getcwd()."Ilan_Log.txt",$filePath);
			return $filePath?$filePath:false;
		}
		/**
		 * creates an sql file with queries to insert all of the DATA in the DB
		 * *****************it does not handle DB or table CREATION**********************
		 * returns a string with the relative file path created
		 * or false if failed to open file
		 */
		public function backupDatabase()
		{
			try
			{
				$FILE_PATH = dbConnection::BACKUP_PATH.date('ymd_Hms',time()).".sql";
				$file = fopen($FILE_PATH,"w");
				if(!$file)
					return false;

				$this->connect();
				//getting an array of all table names
				$result = $this->connection->prepare("SHOW TABLES");
				$result->execute();
				$tables = $result->fetchAll(PDO::FETCH_COLUMN);
				//for each table, generate an insert query and write it to file
				foreach($tables as $table)
				{
					fwrite($file,$this->getTableValuesAsString($table));
				}
				fclose($file);
				$this->disconnect();
			}
			catch(Exception $e)
			{
				$this->disconnect();
				fclose($file);
				return false;
			}
			return $FILE_PATH;
		}

		private function getTableValuesAsString($table,$connect=false)
		{
			$tableValueString = dbConnection::TABLE_NAME_MARKER.$table.dbConnection::TABLE_NAME_MARKER."\n";
			if($connect)
				$this->connect();
			$result = $this->connection->prepare("SELECT * FROM $table");
			$result->execute();
			$tableData = $result->fetchAll(PDO::FETCH_ASSOC);
			if(empty($tableData))
				return "";
			foreach($tableData as $row)
			{
				foreach($row as $columnName => $columnValue)
				{
					$tableValueString .= dbConnection::TABLE_VALUE_SEPERATOR.$columnValue.dbConnection::TABLE_VALUE_SEPERATOR;
					$tableValueString .=",";
				}
				//removing the last ','
				$tableValueString = substr($tableValueString,0,-1);
				$tableValueString .= "\n";
			}
			//mark end of values
			$tableValueString .= dbConnection::TABLE_VALUES_END;
			$tableValueString .= "\n\n";
			if($connect)
				$this->disconnect();
			return $tableValueString;
			
		}
		
		/**
		 * restores a database according to a given file
		 * ALL THE OLD DATA WILL BE DELETED!!!!
		 */
		public function restoreDatabase($filePath="")
		{
			try
			{
				$insertQueryArray = $this->getInsertQueryArray($filePath);
				if(empty($insertQueryArray))
					return false;
				$this->purgeDatabase();
				$this->connect();
				$this->setForeignKeyChecks(false);
				foreach($insertQueryArray as $tableName=>$tableArray)
				{
					if(empty($tableName))
						continue;
					$valueArray = $tableArray['tableValues'];
					$tableValuesCount = count($valueArray);
					$result = $this->connection->prepare($this->getInsertQueryString($tableName,$tableArray));
					for($i=0;$i<$tableValuesCount;$i++)
					{
						if($valueArray[$i] === "-1" || $valueArray[$i] === '' || $valueArray[$i] == '' ){
							$result->bindValue(":".strval($i),NULL,PDO::PARAM_NULL);
						}
						else{
							$result->bindValue(":".strval($i),$valueArray[$i]);
						}
					}
					$result->execute();
				}
				$this->setForeignKeyChecks(true);
				$this->disconnect();
				return true;
			}
			catch(Exception $e)
			{
				$this->disconnect();
				return false;
			}
		}
		private function getInsertQueryString($tableName,$tableArray)
		{
			$tableValuesCount = count($tableArray['tableValues']);
			$tableColumnCount = count($tableArray['ColumnNames']);

			$insertQuery = "INSERT INTO `{$tableName}`(";
			foreach($tableArray['ColumnNames'] as $columnName)
				$insertQuery .= "`{$columnName}`,";
			//removing the last ','
			$insertQuery = substr($insertQuery,0,-1);

			$insertQuery .= ") VALUES";
			$i=0;
			while($i<$tableValuesCount)
			{
				$j=0;
				$insertQuery .= "(";
				while($j<$tableColumnCount && $i<$tableValuesCount)
				{
					$insertQuery .= ":".$i.",";
					$j++;
					$i++;
				}
				//removing the last ','
				$insertQuery = substr($insertQuery,0,-1);
				$insertQuery .= "),\n";
			}
			$insertQuery = substr($insertQuery,0,-2).";";
			return $insertQuery;		
		}

		private function getInsertQueryArray($filePath)
		{
			$insertQueryArray = array();
			$tableNames = array();
			$tableColumns = array();
			$tableValues = array();
			if($filePath === "")
				return false;
			$file = fopen($filePath,"r");
			if(!$file || feof($file))
				return false;
			while(!feof($file))
			{
				$tableName = $this->readToNextTableValuesAndReturnTableName($file);
				$insertQueryArray[$tableName] = array(
					"ColumnNames"=> $this->getTableColumnNamesArray($tableName),
					"tableValues"=>$this->readNextTableValuesAndReturnValuesArray($file));
			}
			return $insertQueryArray;
		}

		private function readNextTableValuesAndReturnValuesArray($fileStream)
		{
			$valueArray = array();
			$currentValue = "";
			$tableValue = "";
			$currentlyReadingTableValue = false;
			if(!$fileStream || feof($fileStream))
				return false;
			while(!feof($fileStream) && strpos($currentValue,dbConnection::TABLE_VALUES_END) === false)
			{
				$currentValue .= fread($fileStream,1);
				if(strpos($currentValue,dbConnection::TABLE_VALUE_SEPERATOR) !== false)
				{
					$currentlyReadingTableValue = true;
					while($currentlyReadingTableValue && !feof($fileStream))
					{
						$tableValue .= fread($fileStream,1);
						if(strpos($tableValue,dbConnection::TABLE_VALUE_SEPERATOR) !== false)
						{
							$currentlyReadingTableValue = false;
							$valueArray[] = str_replace(dbConnection::TABLE_VALUE_SEPERATOR,'',$tableValue);
							$currentValue = "";
							$tableValue = "";
						}
					}
				}
			}
			return $valueArray;
		}

		private function getTableColumnNamesArray($tableName)
		{
			if(!$tableName)
				return null;
			$this->connect();
			$result = $this->connection->prepare("SHOW COLUMNS FROM {$tableName}");
			$result->execute(["table"=>$tableName]);
			return $result->fetchAll(PDO::FETCH_COLUMN);
			 
		}

		private function readToNextTableValuesAndReturnTableName($fileStream)
		{
			$currentValue = "";
			$tableName = "";
			$currentlyReadingTableName = false;
			if(!$fileStream || feof($fileStream))
				return "";
			while(!feof($fileStream) && empty($tableName))
			{
				$currentValue .= fread($fileStream,1);
				if(strpos($currentValue,dbConnection::TABLE_NAME_MARKER) !== false)
				{
					$currentlyReadingTableName = true;
					while($currentlyReadingTableName && !feof($fileStream))
					{
						$tableName .= fread($fileStream,1);
						if(strpos($tableName,dbConnection::TABLE_NAME_MARKER) !== false)
							$currentlyReadingTableName = false;
					}
				}
			}
			return str_replace(dbConnection::TABLE_NAME_MARKER,'',$tableName);
		} 
		
		private function setForeignKeyChecks($shouldBeSet=true,$connect=false)
		{
			if($connect)
				$this->connect();
			if($shouldBeSet)
				$result = $this->connection->prepare("SET foreign_key_checks = 1");
			else
				$result = $this->connection->prepare("SET foreign_key_checks = 0");
			$result->execute();
			if($connect)
				$this->disconnect();

		}
		/**
		 * removes ALL THE DATA from all the tables in the DB
		 * ADVISE THE USER TO BACKUP BEFORE USING THIS FUNCTION
		 * 
		 * this function DROPS all the tables and then recreates them
		 */
		private function purgeDatabase()
		{
			clearstatcache();//filesize() can use cached results, this is to make sure it will check the current state
			if(!file_exists(dbConnection::CREATE_TABLES_FILE_PATH) || !filesize(dbConnection::CREATE_TABLES_FILE_PATH))
				$this->generateCreateTablesFile();
			$this->connect();
			$result = $this->connection->prepare("SHOW TABLES");
			$result->execute();
			//creating an array of all the table names
			$tables = $result->fetchAll(PDO::FETCH_COLUMN);
			//ignoring all the foregin key references before we drop the tables
			$this->setForeignKeyChecks(false);
			foreach($tables as $table)
			{
				$result = $this->connection->prepare("DROP TABLE $table");
				$result->execute();
			}
			//restoring foregin key check
			$this->setForeignKeyChecks(true);
			$this->disconnect();
			$this->createAllTables();
		}
		/**
		 * used the createTables.sql file to create all the tables (with constraints) in the DB
		 */
		private function createAllTables()
		{
			$createQuerys = file_get_contents(dbConnection::CREATE_TABLES_FILE_PATH);
			$createQuerys = explode("CREATE",$createQuerys);
			$this->connect();
			$this->setForeignKeyChecks(false);
			foreach($createQuerys as $query)
			{
				//explode can give us empty strings in the array, if so... skip!
				if($query === "")
					continue;
				//explode does not include the delimiter in the returned array elements, so it has be be added back;
				$query = "CREATE".$query;
				$result = $this->connection->prepare($query);
				$result->execute();
			}
			$this->setForeignKeyChecks(true);
			$this->disconnect();
		}
		/**
		 * creates a file with all the DB "CREATE TABLES" querys (including constraints)
		 */
		private function generateCreateTablesFile()
		{
			$this->connect();
			$result = $this->connection->prepare("SHOW TABLES");
			$result->execute();
			//creating an array of all the table names
			$tables = $result->fetchAll(PDO::FETCH_COLUMN);
			$this->disconnect();
			clearstatcache();//filesize() can use cached results, this is to make sure it will check the current state
			if(file_exists(dbConnection::CREATE_TABLES_FILE_PATH) && filesize(dbConnection::CREATE_TABLES_FILE_PATH))
				return;
			$file = fopen(dbConnection::CREATE_TABLES_FILE_PATH,"w");
			if($file)
			{
				$this->connect();
				foreach($tables as $table)
				{
					$result = $this->connection->prepare("SHOW CREATE TABLE $table");
					$result->execute();
					$tmp = $result->fetchColumn(1)."\n\n";
					fwrite($file,$tmp);
				}
				$this->disconnect();
			}
			fclose($file);
		}
	}
?>