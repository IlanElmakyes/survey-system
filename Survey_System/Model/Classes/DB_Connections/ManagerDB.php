<?php
//class is for connection to manager table in database
namespace Model\Classes\DB_Connections;
use Model\Classes\Data_Objects as objects;
use \PDO;
require_once $_SESSION['AUTOLOAD_PATH'];
class ManagerDB extends dbConnection implements iQuery
{
	//Stores the manager login information as hashed values in the database
	public function insert($toAdd)
	{
		if(!empty($toAdd))
		{
			$this->connect();
			$result = $this->connection->prepare("INSERT INTO manager VALUES (:newCode, :newPassword)");
			$newCode=password_hash($toAdd->getCode(),PASSWORD_DEFAULT);
			$newPassword=password_hash($toAdd->getPassword(),PASSWORD_DEFAULT);
			$result->execute(['newCode'=>$newCode,'newPassword'=>$newPassword]);
			$this->disconnect();
		}
	}
	
	//method returns all rows from manager table
	//by default returns an array of objects
	//if flag "assoc" passed as parametr - returns an assoc array
	public function getAll($dataType="object")
	{
		$managerArr = null;
		$this->connect();
		$result = $this->connection->prepare("SELECT * FROM manager");
		$result->execute();
		
		switch($dataType)
		{
		case "object":
			$managerArr = $result->fetchAll(\PDO::FETCH_CLASS,'Model\\Classes\\Data_Objects\\Manager');
			break;

		case "assoc":
			$managerArr=$result->fetchAll(\PDO::FETCH_ASSOC);
			break;
		}
		$this->disconnect();
		return $managerArr;
	}
	
	//Delete row form table manager by primary key - code
	public function remove($primaryKeyToRemove)
	{
		$this->connect();
		$result = $this->connection->prepare("DELETE FROM manager WHERE code = :toDeleteCode");
		$toDeleteCode=$primaryKeyToRemove;
		$result->execute(['toDeleteCode'=>$toDeleteCode]);
		$this->disconnect();
	}
	//checks if a manager username exist in the database.
	//if a username exist and a password is givin, checks if the password matchs the username
	//recivies a plain text user name and compares it to the hashed values in the table
	//**THERE IS NO NEED FOR PRE-HASHING**
	public function verifyManagerLogin($username,$password=null)
	{
		$managerArr = $this->getAll();
		$username_flag = true;
		$password_flag = true;
		for($i=0, $username_flag = false;$i<sizeof($managerArr) && !$username_flag ;$i++)
			$username_flag = password_verify($username,$managerArr[$i]->getCode());
		if($username_flag && !empty($password))
		{
			$password_flag = password_verify($password,$managerArr[$i-1]->getPassword());
		}
		return $username_flag && $password_flag;
	}

	public function updateManagerLoginInfo($usernameToChange,$newUsername,$newPassword)
	{
		if(!$usernameToChange || !$newUsername || !$newPassword)
			return false;
		$usernameToChange = $this->getHashedCodeValueFromDB($usernameToChange);
		if(empty($usernameToChange))
			return false;
		$this->connect();
		$result = $this->connection->prepare("UPDATE manager SET code = :newUsername, password = :newPassword  WHERE code=:currentUsername");
		$newCode=password_hash($newUsername,PASSWORD_DEFAULT);
		$newPassword=password_hash($newPassword,PASSWORD_DEFAULT);
		$result->execute(['newUsername'=>$newCode,'newPassword'=>$newPassword,'currentUsername'=>$usernameToChange]);
		$this->disconnect();
		return true;
	}
	private function getHashedCodeValueFromDB($code)
	{
		if(!$code)
			return;
		$allManagers = $this->getAll();
		foreach($allManagers as $manager)
		{
			if(\password_verify($code,$manager->getCode()))
				return $manager->getCode();
		}
	}
}
?>