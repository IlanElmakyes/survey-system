<?php
namespace Model\Classes\DB_Connections;
use Model\Classes\Data_Objects as objects;
use \PDO;

require_once $_SESSION['AUTOLOAD_PATH'];
##class to connect to and manipulate questions and related tables in database
Class QuestionsDB extends dbConnection implements iQuery
{
	//insert new question
	public function insert($toAdd)
	{
		if(!empty($toAdd))
		{
			//if the question existed but was inactive, it is re-activated
			if($this->checkIfExist("questions","text",$toAdd->getText()))
			{
				$this->connect();
				$result = $this->connection->prepare("UPDATE questions SET status=1 WHERE `text`=:checkedText");
				$result->execute(['checkedText'=>($toAdd->getText())]);
				$this->disconnect();
			}
			//if the question wasn't in the DB at all, a new row will be created with the question
			else
			{
				$this->connect();
				$result = $this->connection->prepare("INSERT INTO questions (text, status) VALUES (:insertText, :insertStatus)");
				$result->execute(['insertText'=>($toAdd->getText()), 'insertStatus'=>($toAdd->getStatus())]);
				$this->disconnect();
			}
		}
	}

	//method returns all rows from questions table, where status is true (1)
	//by default returns an array of objects
	//if flag "assoc" passed as parametr - returns an assoc array
	public function getAll($dataType="object")
	{
		$this->connect();
		$result = $this->connection->prepare("SELECT * FROM questions WHERE status = 1");
		$result->execute();
		
		switch($dataType)
		{
		case "object":
			$arr = $result->fetchAll(\PDO::FETCH_CLASS,'Model\\Classes\\Data_Objects\\Questions');
			break;

		case "assoc":
			$arr=$result->fetchAll(\PDO::FETCH_ASSOC);
			break;
		}
		$this->disconnect();
		return $arr;
	}

	//Change row status to 'false'
	public function remove($primaryKeyToRemove)
	{
		$this->connect();
		$result = $this->connection->prepare("UPDATE questions SET status=0 WHERE id = :idToRemove");
		$result->execute(['idToRemove'=>$primaryKeyToRemove]);
		$this->disconnect();
	}
	/**
	 * updates a question with new text
	 * if the question id is not found, the function does nothing
	 */
	public function update($questionKey,$text)
	{
		if(!$this->checkIfExist("questions","id",$questionKey))
			return false;
		//TODO - CHECK FOR ACTIVE DURVEY
		$this->connect();
		$result = $this->connection->prepare("UPDATE questions SET text=:newText WHERE id=:idKey");
		$result->execute(['newText'=>$text,'idKey'=>$questionKey]);
		$this->disconnect();
	}

	/**
	 * return the question text as string
	 */
	public function getTextById($questionId)
	{
		$this->connect();
		$result = $this->connection->prepare("SELECT text FROM questions WHERE id=:questionId");
		$result->execute(['questionId'=>$questionId]);
		$this->disconnect();
		return $result->fetchColumn();
	}
}
?>