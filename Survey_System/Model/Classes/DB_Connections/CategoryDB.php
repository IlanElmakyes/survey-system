<?php
namespace Model\Classes\DB_Connections;
require_once $_SESSION['AUTOLOAD_PATH'];
use \PDO;
##class to connect to and manipulate category table
class CategoryDB extends dbConnection implements iQuery
{
	const teacher="מרצה";//special teacher category name const
	//insert new category
	public function insert($toAdd)
	{
		$this->connect();
		$result =$this->connection->prepare("INSERT INTO category VALUES(:name,:status)");
		$result->execute(["name"=>($toAdd->getName()),"status"=>($toAdd->getStatus())]);
		$this->disconnect();
	}

	//get all rows from category table
	public function getAll($dataType="object")
	{
		$arr = null;
		$this->connect();
		$result = $this->connection->prepare("SELECT * FROM category WHERE status=1");
		$result->execute();
		
		switch($dataType)
		{
			case "object":
				$arr = $result->fetchAll(\PDO::FETCH_CLASS,'Model\\Classes\\Data_Objects\\Category');
			break;
			case "assoc":
				$arr = $result->fetchAll(\PDO::FETCH_ASSOC);                  
			break;
		}
		$this->disconnect();
		return $arr;
	}
	//This function sets the status of the category to false
	public function remove($primaryKeyToRemove)
	{
		$this->connect();
		$result = $this->connection->prepare("UPDATE category SET status = :value WHERE name = :input");
		$result->execute(["value"=>"false","input"=>$primaryKeyToRemove]);
		$this->disconnect();
	}
}
?>