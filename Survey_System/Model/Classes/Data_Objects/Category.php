<?php
namespace Model\Classes\Data_Objects;
require_once $_SESSION['AUTOLOAD_PATH'];
##class category - instance of row from category table
class Category
{
	protected $name;
	protected $status;
	
	public function __construct($newName = null, $newStatus = null)
	{
		if(!isset($this->name) && !empty($newName))
		{
			$this->name = $newName;
			$this->status = $newStatus;
		}
	}
	public function getName()
	{
		return $this->name;
	}
	public function setName($newName)
	{
		$this->name = $newName;
	}
	public function getStatus()
	{
		return $this->status;
	}
	public function setStatus($newStatus)
	{
		$this->status = $newStatus;
	}
}
?>