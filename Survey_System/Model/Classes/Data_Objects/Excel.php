<?php
##Class Excel - represent excel file
##Main class functionality is to read excel file, parse file, prepare data to insert to DB
namespace Model\Classes\Data_Objects;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;

require_once $_SESSION['AUTOLOAD_PATH'];
require_once $_SESSION['HOME_PATH']."/vendor/autoload.php";


class Excel
{
    private $fileType;
    private $filePath;

    public function __construct($filePath)
    {
        $this->filePath=$filePath;
        if(\file_exists($this->filePath))
            $this->fileType=static::getFileType($filePath);
        else $this->fileType=-1;
    }

    //file format filter 
    //need to be checked on upload and before moving file from default tmp directory
    public static function checkFileType($filePath)
    {
        if(file_exists($filePath))
        {
            $mimeType=mime_content_type($filePath);
            $fileType=static::getFileType($filePath);
            return ($fileType=='Xls'||$fileType=='Xlsx');
        }
        else return false;
    }

    //function return file type by checking mime 
    public static function getFileType($filePath)
    {            
        $mimeType=mime_content_type($filePath);
        if($mimeType=='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
            return 'Xlsx';
        else if($mimeType=="application/vnd.ms-excel")
            return 'Xls';
        else return -1;
    }

    //function prepare data to be inserted to DB 
    public function prepareData()
    {
        $data=$this->readDataOnly();
        if($data!=null)
            return $this->parseData($data);
        else return null;
    }

    //read data only (without any styling or formatting)
    //validate data format - check we have only 1 sheet and all required columns
    //return raw data array or null if format not good or can't read the file
    private function readDataOnly()
    {
        if(file_exists($this->filePath)&&$this->fileType!=-1)
        {
            $reader = IOFactory::createReader($this->fileType);
            $reader->setReadDataOnly(true);
            $spreadsheet = $reader->load($this->filePath);
            if($spreadsheet->getSheetCount()>1)
                return null;
            $data=$spreadsheet->getActiveSheet()->toArray(null,true,true,true);
            if(!$this->checkColumns($data))
                return null;
            return $data;
        }
        else return null;
    }

    //parse data from excel file to assoc array 
    private function parseData($data)
    {
        $keys=$this->getKeys($data[1]);        
        $preparedData=array();
        for($i=2;$i<sizeof($data);$i++)
        {
           $faculty= $this->getFacultyName($data[$i][$keys['כיתה']]);//get faculty name            
            if(!isset($preparedData[$faculty]))//add faculty if we don't have faculty in prepared data array
                $preparedData[$faculty]=array();
            $class=$this->getClassName($data[$i][$keys['כיתה']]);
            if(!isset($preparedData[$faculty][$class]))//add class to faculty if it's not present in prepared data array
                $preparedData[$faculty][$class]=array();
            $teacher=$data[$i][$keys['מרצים']];
            if(!isset($preparedData[$faculty][$class][$teacher]))//add teacher to class if it's not present
                $preparedData[$faculty][$class][$teacher]=true;
        }
        return $preparedData;
    }

    //get class name from excel table cell
    //class name is all words separeted by spaces after fisrt word with numeric value and including this word
    private function getClassName($cell)
    {
        $classRegex="/[0-9]+([\/]?[0-9]?)*/";
        $arr=explode(" ",$cell);
        for($i=0;$i<sizeof($arr)&&!preg_match($classRegex,$arr[$i]);$i++);
        $classNameArr=array_slice($arr,$i);
        $className=implode(" ",$classNameArr);

        return $className;
    }

    //get faculty name from excel table cell
    //faculty name is all words separted by spaces untill first word with numeric value in it
    private function getFacultyName($cell)
    {
        $classRegex="/[0-9]+([\/]?[0-9]?)*/";
        $arr=explode(" ",$cell);
        for($i=0;$i<sizeof($arr)&&!preg_match($classRegex,$arr[$i]);$i++);
        $facultyNameArr=array_slice($arr,0,$i);
        $facultyName=implode(" ",$facultyNameArr);

        return $facultyName;
    }

    //function return array of column keys in data 
    //so we can access data fields by column name like: $data[i][$keys['column name']]
    private function getKeys($data)
    {
        $keys=array();
        foreach($data as $key=>$value)
            {
                $keys[trim($value)]=$key;
            }
        return $keys;
    }

    //check if excel file contains expxted columns names
    private function checkColumns($data)
    {
        $colNames=['כיתה','מרצים'];
        foreach($data[1] as $v)
        {
            if(!in_array(trim($v),$colNames))
                return false;
        }
        return true;

    }
}
?>