<?php
namespace Model\Classes\Data_Objects;
require_once $_SESSION['AUTOLOAD_PATH'];
##class Survey is instance of row in survey table
class Survey
{
    protected $id;
    protected $name;
    protected $status;

    public function __construct($newId=null,$newName=null,$newStatus=null)
    {
        if(!isset($this->id)&& !empty($newId))
        {
            $this->id = $newId;
            $this->name = $newName;
            $this->status = $newStatus;
        }
    }
    

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }
    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }
    public function getStatus()
    {
        return $this->status;
    }
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }
}
?>