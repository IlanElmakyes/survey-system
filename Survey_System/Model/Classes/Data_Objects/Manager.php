<?php
//Class represent manager data - code and password
namespace Model\Classes\Data_Objects;
require_once $_SESSION['AUTOLOAD_PATH'];

	class Manager{
		protected $code;
		protected $password;
		
		public function __construct($newCode=null,$newPassword=0)
		{
		    if(!isset($this->code) && !empty($newCode))
			{
			$this->code = $newCode;
			$this->password = $newPassword;
			}
		}
		
		public function getCode()
		{
			return $this->code;
		}
		public function setCode($newCode)
		{
			$this->code = $newCode;
		}
		
		public function getPassword()
		{
			return $this->password;
		}
		public function setPassword($newPassword)
		{
			$this->password = $newPassword;
		}
	}
?>