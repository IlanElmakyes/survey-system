<?php ##Class is instance of row in table questionnaire
namespace Model\Classes\Data_Objects;
require_once $_SESSION['AUTOLOAD_PATH'];
Class Questionnaire implements \JsonSerializable
{
    protected $id;
    protected $name;
    protected $status;
    protected $category_name;

    public function __construct($newName=null,$newCategory=null,$newStatus=1,$newId=null)
    {
        if(!isset($this->id)&&$newName!=null)
        {
            $this->id=$newId;
            $this->status=$newStatus;
            $this->name=$newName;
            $this->category_name=$newCategory;
        }
    }

    public function setId($newId)
    {$this->id=$newId;}
    public function getID()
    {return $this->id;}

    public function setName($newName)
    {$this->name=$newName;}
    public function getName()
    {return $this->name;}

    public function setStatus($newStatus)
    {$this->status=$newStatus;}
    public function getStatus()
    {return $this->status;}

    public function setCategory($newCategory)
    {$this->category_name=$newCategory;}
    public function getCategory()
    {return $this->category_name;}

    public function jsonSerialize()
    {
        return [
            'id'=>$this->getID(),
            'name'=>$this->getName(),
            'category_name'=>$this->getCategory(),
            'status'=>$this->getStatus()
        ];
    }
}
?>