<?php
namespace Model\Classes\Data_Objects;
require_once $_SESSION['AUTOLOAD_PATH'];
##class Casses - instance of row in classes table
class Classes
{
    protected $id;
    protected $name;
    protected $faculty_id;
    protected $status;

    public function __construct($newId=null,$newName=null,$newFacultyId=null,$newStatus=null)
    {
       if(!isset($this->id) && !empty($newId))
       {
           $this->id = $newId;
           $this->name = $newName;
           $this->faculty_id = $newFacultyId;
           $this->status = $newStatus;
       } 
    }
    
    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }
    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function getFacultyId()
    {
        return $this->faculty_id;
    }

    public function setFacultyId($facultyId)
    {
        $this->faculty_id = $facultyId;

        return $this;
    }
    public function getStatus()
    {
        return $this->status;
    }
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }
}
?>