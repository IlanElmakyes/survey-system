<?php
namespace Model\Classes\Data_Objects;
use Model\Classes\Data_Objects\ActiveSurvey;

require_once $_SESSION['AUTOLOAD_PATH'];
##class answer numeric- instance of row from answer_numeric table -
##a unique answer for specific class, specific survey, specific teacher or category, on specific question - 
##fields answer_1-answer_5 are counters for results 1-5
class AnswerNumeric
{
	protected $id;
	protected $answer_1;
	protected $answer_2;
	protected $answer_3;
	protected $answer_4;
	protected $answer_5;
	protected $question_id;
	protected $teacher_id;
	protected $class_id;
	protected $activation_date_time;
	protected $category_name;
	
	public function __construct($newId=null,$newAnswer_1=null,$newAnswer_2=null,$newAnswer_3=null,$newAnswer_4=null,$newAnswer_5=null,$newQuestionId=null,$newTeacherId=null,$newClassId=null,$newActivationDateTime=null,$newCategoryName=null)
	{
		if(!isset($this->id) && !empty($newCategoryName))
		{
			$this->id = $newId;
			$this->answer_1 = $newAnswer_1;
			$this->answer_2 = $newAnswer_2;
			$this->answer_3 = $newAnswer_3;
			$this->answer_4 = $newAnswer_4;
			$this->answer_5 = $newAnswer_5;
			$this->question_id = $newQuestionId;
			$this->teacher_id = $newTeacherId;
			$this->class_id = $newClassId;
			$this->activation_date_time = $newActivationDateTime;
			$this->category_name = $newCategoryName;
		}
	}

	public function getId()
	{
		return $this->id;
	}

	public function setId($id)
	{
		$this->id = $id;

		return $this;
	}

	public function getAnswer_1()
	{
		return $this->answer_1;
	}

	public function setAnswer_1($answer_1)
	{
		$this->answer_1 = $answer_1;

		return $this;
	}

	public function getAnswer_3()
	{
		return $this->answer_3;
	}

	public function getAnswer_2()
	{
		return $this->answer_2;
	}

	public function setAnswer_2($answer_2)
	{
		$this->answer_2 = $answer_2;

		return $this;
	}
	
	public function setAnswer_3($answer_3)
	{
		$this->answer_3 = $answer_3;

		return $this;
	}

	public function getAnswer_4()
	{
		return $this->answer_4;
	}

	public function setAnswer_4($answer_4)
	{
		$this->answer_4 = $answer_4;

		return $this;
	}

	public function getAnswer_5()
	{
		return $this->answer_5;
	}

	public function setAnswer_5($answer_5)
	{
		$this->answer_5 = $answer_5;

		return $this;
	}

	public function getQuestionId()
	{
		return $this->question_id;
	}

	public function setQuestionId($questionId)
	{
		$this->question_id = $questionId;

		return $this;
	}

	public function getTeacherId()
	{
		return $this->teacher_id;
	}

	public function setTeacherId($teacherId)
	{
		$this->teacher_id = $teacherId;

		return $this;
	}

	public function getClassId()
	{
		return $this->class_id;
	}

	public function setClassId($classId)
	{
		$this->class_id = $classId;

		return $this;
	}

	public function getActivationDateTime()
	{
		return $this->activation_date_time;
	}

	public function setActivationDateTime($activationDateTime)
	{
		$this->activation_date_time = $activationDateTime;

		return $this;
	}

	public function getCategoryName()
	{
		return $this->category_name;
	}

	public function setCategoryName($categoryName)
	{
		$this->category_name = $categoryName;

		return $this;
	}

	//function counts total number of answers
	public function totalNumberAnswered()
	{
		return $this->answer_1 +$this->answer_2 +$this->answer_3 +$this->answer_4 +$this->answer_5;
	}

	//function return average result for this answer
	public function getAverage()
	{
		$res = ($this->answer_1 + $this->answer_2*2 + $this->answer_3*3 + $this->answer_4*4 + $this->answer_5*5)/$this->totalNumberAnswered();
		return number_format((float)$res,2);
	}

	//function return standard deviation for this answer
	public function standardDeviation()
	{
		$avg = $this->getAverage();
		$total = $this->totalNumberAnswered();
		$res = sqrt(1/$total*(($this->answer_1*pow(1-$avg,2))+($this->answer_2*pow(2-$avg,2))+($this->answer_3*pow(3-$avg,2))+($this->answer_4*pow(4-$avg,2))+
		($this->answer_5*pow(5-$avg,2))));
		return number_format((float)$res,2);
	}

	public function addAnswer($answerNumeric)
	{
		$this->answer_1+=$answerNumeric->answer_1;
		$this->answer_2+=$answerNumeric->answer_2;
		$this->answer_3+=$answerNumeric->answer_3;
		$this->answer_4+=$answerNumeric->answer_4;
		$this->answer_5+=$answerNumeric->answer_5;
	}
}
?>