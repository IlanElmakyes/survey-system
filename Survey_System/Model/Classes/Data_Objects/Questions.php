<?php ##class is an instance of row in table questions
namespace Model\Classes\Data_Objects;
require_once $_SESSION['AUTOLOAD_PATH'];

Class Questions
{
    protected $id;
    protected $text;
    protected $status;
    
    //in the constructor the required value to create an object not from table is a text input
    //status by default is 1 and id is null
    public function __construct($newText=null,$newStatus=1,$newId=null)
    {
        if(!isset($this->id)&&$newText!=null)
        {
            $this->id=$newId;
            $this->text=$newText;
            $this->status=$newStatus;
        }
    }

    public function setId($newId)
    {$this->id=$newId;}
    public function getId()
    {return $this->id;}

    public function setText($newText)
    {$this->text=$newText;}
    public function getText()
    {return $this->text;}

    public function setStatus($newStatus)
    {$this->status=$newStatus;}
    public function getStatus()
    {return $this->status;}
}
?>