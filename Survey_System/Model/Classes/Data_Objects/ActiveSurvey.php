<?php ##class active survey - instance of row from active_survey table
namespace Model\Classes\Data_Objects;
use Model\Classes\DB_Connections as dbConnect;
use Model\Classes\DB_Connections\SurveyDB;

require_once $_SESSION['AUTOLOAD_PATH'];
/*
 * Date is in a DateTime format accordint to ISO8601:
 * YYYY-MM-DDThh:mm:ss[.nnn] - (nnn - milliseconds is optinal)
 * Example 1990-03-21T21:46:21+02:00 - first part is the date then a capital T and then the time, the +02:00 represents the timezone diffrence
 * function to use:
 * date_default_timezone_set("Asia/Jerusalem") - sets the timezone from the normal default 00:00 to 02:00 (Jerusalem)
 * date format used "Y-m-d H:i:s"
 */
date_default_timezone_set("Asia/Jerusalem");
class ActiveSurvey
{
    protected $date_time;
    protected $code;
    protected $class_id;
    protected $survey_id;
    protected $timer_start;
    protected $number_answered;

    public function __construct($newClassId=null,$newSurveyId=null)
    {
        if(!isset($this->date_time)&&$newClassId != null)
        {
            $this->date_time = date("Y-m-d H:i:s");
            $this->code = static::generateNewCode();
            $this->class_id = $newClassId;
            $this->survey_id = $newSurveyId;
            $this->number_answered = 0;
        }
    }
    
    //method generates uniqe code 
    public static function generateNewCode()
    {
        $db=new dbConnect\dbConnection();
		do{
			$newCode=random_int(10000,99999);
		}while($db->checkIfExist("active_survey","code",$newCode));
		return $newCode;
    }

    public function setDate($newDate)
    {
        $this->$date_time=$newDate;
    }

    public function getDate()
    {
        return $this->date_time;
    }

    public function setCode($newCode)
    {
        $this->code=$newCode;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function setClassId($newClassId)
    {
        $this->class_id=$newClassId;
    }

    public function getClassId()
    {
        return $this->class_id;
    }

    public function setSurveyId($newSurveyId)
    {
        $this->survey_id=$newSurveyId;
    }

    public function getSurveyId()
    {
        return $this->survey_id;
    }

    public function setTimerStart($newTStart)
    {
        $this->timer_start=$newTStart;
    }

    public function getTimerStart()
    {
        return $this->timer_start;
    }
    function getNumberAnswered()
    {
        return $this->number_answered;
    }

    public function setNumberAnswered($number_answered)
    {
        $this->number_answered = $number_answered;
    }

    //function create assoc array with all questionnaires and other survey data for this active survey in format:
    //each cell in array contain all info to represent one questinnaire - array('id','name','isTeacher','questions'=>array())
    //if category is teacher - keys 'id' and 'name' contain teacher id and name accordingly
    //otherwise both keys 'id' and 'name' contain category_name
    //funation return created array
    public function getSurveyData()
    {
        $surveyDb= new dbConnect\SurveyDB();
        $questionnairesArr=$surveyDb->getAllQuestionnaires($this->survey_id);
        $qustionnaireDb = new dbConnect\QuestionnaireDB();
        $resultArr=array();
        $teacherIndex=array_search(dbConnect\CategoryDB::teacher,array_column($questionnairesArr,'category_name'));
        if($teacherIndex!==false)
        {
            $classDb=new dbConnect\ClassesDB();
            $teachersArr=$classDb->getAllTeachers($this->getClassId());
            foreach($teachersArr as $teacher)
            $resultArr[]=array('id'=>$teacher['id'],'name'=>$teacher['name'],'isTeacher'=>1,'questions'=>$qustionnaireDb->getAllQuestions($questionnairesArr[$teacherIndex]['id']));
        }
        foreach ($questionnairesArr as $questioannire)
        {
            if($questioannire['category_name']!=dbConnect\CategoryDB::teacher)
            {
                $resultArr[]=array('id'=>$questioannire['category_name'],'name'=>$questioannire['category_name'],'isTeacher'=>0,'questions'=>$qustionnaireDb->getAllQuestions($questioannire['id']));
            }           
        }
        return $resultArr;
    }

    //function saves numeric answer for this active survey
    public function saveNumericAnswer($result,$questId,$teacherId,$categoryName,$isTeacher)
    {
        if($isTeacher != 1)
            $teacherId = null;
        else
            $categoryName = dbConnect\CategoryDB::teacher;
        $answerNumaricDB = new dbConnect\AnswerNumericDB();
        $answerNumaric= $answerNumaricDB->getUniqueAnswer($teacherId,$this->getDate(),$questId,$this->class_id,$categoryName);
        $resultArr = array();
        //creating a result array to insert into the answer row
        for($i=0;$i<5;$i++)
                $resultArr[] = 0 + (($i == $result-1)?1:0);
        if(!empty($answerNumaric))
            $answerNumaricDB->updateAnswerNumbersWithArr($answerNumaric->getId(),$resultArr);
        else
        {
            $answerNumaric = new AnswerNumeric(-1,$resultArr[0],$resultArr[1],$resultArr[2],$resultArr[3],$resultArr[4],$questId,$teacherId,$this->getClassId(),$this->getDate(),$categoryName);
            $answerNumaricDB->insert($answerNumaric);
        }
    }

    //function saves text answer for this active survey
    public function saveTextAnswer($text,$teacherId,$categoryName,$isTeacher)
    {
        if($isTeacher==1)
        {
            $answer=new AnswerText(-1,$text,$this->date_time,$teacherId,dbConnect\CategoryDB::teacher);
        }
        else
            $answer=new AnswerText(-1,$text,$this->date_time,null,$categoryName);
        $save=new dbConnect\AnswerTextDB();
        $save->insert($answer);
    }
}
?>