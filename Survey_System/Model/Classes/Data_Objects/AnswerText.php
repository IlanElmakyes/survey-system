<?php
namespace Model\Classes\Data_Objects;
require_once $_SESSION['AUTOLOAD_PATH'];
##class answer text is instance of row in answer text table - 
##text answer for specific survey and specific teacher or category
class AnswerText
{
	protected $id;
	protected $text;
	protected $activation_date_time;
	protected $teacher_id;
	protected $category_name;
	
	public function __construct($newId = null,$newText = null,$newActivationDateTime = null,$newTeacherId = null,$newCategoryName = null)
	{
		if(!isset($this->id) && !empty($newId))
		{
			$this->id = $newId;
			$this->text = $newText;
			$this->activation_date_time = $newActivationDateTime;
			$this->teacher_id = $newTeacherId;
			$this->category_name = $newCategoryName;
		}
	}
	public function getId()
	{
		return $this->id;
	}
	public function setId($newId)
	{
		$this->id = $newId;
	}
	public function getText()
	{
		return $this->text;
	}
	public function setText($newText)
	{
		$this->text = $newText;
	}
	public function getActivationDateTime()
	{
		return $this->activation_date_time;
	}
	public function setActivationDateTime($newActivationDateTime)
	{
		$this->activation_date_time = $newActivationDateTime;
	}
	public function getTeacherId()
	{
		return $this->teacher_id;
	}
	public function setTeacherId($newTeacherId)
	{
		$this->teacher_id = $newTeacherId;
	}
	public function getCategoryName()
	{
		return $this->category_name;
	}
	public function setCategoryName($newCategoryName)
	{
		$this->category_name = $newCategoryName;
	}
	
}
?>