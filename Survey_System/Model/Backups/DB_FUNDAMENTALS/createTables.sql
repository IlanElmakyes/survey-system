CREATE TABLE `active_survey` (
  `date_time` datetime NOT NULL,
  `code` varchar(50) NOT NULL,
  `class_id` int(11) NOT NULL,
  `survey_id` int(11) NOT NULL,
  `timer_start` datetime NOT NULL,
  `number_answered` int(11) NOT NULL,
  PRIMARY KEY (`date_time`,`class_id`),
  UNIQUE KEY `code` (`code`),
  KEY `class_id` (`class_id`),
  KEY `survey_id` (`survey_id`),
  CONSTRAINT `active_survey_ibfk_1` FOREIGN KEY (`class_id`) REFERENCES `classes` (`id`),
  CONSTRAINT `active_survey_ibfk_2` FOREIGN KEY (`survey_id`) REFERENCES `survey` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

CREATE TABLE `answer_numeric` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) NOT NULL,
  `answer_1` int(11) NOT NULL,
  `answer_2` int(11) NOT NULL,
  `answer_3` int(11) NOT NULL,
  `answer_4` int(11) NOT NULL,
  `answer_5` int(11) NOT NULL,
  `teacher_id` int(11) DEFAULT NULL,
  `class_id` int(11) NOT NULL,
  `activation_date_time` datetime NOT NULL,
  `category_name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `activation_date_time` (`activation_date_time`),
  KEY `category_name` (`category_name`),
  KEY `class_id` (`class_id`),
  KEY `question_id` (`question_id`),
  KEY `teacher_id` (`teacher_id`),
  CONSTRAINT `answer_numeric_ibfk_1` FOREIGN KEY (`activation_date_time`) REFERENCES `active_survey` (`date_time`),
  CONSTRAINT `answer_numeric_ibfk_2` FOREIGN KEY (`category_name`) REFERENCES `category` (`name`),
  CONSTRAINT `answer_numeric_ibfk_3` FOREIGN KEY (`class_id`) REFERENCES `classes` (`id`),
  CONSTRAINT `answer_numeric_ibfk_4` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`),
  CONSTRAINT `answer_numeric_ibfk_5` FOREIGN KEY (`teacher_id`) REFERENCES `teachers` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=157 DEFAULT CHARSET=utf8

CREATE TABLE `answer_text` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` varchar(240) NOT NULL,
  `activation_date_time` datetime NOT NULL,
  `teacher_id` int(11) DEFAULT NULL,
  `category_name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `activation_date_time` (`activation_date_time`),
  KEY `category_name` (`category_name`),
  KEY `teacher_id` (`teacher_id`),
  CONSTRAINT `answer_text_ibfk_1` FOREIGN KEY (`activation_date_time`) REFERENCES `active_survey` (`date_time`),
  CONSTRAINT `answer_text_ibfk_2` FOREIGN KEY (`category_name`) REFERENCES `category` (`name`),
  CONSTRAINT `answer_text_ibfk_3` FOREIGN KEY (`teacher_id`) REFERENCES `teachers` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8

CREATE TABLE `category` (
  `name` varchar(50) NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

CREATE TABLE `classes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `faculty_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `faculty_id` (`faculty_id`),
  CONSTRAINT `classes_ibfk_1` FOREIGN KEY (`faculty_id`) REFERENCES `faculties` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8

CREATE TABLE `faculties` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8

CREATE TABLE `manager` (
  `code` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

CREATE TABLE `questionnaire` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `category_name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_name` (`category_name`),
  CONSTRAINT `questionnaire_ibfk_1` FOREIGN KEY (`category_name`) REFERENCES `category` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8

CREATE TABLE `questionnaire_survey` (
  `survey_id` int(11) NOT NULL,
  `questionnaire_id` int(11) NOT NULL,
  PRIMARY KEY (`survey_id`,`questionnaire_id`),
  KEY `questionnaire_id` (`questionnaire_id`),
  KEY `survey_id` (`survey_id`),
  CONSTRAINT `questionnaire_survey_ibfk_1` FOREIGN KEY (`questionnaire_id`) REFERENCES `questionnaire` (`id`),
  CONSTRAINT `questionnaire_survey_ibfk_2` FOREIGN KEY (`survey_id`) REFERENCES `survey` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

CREATE TABLE `questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` varchar(120) NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8

CREATE TABLE `questions_questionnaire` (
  `question_id` int(11) NOT NULL,
  `questionnaire_id` int(11) NOT NULL,
  PRIMARY KEY (`question_id`,`questionnaire_id`),
  KEY `questionnaire_id` (`questionnaire_id`),
  KEY `question_id` (`question_id`),
  CONSTRAINT `questions_questionnaire_ibfk_1` FOREIGN KEY (`questionnaire_id`) REFERENCES `questionnaire` (`id`),
  CONSTRAINT `questions_questionnaire_ibfk_2` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

CREATE TABLE `survey` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8

CREATE TABLE `teachers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=165 DEFAULT CHARSET=utf8

CREATE TABLE `teachers_classes` (
  `teacher_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  PRIMARY KEY (`teacher_id`,`class_id`),
  KEY `class_id` (`class_id`),
  KEY `teacher_id` (`teacher_id`),
  CONSTRAINT `teachers_classes_ibfk_1` FOREIGN KEY (`class_id`) REFERENCES `classes` (`id`),
  CONSTRAINT `teachers_classes_ibfk_2` FOREIGN KEY (`teacher_id`) REFERENCES `teachers` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

